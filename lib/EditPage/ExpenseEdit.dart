import 'package:expanse_manegment/Screens/Category/categoryMainPage.dart';
import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:expanse_manegment/main.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';



class ExpenseEdit extends StatefulWidget {
  final inStatus;
  final expenseObject;
  final expenseSheet;
  final updateIndex;
  ExpenseEdit(this.inStatus, this.expenseObject, this.expenseSheet, this.updateIndex);

  @override
  _ExpenseEditState createState() => _ExpenseEditState();
}

class _ExpenseEditState extends State<ExpenseEdit> {
  final ExpenseDataHelper inhelper = new ExpenseDataHelper();
  TextEditingController _amountController = new TextEditingController();

  List<ExpenseSheet> excategorylist; /////////////show category list///////
  ExpenseSheet expenseSheet;
  int updateIndex;
  List icon = [];
  String ic = "";
  String inDate = "";
  int indx = 0;
  int icoindx = 0;

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  bool isItem = true;
  
  List<String> _paymentMethod = [
    'Card',
    'Cash',
    'Check',
  ];
  String _currentPaymentMethodSelected = 'Card';

  String t = "text";
  String incDate = "";
  DateTime selectedDate = DateTime.now();
  final f = new DateFormat('yyyy-MM-dd');

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1964, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        incDate = DateFormat.yMMMd().format(selectedDate);
      });
    }
  }

  TextEditingController categoryController = TextEditingController();
  TextEditingController subCategoryController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController noteController = TextEditingController();

@override
  void initState() {
    setState(() {
      icon.clear();

      incDate = widget.expenseObject['date'];
      categ = widget.expenseObject['incategory'];
      iconInd = widget.expenseObject['icon'];
      icnInt = int.parse(iconInd);
      ic = widget.expenseObject['amount'];
      amountController.text = widget.expenseObject[
          'amount']; ////for editing somthing use controller with .text///
      print("categ");
      print(categ);
      print("icnInt");
      print(icnInt);
      print("iconInd");
      print(iconInd);
      expenseSheet = widget.expenseSheet;
      updateIndex = widget.updateIndex;
    });
    super.initState();
  }
////initially clear the list & then add icon///

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            ////////////////////////////////   Date Selection Start  //////////////////////////////
            GestureDetector(
              onTap: () {
                _selectDate(context);
              },
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 0.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.all(2.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    //alignment: Alignment.center,
                                    padding: EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      "Date",
                                      style: TextStyle(
                                          color: Theme.of(context).accentColor,
                                          fontFamily: 'Roboto',
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      incDate,
                                      //DateFormat.yMMMd().format(selectedDate),
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color,
                                          fontFamily: 'Poppins',
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Color(0XFF707070).withOpacity(0.3),
                      height: 10.0,
                    ),
                  ],
                ),
              ),
            ),
            ////////////////////////////////   Date Selection End  //////////////////////////////
            ////////////////////////////////   Category Selection Start  ////////////////////////////
            Container(
              //Catagory
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 1.0),
                                  child: Text(
                                    'Category',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontFamily: 'Roboto',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (!isItem) {
                                        isItem = true;
                                      }
                                      isItem = false;
                                    });
                                    Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (ctxt) =>
                                                new CategoryPage(widget.inStatus)));
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    // decoration: BoxDecoration(
                                    //     border: Border(
                                    //         bottom: BorderSide(
                                    //             width: 1,
                                    //             color: Color(0XFF707070)
                                    //                 .withOpacity(0.2)))),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 18, bottom: 5),
                                      child: 
                                      // isItem
                                      //     ? Text('Select Category',
                                      //         style: TextStyle(
                                      //           color: Theme.of(context)
                                      //               .accentTextTheme
                                      //               .subtitle
                                      //               .color,
                                      //           fontSize: 10,
                                      //           fontFamily: "Poppins",
                                      //         ))
                                      //     :
                                           Row(
                                                  children: <Widget>[
                                                    Text(categ, /////global ver call///
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .accentTextTheme
                                                      .subtitle
                                                      .color,
                                                  fontSize: 10,
                                                  fontFamily: "Poppins",
                                                )),
                                                Icon(iconList[icnInt]['name'],  color: Theme.of(context)
                                                      .accentTextTheme
                                                      .subtitle
                                                      .color,)
                                                  ],
                                                                                     
                                          ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                            
                        //     Navigator.push(
                        //         context,
                        //         MaterialPageRoute(
                        //             builder: (context) => ChooseIcon()));
                        //   },
                        //   child: (icon.length ==0)
                        //       ?  Row(
                        //       children: <Widget>[
                        //         Container(
                        //             height: 17,
                        //             width: 17,
                        //             alignment: Alignment.center,
                        //             decoration: BoxDecoration(
                        //                 border: Border.all(
                        //               color: Theme.of(context)
                        //                   .selectedRowColor,
                        //             )),
                        //             child: Icon(
                        //               Icons.add,
                        //               color: Theme.of(context)
                        //                   .selectedRowColor,
                        //               size: 15,
                        //             )),
                        //         // GestureDetector(
                        //         //   onTap: () {
                        //         //     Navigator.push(
                        //         //         context,
                        //         //         MaterialPageRoute(
                        //         //             builder: (context) => ChooseIcon()));
                        //         //   },
                        //         //   child:
                        //         Container(
                        //           padding: EdgeInsets.only(left: 7.0),
                        //           child:  Text(
                        //             'Choose icon',
                        //             style: TextStyle(
                        //                 color:
                        //                     Theme.of(context).accentColor,
                        //                 fontFamily: 'Roboto',
                        //                 fontSize: 16,
                        //                 fontWeight: FontWeight.bold),
                        //           ),
                        //         ),
                        //         // ),
                        //       ],
                        //     ): Icon(icon[0]['icon']),
                        // )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Divider(height:10, color: Color(0XFF707070)
                                                    .withOpacity(0.5)),
            ////////////////////////////////   Category Selection End  //////////////////////////////
            // ///////////////////////////////////   Sub Category Selection Start  //////////////////////
            // Container(
            //   //Sub Catagory
            //   child: Column(
            //     children: <Widget>[
            //       Container(
            //         padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: <Widget>[
            //             Expanded(
            //               child: Container(
            //                 padding: EdgeInsets.all(2.0),
            //                 child: Column(
            //                   mainAxisAlignment: MainAxisAlignment.start,
            //                   crossAxisAlignment: CrossAxisAlignment.start,
            //                   children: <Widget>[
            //                     Container(
            //                       padding: EdgeInsets.only(bottom: 1.0),
            //                       child: Text(
            //                         'Sub Category',
            //                         style: TextStyle(
            //                             color: Theme.of(context).accentColor,
            //                             fontFamily: 'Roboto',
            //                             fontSize: 16,
            //                             fontWeight: FontWeight.bold),
            //                       ),
            //                     ),
            //                     GestureDetector(
            //                       onTap: () {
            //                         Navigator.push(
            //                             context,
            //                             new MaterialPageRoute(
            //                                 builder: (ctxt) =>
            //                                     new CategoryPage('2')));

            //                       },
            //                       child: Container(
            //                         width: MediaQuery.of(context).size.width,
            //                         decoration: BoxDecoration(
            //                             border: Border(
            //                                 bottom: BorderSide(
            //                                     width: 1,
            //                                     color: Color(0XFF707070)
            //                                         .withOpacity(0.2)))),
            //                         child: Padding(
            //                           padding: const EdgeInsets.only(
            //                               top: 18, bottom: 15),
            //                           child: Text("Food",
            //                               style: TextStyle(
            //                                 color: Theme.of(context)
            //                                     .accentTextTheme
            //                                     .subtitle
            //                                     .color,
            //                                 fontSize: 10,
            //                                 fontFamily: "Poppins",
            //                               )),
            //                         ),
            //                       ),
            //                     ),
            //                   ],
            //                 ),
            //               ),
            //             ),
            //             GestureDetector(
            //               onTap: () {
            //                 Navigator.push(
            //                     context,
            //                     MaterialPageRoute(
            //                         builder: (context) => ChooseIcon()));
            //               },
            //               child: Row(
            //                 children: <Widget>[
            //                   Container(
            //                       height: 17,
            //                       width: 17,
            //                       alignment: Alignment.center,
            //                       decoration: BoxDecoration(
            //                           border: Border.all(
            //                         color: Theme.of(context).selectedRowColor,
            //                       )),
            //                       child: Icon(
            //                         Icons.add,
            //                         color: Theme.of(context).selectedRowColor,
            //                         size: 15,
            //                       )),
            //                   // GestureDetector(
            //                   //   onTap: () {
            //                   //     Navigator.push(
            //                   //         context,
            //                   //         MaterialPageRoute(
            //                   //             builder: (context) => ChooseIcon()));
            //                   //   },
            //                   //   child:
            //                   Container(
            //                     padding: EdgeInsets.only(left: 7.0),
            //                     child: Text(
            //                       'Choose icon',
            //                       style: TextStyle(
            //                           color: Theme.of(context).accentColor,
            //                           fontFamily: 'Roboto',
            //                           fontSize: 16,
            //                           fontWeight: FontWeight.bold),
            //                     ),
            //                   ),
            //                   // ),
            //                 ],
            //               ),
            //             )
            //           ],
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            // ////////////////////////////////  Sub Category Selection End  ////////////////////////////
            //////////////////////////////   Account Amount  Selection Start  ////////////////////////////
            Container(
              //Account Amount
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 1.0),
                                  child: Text(
                                    'Account Amount',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontFamily: 'Roboto',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  child: TextFormField(
                                    onChanged: (value) {
                                      setState(() {
                                        ic = value;
                                      });
                                    },
                                    style: TextStyle(
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .subtitle
                                          .color,
                                      fontSize: 14,
                                      fontFamily: "Poppins",
                                    ),
                                    cursorColor: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    controller: amountController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0XFF707070)
                                                .withOpacity(0.2)),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0XFF707070)
                                                .withOpacity(0.2)),
                                      ),
                                      hintText: "\$10",
                                      hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color,
                                          fontSize: 10,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //////////////////////////////   Account Amount  Selection End  ////////////////////////////
            /////////////////////////////   Payment Method  Selection Start  ////////////////////////////
            Container(
              //Payment Method
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Payment Method',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontFamily: 'Roboto',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                            ////////  Spacing  /////////
                            SizedBox(
                              height: 8.0,
                            ),
                            //////////   Dropdown Button For Payment Method Start  ////////
                            DropdownButton<String>(
                              value: _currentPaymentMethodSelected,
                              iconSize: 24,
                              iconEnabledColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              iconDisabledColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              isExpanded: false,
                              underline: Container(
                                //height: 2,
                                color: Color(0XFFF2F4F7),
                              ),
                              onChanged: (String newValue) {
                               // _onDropdownSelectedMonth(newValue);
                              },
                              items: _paymentMethod
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    height: 26,
                                    padding: EdgeInsets.fromLTRB(
                                        12.0, 5.0, 30.0, 5.0),
                                    decoration: BoxDecoration(
                                        color:
                                            Color(0XFF294F95).withOpacity(0.1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4.0))),
                                    child: Text(value,
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .accentTextTheme
                                                .subtitle
                                                .color,
                                            fontFamily: 'Poppins',
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                );
                              }).toList(),
                            ),
                            ///////   Dropdown Button For Payment Method End ////////
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 8.0, right: 10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Receipt ',
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                              ////////  Spacing  /////////
                              SizedBox(
                                height: 15.0,
                              ),
                              Container(
                                height: 28,
                                width: 70,
                                decoration: BoxDecoration(
                                    color: Color(0XFF294F95).withOpacity(0.1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4.0))),
                                child: FlatButton(
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    padding: const EdgeInsets.fromLTRB(
                                        1.0, 5.0, 0.0, 5.0),
                                    child: Text(
                                      'Upload',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color,
                                          fontFamily: 'Poppins',
                                          fontSize: 10),
                                    ),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(
                    color: Color(0XFF707070).withOpacity(0.3),
                    height: 5.0,
                  ),
                ],
              ),
            ),
            /////////////////////////////   Payment Method Selection End  ////////////////////////////
            //////////////////////////////  Note/Report Start  /////////////////////////////
            Container(
              //Note/Report
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 1.0),
                                  child: Text(
                                    'Note/Report',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontFamily: 'Roboto',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  child: TextField(
                                    style: TextStyle(
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .subtitle
                                          .color,
                                      fontSize: 14,
                                      fontFamily: "Poppins",
                                    ),
                                    cursorColor: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    controller: noteController,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0XFF707070)
                                                .withOpacity(0.2)),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0XFF707070)
                                                .withOpacity(0.2)),
                                      ),
                                      hintText: "Note your purpose",
                                      hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color,
                                          fontSize: 10,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //////////////////////////////  Note/Report End  /////////////////////////////
            //////////////////////////////////  Save Button Start  /////////////////////////////////
            Container(
              //Save
              margin: EdgeInsets.fromLTRB(5.0, 20.0, 5.0, 20.0),
              width: MediaQuery.of(context).size.width / 1.7,
              height: 55,
              child: Card(
                color: Theme.of(context).buttonColor,
                elevation: 2.0,
                child: FlatButton(
                  color: Theme.of(context).buttonColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Text(
                    "Save",
                    style: TextStyle(
                        color: Theme.of(context).accentTextTheme.display1.color,
                        fontSize: 16,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    _saveCategory(context);
                  },
                ),
              ),
            ),
            /////////////////////////////////  Save Button End  /////////////////////////////////
          ],
        ),
      ),
    );
  }

 void _saveCategory(BuildContext context) {
    if (ic == "") {
      return _showMsg('Amount should not empty');
    } else if (categ == "" || icnInt == 0) {
      return _showMsg('Category should not empty');
    }
    // else if (incDate == "" ) {
    //   return _showMsg('Date should not empty');
    // }
    else {
      print("expenseSheet");
      print(expenseSheet);

      if (expenseSheet == null) {
        print("_amountController.text, icnInt, categ, inDate");
        print(ic);
        print(icnInt.toString());
        print(categ);
        print(inDate);

        //data format
        ExpenseSheet cat = new ExpenseSheet(
          date: incDate,
          selectCat: categ,
          selectIcon: iconInd,
          amount: ic,
        );

        //insert query
        inhelper.insertExpenseSheet(cat).then((id) => {
              _amountController.clear(),
              icon.clear(),
              
            });
        Navigator.of(context).pop();
      } else {
        expenseSheet.date = incDate;
        expenseSheet.selectCat = categ;
        expenseSheet.selectIcon = iconInd;
        expenseSheet.amount =ic;
        print("expenseSheet.amount");
        print(expenseSheet.amount);
        print( "expenseSheet.selectCat");
        print( expenseSheet.selectCat);
        print("expenseSheet.selectIcon");
        print(expenseSheet.selectIcon);
        print("expenseSheet.date");
        print(expenseSheet.date);

        inhelper.updateExpenseSheet(expenseSheet).then((id) => {
              setState(() {
                excategorylist[updateIndex].date = incDate;
                excategorylist[updateIndex].selectCat = categ;
                excategorylist[updateIndex].selectIcon = iconInd;
                excategorylist[updateIndex].amount = ic;

                //icon.clear();
              }),
              _amountController.clear(),
              icon.clear(),

              expenseSheet = null
            });

            Navigator.of(context).pop();
      }
    }
 
    void _onDropdownSelectedMonth(String newValue) {
      setState(() {
        this._currentPaymentMethodSelected = newValue;
      });
    }
  }
}
