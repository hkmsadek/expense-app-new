import 'package:expanse_manegment/Screens/HomePage/homePage.dart';
import 'package:expanse_manegment/Screens/LoginPage/loginPage.dart';
import 'package:expanse_manegment/customPlugin/routeTransition/routeAnimation.dart';
import 'package:expanse_manegment/database/RegistrationData.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RegistrationForm extends StatefulWidget {
  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  final RegistrationHelper helper = new RegistrationHelper();

  String fn = "";
  String ln = "";
  String un = "";
  String ml = "";
  String em = "";
  String ps = "";
  String conps = "";

  bool passwordvisible = true;
  bool conpasswordvisible = true;

  void _togglePasswordVisibility() {
    setState(() {
      passwordvisible = !passwordvisible;
    });
  }

  void _toggleconPasswordVisibility() {
    setState(() {
      conpasswordvisible = !conpasswordvisible;
    });
  }

  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();

  List<Registration> registrationlist;
  Registration registration;
  int updateIndex;
  int usernameInt = 0, phoneInt = 0;

  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
        child: Container(
            padding: EdgeInsets.only(top: 8, bottom: 0, left: 20, right: 20),
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
            ),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  fn = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: firstnameController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Firstname",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  ln = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: lastnameController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Lastname",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  un = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: usernameController,

                              // inputFormatters: <TextInputFormatter>[
                              //   WhitelistingTextInputFormatter(RegExp("[a-z]")),
                              // ],
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "User Name",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.call,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 8),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  ml = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.number,
                              controller: phoneController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Mobile",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.email,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  em = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: emailController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Email",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.lock,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 8),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  ps = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: passwordController,
                              keyboardType: TextInputType.text,
                              obscureText: passwordvisible,
                              autofocus: false,
                              decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal,
                                  ),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      _togglePasswordVisibility();
                                    },
                                    child: Icon(
                                      passwordvisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: passwordvisible
                                          ? Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color
                                          : Theme.of(context).bottomAppBarColor,
                                    ),
                                  ),
                                  isDense: true),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.lock,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 8),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  conps = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: confirmpasswordController,
                              keyboardType: TextInputType.text,
                              obscureText: conpasswordvisible,
                              autofocus: false,
                              decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  hintText: "Confirm Password",
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .subtitle
                                          .color,
                                      fontSize: 15,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.normal),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      _toggleconPasswordVisibility();
                                    },
                                    child: Icon(
                                      conpasswordvisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: conpasswordvisible
                                          ? Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color
                                          : Theme.of(context).bottomAppBarColor,
                                    ),
                                  ),
                                  isDense: true),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 10, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                onPressed: () {
                                  setState(() {
                                    firstnameController.text = "";
                                    lastnameController.text = "";
                                    usernameController.text = "";
                                    phoneController.text = "";
                                    emailController.text = "";
                                    passwordController.text = "";
                                    confirmpasswordController.text = "";
                                  });
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Cancel',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 10, right: 20, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                onPressed: _reg,
                                //onPressed: () {},
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Save',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )));
  }

  void _reg() async {
    setState(() {
      phoneInt = 0;
      usernameInt = 0;
    });
    if (firstnameController.text.isEmpty) {
      return _showMsg("FirstName is empty");
    } else if (lastnameController.text.isEmpty) {
      return _showMsg("LastName is empty");
    }
    if (usernameController.text.isEmpty) {
      return _showMsg("UserName is empty");
    } else if (usernameController.text.contains(" ")) {
      return _showMsg("Username should not contain space");
    } else if (phoneController.text.isEmpty) {
      return _showMsg("Phone is empty");
    }
    if (emailController.text.isEmpty) {
      return _showMsg("Email is empty");
    } else if (!emailController.text.contains("@") ||
        !emailController.text.contains(".")) {
      return _showMsg("Enter a valid email");
    } else if (passwordController.text.isEmpty) {
      return _showMsg("Enter your password");
    }
    if (confirmpasswordController.text.isEmpty) {
      return _showMsg("Confirm your password");
    } else if (confirmpasswordController.text != passwordController.text) {
      return _showMsg("Password not match");
    }
    if (registration == null) {
      Future.delayed(Duration.zero, () async {
        final _userAuthData1 = await helper.getRegistrationList();

        print("_userAuthData1.length");
        print(_userAuthData1.length);

        for (int i = 0; i < _userAuthData1.length; i++) {
          /////amount calculation///
          setState(() {
            Registration cat = _userAuthData1[i];

            print("cat.mobile");
            print(cat.mobile);

            if (phoneController.text == _userAuthData1[i].mobile) {
              phoneInt++;
            }
            if (usernameController.text == _userAuthData1[i].userName) {
              usernameInt++;
            }
          });
        }

        if (phoneInt == 0 && usernameInt == 0) {
          Registration reg = new Registration(
              firstName: firstnameController.text,
              lastName: lastnameController.text,
              userName: usernameController.text,
              mobile: phoneController.text,
              email: emailController.text,
              password: passwordController.text,
              confpassword: confirmpasswordController.text);

          helper.insertRegistration(reg).then((id) => {
                firstnameController.clear(),
                lastnameController.clear(),
                usernameController.clear(),
                phoneController.clear(),
                emailController.clear(),
                passwordController.clear(),
                confirmpasswordController.clear(),
              });

          Future.delayed(Duration.zero, () async {
            final _userAuthData2 = await helper.getRegistrationList();

            print("_userAuthData2.length");
            print(_userAuthData2.length);
          });
          Navigator.pop(context);
        } else if (phoneInt != 0) {
          return _showMsg("Phone number already exists!");
        } else if (usernameInt != 0) {
          return _showMsg("Username already exists!");
        }
      });
    } else {
      registration.firstName = firstnameController.text;
      registration.lastName = lastnameController.text;
      registration.userName = usernameController.text;
      registration.mobile = phoneController.text;
      registration.email = emailController.text;
      registration.password = passwordController.text;
      registration.confpassword = confirmpasswordController.text;

      helper.updateRegistration(registration).then((id) => {
            setState(() {
              registrationlist[updateIndex].firstName =
                  firstnameController.text;
              registrationlist[updateIndex].lastName = lastnameController.text;
              registrationlist[updateIndex].userName = usernameController.text;
              registrationlist[updateIndex].mobile = phoneController.text;
              registrationlist[updateIndex].email = emailController.text;
              registrationlist[updateIndex].password = passwordController.text;
              registrationlist[updateIndex].confpassword =
                  confirmpasswordController.text;
            }),
            firstnameController.clear(),
            lastnameController.clear(),
            usernameController.clear(),
            phoneController.clear(),
            emailController.clear(),
            passwordController.clear(),
            confirmpasswordController.clear(),
            registration = null
          });
    }

    //Navigator.push(context, SlideLeftRoute(page: LoginPage()));
  }
}
