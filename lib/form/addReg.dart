import 'package:expanse_manegment/form/LoginForm/RegistrationForm.dart';
import 'package:flutter/material.dart';

class AddReg extends StatefulWidget {
  @override
  _AddRegState createState() => _AddRegState();
}

class _AddRegState extends State<AddReg> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text("Registration",
                  // widget.val =="1"?"Category":"Sub Category",
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: RegistrationForm(),
    );
  }
}