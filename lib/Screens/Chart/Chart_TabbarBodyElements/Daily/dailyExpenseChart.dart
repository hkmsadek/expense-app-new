import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';

class DailyExpenseChart extends StatefulWidget {
  @override
  _DailyExpenseChartState createState() => _DailyExpenseChartState();
}

class _DailyExpenseChartState extends State<DailyExpenseChart> {
  bool toggle = false;
  Map<String, double> dataMap = new Map();
  List<Color> colorList = [
    Color(0XFFF78F1E),
    Color(0XFFED6D23),
    Color(0XFF027A6C),
    Color(0XFF9D9D36),
    Color(0XFFE54D25),
  ];
  final ExpenseDataHelper exhelper = new ExpenseDataHelper();
  List allData = [];
  double exbalanc = 0;
  String expDate = "";
  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    setState(() {
      expDate = DateFormat.yMMMd().format(selectedDate);
    });

    ////////// Calling Pie Chart //////////
    togglePieChart();

    expensetotal();
  }

  void expensetotal() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await exhelper.getDateExpenseSheetList(expDate);
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String exbalnc = _userAuthData[i].amount;
          exbalanc = double.parse(exbalnc);
          print(exbalanc);
          allData.add({
            'name': _userAuthData[i].selectCat,
            'amt': _userAuthData[i].amount,
          });
        });
      }
      if (exbalanc != 0) {
        dataMap.putIfAbsent("Total Expense", () => exbalanc.toDouble());
        setState(() {
          toggle = true;
        });
      } else {
        setState(() {
          toggle = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              child: toggle
                  ? PieChart(
                      dataMap: dataMap,
                      legendFontColor: Colors.blueGrey[900],
                      legendFontSize: 14.0,
                      legendFontWeight: FontWeight.w500,
                      animationDuration: Duration(milliseconds: 800),
                      chartLegendSpacing: 32.0,
                      chartRadius: MediaQuery.of(context).size.width / 2.3,
                      showChartValuesInPercentage: true,
                      showChartValues: true,
                      showChartValuesOutside: false,
                      chartValuesColor: Colors.white,
                      colorList: colorList,
                      showLegends: false,
                      //decimalPlaces: 1,
                      showChartValueLabel: false,
                      chartValueFontSize: 14,
                      chartValueFontWeight: FontWeight.bold,
                      initialAngle: 0,
                    )
                  : Center(child: Text("No data available!")),
            ),

            ////////  Showing Items List  ////////
            allData == null
                ? Container()
                : Container(
                    margin: EdgeInsets.only(top: 30, right: 20),
                    child: Column(
                      children: List.generate(allData.length, (int index) {
                        return itemsColumn(
                            Color(0XFFE54D25),
                            allData[index]['name'],
                            '\$${allData[index]['amt']}');
                      }),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Container itemsColumn(Color color, String title, String amount) {
    return Container(
      margin: EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 10.0),
      padding: EdgeInsets.only(right: 8.0, left: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  ////// Color Container //////
                  // Container(
                  //   height: 20,
                  //   width: 20,
                  //   decoration: BoxDecoration(
                  //     color: color,
                  //     borderRadius: BorderRadius.circular(20.0),
                  //   ),
                  // ),

                  //////Spacing////
                  SizedBox(
                    width: 12.0,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      color: Theme.of(context).accentTextTheme.body1.color,
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Text(
                amount,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  void togglePieChart() {
    setState(() {
      toggle = !toggle;
    });
  }
}
