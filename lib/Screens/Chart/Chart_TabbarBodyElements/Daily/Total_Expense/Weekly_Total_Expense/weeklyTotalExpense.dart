import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class WeeklyTotalExpense extends StatefulWidget {
  @override
  _WeeklyTotalExpenseState createState() => _WeeklyTotalExpenseState();
}

class _WeeklyTotalExpenseState extends State<WeeklyTotalExpense> {
  List<charts.Series<DaySales, DateTime>> _seriesLineData;

  _generateData() {
    var linesalesdata1 = [
      new DaySales(DateTime(2019, 10, 6), 5),
      new DaySales(DateTime(2019, 10, 7), 25),
      new DaySales(DateTime(2019, 10, 8), 100),
      new DaySales(DateTime(2019, 10, 9), 75),
      new DaySales(DateTime(2019, 10, 10), 25),
      new DaySales(DateTime(2019, 10, 11), 100),
      new DaySales(DateTime(2019, 10, 12), 75),
    ];

//   var linesalesdata2 = [
//    new Sales(0, 20),
//    new Sales(1, 30),
//    new Sales(2, 40),
//    new Sales(3, 45),
//    new Sales(4, 55),
//    new Sales(5, 60),
//  ];

//   var linesalesdata3 = [
//    new Sales(0, 20),
//    new Sales(1, 25),
//    new Sales(2, 45),
//    new Sales(3, 50),
//    new Sales(4, 45),
//    new Sales(5, 60),
//  ];

    _seriesLineData.add(charts.Series(
      data: linesalesdata1,
      domainFn: (DaySales daySales, _) => daySales.dayval,
      measureFn: (DaySales daySales, _) => daySales.salesval,
      //areaColorFn: (DaySales daySales, _) => charts.Color.fromHex(code: "#ff0001"),
      // seriesColor: charts.ColorUtil.fromDartColor(Colors.pink),
      // insideLabelStyleAccessorFn: ,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      colorFn: (DaySales daySales, _) =>
          charts.ColorUtil.fromDartColor(Color(0xFF1A3D7A)),
      //fillColorFn: (DaySales daySales, _) => charts.ColorUtil.fromDartColor(Colors.amber),

      id: 'Sales',
    ));

    //  _seriesLineData.add(
    //    charts.Series(
    //      data: linesalesdata2,
    //      domainFn: (Sales sales,_)=> sales.yearval,
    //      measureFn: (Sales sales,_)=> sales.salesval,
    //      fillPatternFn: (_, __)=> charts.FillPatternType.solid,
    //      colorFn: (Sales sales,_)=> charts.ColorUtil.fromDartColor(Colors.red),
    //      id: 'Air Pollution',
    //      )
    //  );

    //   _seriesLineData.add(
    //    charts.Series(
    //      data: linesalesdata3,
    //      domainFn: (Sales sales,_)=> sales.yearval,
    //      measureFn: (Sales sales,_)=> sales.salesval,
    //      fillPatternFn: (_, __)=> charts.FillPatternType.solid,
    //      colorFn: (Sales sales,_)=> charts.ColorUtil.fromDartColor(Colors.amber),
    //      id: 'Air Pollution',
    //      )
    //  );
  }

  @override
  void initState() {
    super.initState();
    _seriesLineData = List<charts.Series<DaySales, DateTime>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          child: Container(
          child: Column(
        children: <Widget>[
          Container(
            height: 270,
            color: Theme.of(context).accentTextTheme.display4.color,
            margin: EdgeInsets.fromLTRB(8.0, 35.0, 10.0, 10.0),
            padding: EdgeInsets.fromLTRB(8.0, 20.0, 15.0, 10.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: charts.TimeSeriesChart(
                      _seriesLineData,
                      animate: true,
                      animationDuration: Duration(seconds: 1),

                      defaultRenderer: new charts.LineRendererConfig(
                        includePoints: true,
                        stacked: true,
                        //includeLine: true,

                        //includeArea: true,
                      ),
                      //             layoutConfig: charts.LayoutConfig(
                      //     leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                      //     topMarginSpec: charts.MarginSpec.fixedPixel(75),
                      //     rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                      //     bottomMarginSpec: charts.MarginSpec.fixedPixel(5)
                      // ),

                      //defaultInteractions: false,
                      dateTimeFactory: const charts.LocalDateTimeFactory(),
                      domainAxis: charts.DateTimeAxisSpec(
                        tickFormatterSpec: charts.AutoDateTimeTickFormatterSpec(
                          day: charts.TimeFormatterSpec(
                            format: 'EEE',
                            transitionFormat: 'EEE',
                          ),
                        ),
                        renderSpec: charts.SmallTickRendererSpec(
                          labelOffsetFromAxisPx: 10,
                          minimumPaddingBetweenLabelsPx: 2,
                          labelAnchor: charts.TickLabelAnchor.inside,
                          lineStyle: charts.LineStyleSpec(
                            color: charts.Color.fromHex(code: "#2F4F87"),
                            thickness: 2,
                          ),
                          axisLineStyle: charts.LineStyleSpec(
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                          labelStyle: charts.TextStyleSpec(
                              fontSize: 11,
                              fontFamily: 'Poppins',
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                        ),
                      ),
                      // domainAxis: charts.NumericAxisSpec(
                      //   // tickProviderSpec: new charts.StaticNumericTickProviderSpec(tr),
                      // ),
                      primaryMeasureAxis: charts.NumericAxisSpec(
                        showAxisLine: true,
                        renderSpec: charts.SmallTickRendererSpec(
                          labelOffsetFromAxisPx: 5,
                          minimumPaddingBetweenLabelsPx: 2,
                          labelAnchor: charts.TickLabelAnchor.centered,
                          lineStyle: charts.LineStyleSpec(
                            color: charts.Color.fromHex(code: "#2F4F87"),
                            thickness: 2,
                          ),
                          axisLineStyle: charts.LineStyleSpec(
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                          labelStyle: charts.TextStyleSpec(
                              fontSize: 11,
                              fontFamily: 'Poppins',
                              // lineHeight: 15,
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                        ),
                      ),

                      // behaviors: [
                      //   // new charts.ChartTitle('',
                      //   //     behaviorPosition: charts.BehaviorPosition.bottom,
                      //   //     titleOutsideJustification:
                      //   //         charts.OutsideJustification.middleDrawArea),
                      //   // new charts.ChartTitle('Sales',
                      //   // behaviorPosition: charts.BehaviorPosition.start,
                      //   // titleOutsideJustification: charts.OutsideJustification.startDrawArea),
                      //   //   new charts.ChartTitle('Departments',
                      //   //   behaviorPosition: charts.BehaviorPosition.end,
                      //   //   titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
                      // ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 30.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ///////   Color Container  /////////
                    Container(
                      margin: EdgeInsets.only(right: 8.0),
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        color: Color(0XFF2F4F87),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                    Text(
                      "Oct, 2019",
                      style: TextStyle(
                          color: Theme.of(context).accentTextTheme.body1.color,
                          fontFamily: 'Poppins',
                          fontSize: 19,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Total Expense 1350",
                    style: TextStyle(
                      color: Theme.of(context).accentTextTheme.body1.color,
                      fontFamily: 'Poppins',
                      fontSize: 17,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      )),
    );
  }
}

class DaySales {
  DateTime dayval;
  double salesval;

  DaySales(this.dayval, this.salesval);
}