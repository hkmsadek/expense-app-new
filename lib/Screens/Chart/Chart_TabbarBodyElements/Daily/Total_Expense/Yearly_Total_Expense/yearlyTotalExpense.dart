import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class YearlyTotalExpense extends StatefulWidget {
  @override
  _YearlyTotalExpenseState createState() => _YearlyTotalExpenseState();
}

class _YearlyTotalExpenseState extends State<YearlyTotalExpense> {
  List<charts.Series<YearSales, DateTime>> _seriesLineData;

  _generateData() {
    var linesalesdata1 = [
      new YearSales(DateTime(2019, 1, 1), 5),
      new YearSales(DateTime(2019, 2, 1), 25),
      new YearSales(DateTime(2019, 3, 1), 100),
      new YearSales(DateTime(2019, 4, 1), 75),
      new YearSales(DateTime(2019, 5, 1), 25),
      new YearSales(DateTime(2019, 6, 1), 100),
      new YearSales(DateTime(2019, 7, 1), 75),
      new YearSales(DateTime(2019, 8, 1), 75),
      new YearSales(DateTime(2019, 9, 1), 25),
      new YearSales(DateTime(2019, 10, 1), 100),
      new YearSales(DateTime(2019, 11, 1), 75),
      new YearSales(DateTime(2019, 12, 1), 75),
    ];

    _seriesLineData.add(charts.Series(
      data: linesalesdata1,
      domainFn: (YearSales yearSales, _) => yearSales.yearval,
      measureFn: (YearSales yearSales, _) => yearSales.salesval,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      colorFn: (YearSales yearSales, _) =>
          charts.ColorUtil.fromDartColor(Color(0xFF1A3D7A)),

      id: 'Sales',
    ));

  }

@override
  void initState() {
    super.initState();
    _seriesLineData = List<charts.Series<YearSales, DateTime>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          child: Container(
          child: Column(
            children: <Widget>[
              Container(
                  height: 270,
                  color: Theme.of(context).accentTextTheme.display4.color,
                  margin: EdgeInsets.fromLTRB(8.0, 35.0, 10.0, 10.0),
                  padding: EdgeInsets.fromLTRB(8.0, 20.0, 15.0, 10.0),
                  child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: charts.TimeSeriesChart(
                      _seriesLineData,
                      animate: true,
                      animationDuration: Duration(seconds: 1),

                      defaultRenderer: new charts.LineRendererConfig(
                        includePoints: true,
                        stacked: true,
                        //includeLine: true,

                        //includeArea: true,
                      ),
                      //             layoutConfig: charts.LayoutConfig(
                      //     leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                      //     topMarginSpec: charts.MarginSpec.fixedPixel(75),
                      //     rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                      //     bottomMarginSpec: charts.MarginSpec.fixedPixel(5)
                      // ),

                      //defaultInteractions: false,
                      dateTimeFactory: const charts.LocalDateTimeFactory(),
                      domainAxis: charts.DateTimeAxisSpec(
                        tickFormatterSpec: charts.AutoDateTimeTickFormatterSpec(
                          day: charts.TimeFormatterSpec(
                            format: 'EEE',
                            transitionFormat: 'EEE',
                          ),
                        ),
                        renderSpec: charts.SmallTickRendererSpec(
                          labelOffsetFromAxisPx: 12,
                          minimumPaddingBetweenLabelsPx: 1,
                          labelAnchor: charts.TickLabelAnchor.centered,
                          lineStyle: charts.LineStyleSpec(
                            color: charts.Color.fromHex(code: "#2F4F87"),
                            thickness: 2,
                          ),
                          axisLineStyle: charts.LineStyleSpec(
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                          labelStyle: charts.TextStyleSpec(
                              fontSize: 11,
                              fontFamily: 'Poppins',
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                        ),
                      ),
                      // domainAxis: charts.NumericAxisSpec(
                      //   // tickProviderSpec: new charts.StaticNumericTickProviderSpec(tr),
                      // ),
                      primaryMeasureAxis: charts.NumericAxisSpec(
                        showAxisLine: true,
                        renderSpec: charts.SmallTickRendererSpec(
                          labelOffsetFromAxisPx: 5,
                          minimumPaddingBetweenLabelsPx: 2,
                          labelAnchor: charts.TickLabelAnchor.centered,
                          lineStyle: charts.LineStyleSpec(
                            color: charts.Color.fromHex(code: "#2F4F87"),
                            thickness: 2,
                          ),
                          axisLineStyle: charts.LineStyleSpec(
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                          labelStyle: charts.TextStyleSpec(
                              fontSize: 11,
                              fontFamily: 'Poppins',
                              // lineHeight: 15,
                              color: charts.Color.fromHex(code: "#1A3D7A")),
                        ),
                      ),

                      // behaviors: [
                      //   // new charts.ChartTitle('',
                      //   //     behaviorPosition: charts.BehaviorPosition.bottom,
                      //   //     titleOutsideJustification:
                      //   //         charts.OutsideJustification.middleDrawArea),
                      //   // new charts.ChartTitle('Sales',
                      //   // behaviorPosition: charts.BehaviorPosition.start,
                      //   // titleOutsideJustification: charts.OutsideJustification.startDrawArea),
                      //   //   new charts.ChartTitle('Departments',
                      //   //   behaviorPosition: charts.BehaviorPosition.end,
                      //   //   titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
                      // ],
                    ),
                  )
                ],
              ),
            ),),
              Container(
                margin: EdgeInsets.only(top: 30.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        ///////   Color Container  /////////
                        Container(
                          margin: EdgeInsets.only(right: 8.0),
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        color: Color(0XFF2F4F87),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                        Text(
                          "Oct, 2019", 
                        style: TextStyle(
                          color: Theme.of(context).accentTextTheme.body1.color,
                          fontFamily: 'Poppins',
                          fontSize: 19,
                          fontWeight: FontWeight.w400

                          ),),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Total Expense 1350",
                      style: TextStyle(
                            color: Theme.of(context).accentTextTheme.body1.color,
                            fontFamily: 'Poppins',
                            fontSize: 17,

                            ),),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}

class YearSales {
  DateTime yearval;
  double salesval;

  YearSales(this.yearval, this.salesval);
}