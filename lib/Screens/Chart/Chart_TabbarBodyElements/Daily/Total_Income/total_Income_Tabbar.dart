import 'package:expanse_manegment/Screens/Chart/Chart_TabbarBodyElements/Daily/Total_Income/Monthly_Total_Income/monthlyTotalIncome.dart';
import 'package:expanse_manegment/Screens/Chart/Chart_TabbarBodyElements/Daily/Total_Income/Weekly_Total_Income/weeklyTotalIncome.dart';
import 'package:expanse_manegment/Screens/Chart/Chart_TabbarBodyElements/Daily/Total_Income/Yearly_Total_Income/yearlyTotalIncome.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class TotalIncome extends StatefulWidget {
  @override
  _TotalIncomeState createState() => _TotalIncomeState();
}

class _TotalIncomeState extends State<TotalIncome>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  List<charts.Series<Sales, int>> _seriesLineData;
  List<charts.Series<Pollution, String>> _seriesBarData;
  List<charts.Series<Task, String>> _seriesChartData;
  _generateData() {
    var linesalesdata1 = [
      new Sales(0, 30),
      new Sales(1, 40),
      new Sales(2, 45),
      new Sales(3, 50),
      new Sales(4, 60),
      new Sales(5, 70),
    ];

    var linesalesdata2 = [
      new Sales(0, 20),
      new Sales(1, 30),
      new Sales(2, 40),
      new Sales(3, 45),
      new Sales(4, 55),
      new Sales(5, 60),
    ];

    var linesalesdata3 = [
      new Sales(0, 20),
      new Sales(1, 25),
      new Sales(2, 45),
      new Sales(3, 50),
      new Sales(4, 45),
      new Sales(5, 60),
    ];

    _seriesLineData.add(charts.Series(
      data: linesalesdata1,
      domainFn: (Sales sales, _) => sales.yearval,
      measureFn: (Sales sales, _) => sales.salesval,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      colorFn: (Sales sales, _) =>
          charts.ColorUtil.fromDartColor(Color(0xFF990099)),
      id: 'Air Pollution',
    ));

    _seriesLineData.add(charts.Series(
      data: linesalesdata2,
      domainFn: (Sales sales, _) => sales.yearval,
      measureFn: (Sales sales, _) => sales.salesval,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      colorFn: (Sales sales, _) => charts.ColorUtil.fromDartColor(Colors.red),
      id: 'Air Pollution',
    ));

    _seriesLineData.add(charts.Series(
      data: linesalesdata3,
      domainFn: (Sales sales, _) => sales.yearval,
      measureFn: (Sales sales, _) => sales.salesval,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      colorFn: (Sales sales, _) => charts.ColorUtil.fromDartColor(Colors.amber),
      id: 'Air Pollution',
    ));

    var data1 = [
      new Pollution('USA', 1980, 30),
      new Pollution('Asia', 1980, 40),
      new Pollution('Europe', 1985, 10),
    ];
    var data2 = [
      new Pollution('USA', 1985, 130),
      new Pollution('Asia', 1980, 240),
      new Pollution('Europe', 1985, 100),
    ];
    var data3 = [
      new Pollution('USA', 1980, 300),
      new Pollution('Asia', 1980, 140),
      new Pollution('Europe', 1985, 200),
    ];

    _seriesBarData.add(charts.Series(
      data: data1,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Color(0xFF990099)),
      id: '2017',
    ));

    _seriesBarData.add(charts.Series(
      data: data2,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Colors.red),
      id: '2017',
    ));

    _seriesBarData.add(charts.Series(
      data: data3,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Colors.amber),
      id: '2017',
    ));

    var chartData = [
      new Task('task', 35.0, Colors.amber),
      new Task('eat', 8.0, Colors.red),
      new Task('commute', 10.0, Colors.green),
      new Task('sleep', 15.0, Colors.blue),
    ];

    _seriesChartData.add(charts.Series(
      data: chartData,
      domainFn: (Task task, _) => task.task,
      measureFn: (Task task, _) => task.taskvalue,
      colorFn: (Task task, _) => charts.ColorUtil.fromDartColor(task.colorval),
      id: 'Daily Task',
      labelAccessorFn: (Task row, _) => '${row.taskvalue}',
    ));
  }

  @override
  void initState() {
    tabController = new TabController(length: 3, vsync: this);
    super.initState();
    _seriesChartData = List<charts.Series<Task, String>>();
    _seriesBarData = List<charts.Series<Pollution, String>>();
    _seriesLineData = List<charts.Series<Sales, int>>();
    _generateData();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var tabBarItem = new TabBar(
      tabs: [
        new Tab(
          child: Text('Weekly',
              style: TextStyle(
                  color: Color(0XFF1A3D7A),
                  fontFamily: 'Roboto',
                  fontSize: 15,
                  fontWeight: FontWeight.bold)),
        ),
        new Tab(
          child: Text('Monthly',
              style: TextStyle(
                  color: Color(0XFF1A3D7A),
                  fontFamily: 'Roboto',
                  fontSize: 15,
                  fontWeight: FontWeight.bold)),
        ),
        new Tab(
          child: Text('Yearly',
              style: TextStyle(
                  color: Color(0XFF1A3D7A),
                  fontFamily: 'Roboto',
                  fontSize: 15,
                  fontWeight: FontWeight.bold)),
        ),
      ],
      controller: tabController,
      indicatorColor: Color(0XFF1A3D7A),
      indicatorSize: TabBarIndicatorSize.label,
      labelColor: Color(0XFF1A3D7A),
      indicatorWeight: 2.5,
      isScrollable: true,
    );

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(120.0),
          child: AppBar(
            elevation: 4.0,
            backgroundColor: Theme.of(context).primaryColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                  color: Color(0XFF1A3D7A),
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Text('Total Income',
                  style: TextStyle(
                    color: Color(0XFF1A3D7A),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
            bottom: tabBarItem,
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: <Widget>[
            //  Container(
            //   padding: EdgeInsets.all(8.0),
            //   child: Center(
            //     child: Column(
            //       children: <Widget>[
            //         Text('Time spent on daily task'),
            //         SizedBox(height: 10.0,),
            //         Expanded(child: charts.LineChart(
            //           _seriesLineData,
            //           animate: true,
            //           animationDuration: Duration(seconds: 5),
            //           defaultRenderer: new charts.LineRendererConfig(
            //             includeArea: true, stacked: true,
            //           ),
            //           behaviors: [
            //             new charts.ChartTitle('Year',
            //             behaviorPosition: charts.BehaviorPosition.bottom,
            //             titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
            //             new charts.ChartTitle('Sales',
            //             behaviorPosition: charts.BehaviorPosition.start,
            //             titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
            //             new charts.ChartTitle('Departments',
            //             behaviorPosition: charts.BehaviorPosition.end,
            //             titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
            //           ],
            //           ),

            //         )
            //       ],
            //     ),
            //   ),
            // ),
             WeeklyTotalIncome(),

            // Container(
            //   padding: EdgeInsets.all(8.0),
            //   child: Center(
            //     child: Column(
            //       children: <Widget>[
            //         Text('Time spent on daily task'),
            //         SizedBox(
            //           height: 10.0,
            //         ),
            //         Expanded(
            //           child: charts.BarChart(
            //             _seriesBarData,
            //             animate: true,
            //             animationDuration: Duration(seconds: 5),
            //             barGroupingType: charts.BarGroupingType.grouped,
            //           ),
            //         )
            //       ],
            //     ),
            //   ),
            // ),
            //WeeklyTotalIncome(),
            MonthlyTotalIncome(),
            YearlyTotalIncome(),
          ],
        ),
      ),
    );
  }
}

class Task {
  String task;
  double taskvalue;
  Color colorval;

  Task(this.task, this.taskvalue, this.colorval);
}

class Pollution {
  String place;
  int year;
  int quantity;

  Pollution(this.place, this.year, this.quantity);
}

class Sales {
  int yearval;
  int salesval;

  Sales(this.yearval, this.salesval);
}
