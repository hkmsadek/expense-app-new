import 'package:expanse_manegment/Screens/Chart/Chart_TabbarBodyElements/Daily/Total_Expense/totalExpenseTabbar.dart';
import 'package:expanse_manegment/Screens/Chart/Chart_TabbarBodyElements/Daily/Total_Income/total_Income_Tabbar.dart';
import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:expanse_manegment/database/IncomeData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../../../main.dart';

class YearlyAllChart extends StatefulWidget {
  @override
  _YearlyAllChartState createState() => _YearlyAllChartState();
}

class _YearlyAllChartState extends State<YearlyAllChart> {
  bool toggle = false;
  String firstDate = "", today = "";
  Map<String, double> dataMap = new Map();
  List<Color> colorList = [
    Color(0XFF027A6C),
    Color(0XFFE54D25),
  ];

  List date = [];
  final IncomeDataHelper inhelper = new IncomeDataHelper();
  final ExpenseDataHelper exhelper = new ExpenseDataHelper();

  @override
  void initState() {
    setState(() {
      incomeTotal = 0;
      expenseTotal = 0;
      allTotal = 0;
      DateTime now = DateTime.now();
      today = DateFormat("yyyy").format(now);
    });

    incometotal(); // calculate income total
    expensetotal();

    super.initState();

    ////////// Calling Pie Chart //////////
    togglePieChart();
  }

  void incometotal() {
    print(firstDate);
    print(today);
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await inhelper.getYearlyNoteList(today);
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String inbalnc = _userAuthData[i].amount;
          double inbalanc = double.parse(inbalnc);
          print("inbalanc.toString()");
          print(inbalanc.toString());
          incomeTotal += inbalanc; //calculation
          print("incomeTotal");
          print(incomeTotal);
        });
      }
    });
  }

//////method for expense calculation/////
  void expensetotal() {
    print(firstDate);
    print(today);
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await exhelper.getYearlyNoteList(today);
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String exbalnc = _userAuthData[i].amount;
          double exbalanc = double.parse(exbalnc);
          print("exbalanc.toString()");
          print(exbalanc.toString());
          expenseTotal += exbalanc; //calculation
          print("expenseTotal");
          print(expenseTotal);
        });
      }

      allTotal = incomeTotal - expenseTotal;
      if (incomeTotal != 0.0) {
        dataMap.putIfAbsent("Total Income", () => incomeTotal.toDouble());
      }
      if (expenseTotal != 0.0) {
        dataMap.putIfAbsent("Total Expense", () => expenseTotal.toDouble());
      }
      print("dataMap");
      print(dataMap);
      if (incomeTotal == 0.0 && expenseTotal == 0.0) {
        setState(() {
          toggle = false;
        });
      } else {
        setState(() {
          toggle = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            toggle
                ? PieChart(
                    dataMap: dataMap,
                    legendFontColor: Colors.blueGrey[900],
                    legendFontSize: 14.0,
                    legendFontWeight: FontWeight.w500,
                    animationDuration: Duration(milliseconds: 800),
                    chartLegendSpacing: 32.0,
                    chartRadius: MediaQuery.of(context).size.width / 2.3,
                    showChartValuesInPercentage: true,
                    showChartValues: true,
                    showChartValuesOutside: false,
                    chartValuesColor: Colors.white,
                    colorList: colorList,
                    showLegends: false,
                    //decimalPlaces: 1,
                    showChartValueLabel: false,
                    chartValueFontSize: 14,
                    chartValueFontWeight: FontWeight.bold,
                    initialAngle: 0,
                  )
                : Center(child: Text("No data available!")),

            ////////  Showing Items List  ////////
            incomeTotal == 0
                ? Container()
                : GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TotalIncome()));
                    },
                    child: itemsColumn(
                        Color(0XFF027A6C), 'Total Income', '\$$incomeTotal')),
            expenseTotal == 0
                ? Container()
                : GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TotalExpense()));
                    },
                    child: itemsColumn(
                        Color(0XFFE54D25), 'Total Expanse', '\$$expenseTotal')),

            //////// Last Row  ( Balance )  ////////
            allTotal == 0
                ? Container()
                : Container(
                    margin: EdgeInsets.fromLTRB(15.0, 20.0, 12.0, 10.0),
                    padding: EdgeInsets.only(right: 8.0, left: 40.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Balance',
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 17,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '\$$allTotal',
                          style: TextStyle(
                            color: Color(0XFF7DC83C),
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Container itemsColumn(Color color, String title, String amount) {
    return Container(
      margin: EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 10.0),
      padding: EdgeInsets.only(right: 8.0, left: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  ///////   Color Container  ///////
                  Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      color: color,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  //////Spacing////
                  SizedBox(
                    width: 12.0,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      color: Theme.of(context).accentTextTheme.body1.color,
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Text(
                amount,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 15,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  void togglePieChart() {
    setState(() {
      toggle = !toggle;
    });
  }
}
