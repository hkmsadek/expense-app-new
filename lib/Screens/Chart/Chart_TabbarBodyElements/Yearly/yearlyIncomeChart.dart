import 'package:expanse_manegment/database/IncomeData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../../../main.dart';

class YearlyIncomeChart extends StatefulWidget {
  @override
  _YearlyIncomeChartState createState() => _YearlyIncomeChartState();
}

class _YearlyIncomeChartState extends State<YearlyIncomeChart> {
  bool toggle = false;
  String firstDate = "", today = "";
  Map<String, double> dataMap = new Map();
  List<Color> colorList = [
    Color(0XFFF78F1E),
    Color(0XFFED6D23),
    Color(0XFF027A6C),
    Color(0XFF9D9D36),
    Color(0XFFE54D25),
  ];
  List date = [];
  List allData = [];
  double inbalanc = 0;
  final IncomeDataHelper inhelper = new IncomeDataHelper();

  @override
  void initState() {
    setState(() {
      incomeTotal = 0;
      DateTime now = DateTime.now();
      today = DateFormat("yyyy").format(now);
    });

    incometotal();

    super.initState();

    ////////// Calling Pie Chart //////////
    togglePieChart();
  }

  void incometotal() {
    print(firstDate);
    print(today);
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await inhelper.getYearlyNoteList(today);
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String inbalnc = _userAuthData[i].amount;
          inbalanc = double.parse(inbalnc);
          incomeTotal += inbalanc;
          allData.add({
            'name': _userAuthData[i].selectCat,
            'amt': _userAuthData[i].amount,
          });
        });
      }

      if (incomeTotal != 0.0) {
        dataMap.putIfAbsent("Total Income", () => incomeTotal.toDouble());
      }
      if (incomeTotal == 0.0) {
        setState(() {
          toggle = false;
        });
      } else {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            toggle
                ? PieChart(
                    dataMap: dataMap,
                    legendFontColor: Colors.blueGrey[900],
                    legendFontSize: 14.0,
                    legendFontWeight: FontWeight.w500,
                    animationDuration: Duration(milliseconds: 800),
                    chartLegendSpacing: 32.0,
                    chartRadius: MediaQuery.of(context).size.width / 2.3,
                    showChartValuesInPercentage: true,
                    showChartValues: true,
                    showChartValuesOutside: false,
                    chartValuesColor: Colors.white,
                    colorList: colorList,
                    showLegends: false,
                    //decimalPlaces: 1,
                    showChartValueLabel: false,
                    chartValueFontSize: 14,
                    chartValueFontWeight: FontWeight.bold,
                    initialAngle: 0,
                  )
                : Center(child: Text("No data available!")),

            ////////  Showing Items List  ////////
            allData == null
                ? Container()
                : Container(
                    margin: EdgeInsets.only(top: 30, right: 20),
                    child: Column(
                      children: List.generate(allData.length, (int index) {
                        return itemsColumn(
                            Color(0XFFE54D25),
                            allData[index]['name'],
                            '\$${allData[index]['amt']}');
                      }),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Container itemsColumn(Color color, String title, String amount) {
    return Container(
      margin: EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 10.0),
      padding: EdgeInsets.only(right: 8.0, left: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  // Container(
                  //   height: 20,
                  //   width: 20,
                  //   decoration: BoxDecoration(
                  //     color: color,
                  //     borderRadius: BorderRadius.circular(20.0),
                  //   ),
                  // ),
                  //////Spacing////
                  SizedBox(
                    width: 12.0,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      color: Theme.of(context).accentTextTheme.body1.color,
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Text(
                amount,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  void togglePieChart() {
    setState(() {
      toggle = !toggle;
    });
  }
}
