import 'package:expanse_manegment/Screens/Category/Expense_Category_Page/expenseCategory.dart';
import 'package:expanse_manegment/Screens/Category/Income_Category_Page/incomeCategory.dart';
import 'package:flutter/material.dart';

enum WidgetMarker {
  income,
  expense,
 
  
}


class CategoryPage extends StatefulWidget {
///status change variable////
  final inStatus;
  CategoryPage(this.inStatus);

  
  
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
 
 WidgetMarker selectedWidgetMarker;
 @override
  void initState() {



    if(widget.inStatus==2){
      selectedWidgetMarker = WidgetMarker.expense;
    }
    else{
      selectedWidgetMarker = WidgetMarker.income;
    }
   
    super.initState();
    // selectedWidgetMarker = 
    // (widget.inStatus ==0 || widget.inStatus==1)? 
    // WidgetMarker.income: WidgetMarker.expense;
  }
   
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text("Category",
              // widget.val =="1"?"Category":"Sub Category",
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ), 
          ),
        ),
        body: Container(
        //  color: Theme.of(context).backgroundColor,
            //alignment: Alignment.centerLeft,
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                  padding:
                      EdgeInsets.only(top: 8, bottom: 0, left: 20, right: 20),
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0)),
                  ),
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                      child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: <Widget>[
                        widget.inStatus!=5? Container():Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(bottom: 5.0),
                              alignment: Alignment.center,
                              height: 50,
                              child: Container(
                                height: 30.5,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Color(0XFF1A3D7A), width: 2),
                                    borderRadius: BorderRadius.circular(5.0)),
                                alignment: Alignment.center,
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: 29,
                                      color: (selectedWidgetMarker ==
                                              WidgetMarker.income)
                                          ? Color(0XFF1A3D7A)
                                          : Theme.of(context).primaryColor,
                                      child: FlatButton(
                                        onPressed: () {
                                        if(widget.inStatus!=2){
                                            setState(() {
                                            selectedWidgetMarker = 

                                                WidgetMarker.income;
                                          });
                                          }

                                          return CircularProgressIndicator();
                                        },
                                        child: Text(
                                          "Income",
                                          textDirection: TextDirection.ltr,
                                          style: TextStyle(
                                            color: (selectedWidgetMarker ==
                                                    WidgetMarker.income)
                                                ? Color(0XFFFFFFFF)
                                                : Color(0XFF1A3D7A),
                                            fontSize: 16.0,
                                            decoration: TextDecoration.none,
                                            fontFamily: 'Roboto',
                                            fontWeight: (selectedWidgetMarker ==
                                                    WidgetMarker.income)
                                                ? FontWeight.bold
                                                : FontWeight.normal,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 29,
                                      color: (selectedWidgetMarker ==
                                              WidgetMarker.expense)
                                          ? Color(0XFF1A3D7A)
                                          : Theme.of(context).primaryColor,
                                      child: FlatButton(
                                        onPressed: () {
                                          if(widget.inStatus!=1){
                                            setState(() {
                                            selectedWidgetMarker = 

                                                WidgetMarker.expense;
                                          });
                                          }
                                          //_getDivData(false);
                                          return CircularProgressIndicator();
                                        },
                                        child: Text(
                                          "Expense",
                                          textDirection: TextDirection.ltr,
                                          style: TextStyle(
                                            color: (selectedWidgetMarker ==
                                                    WidgetMarker.expense)
                                                ? Color(0XFFFFFFFF)
                                                : Color(0XFF1A3D7A),
                                            fontSize: 16.0,
                                            decoration: TextDecoration.none,
                                            fontFamily: 'Roboto',
                                            fontWeight: (selectedWidgetMarker ==
                                                    WidgetMarker.expense)
                                                ? FontWeight.bold
                                                : FontWeight.normal,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(

                          child: widget.inStatus == 0 || widget.inStatus == 5? getCustomContainer() : getCustomContainer2(),
                        ),
                       
                      ],
                    ),
                  )),
                ))));
  }
////////// status change condition ////
  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.income:
        return getIncomeWidget();
      case WidgetMarker.expense:
        return getExpenseWidget();
    }
    return getIncomeWidget();
  }
   getCustomContainer2() {
    
    if(widget.inStatus == 1)
    {
      return getIncomeWidget();
    }
    if(widget.inStatus == 2)
    {
      return getExpenseWidget();
    }
////////// status change condition ////
    
  }

  Widget getIncomeWidget() {
    return Container(child: IncomeCategory(widget.inStatus));
  }

  Widget getExpenseWidget() {
    return Container(child: ExpenseCategory(widget.inStatus));
  }

  
}



