

import 'package:expanse_manegment/database/ExpenceCategoryhelper.dart';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../main.dart';

class ExpenseCategory extends StatefulWidget {
  final status;
  ExpenseCategory(this.status);
  @override
  _ExpenseCategoryState createState() => _ExpenseCategoryState();
}

class _ExpenseCategoryState extends State<ExpenseCategory> {
  final ExpenceHelper helper = new ExpenceHelper();
  final _categoryNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  ExCategory exCategory;
  List icon = [];
  String ic = "";
  int indx = 0;
  int icoindx = 0;
  int updateIndex; /////////////update category list///////

  List<ExCategory> categorylist; /////////////show category list///////
  bool isIcon = false;

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 50),
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: <Widget>[
           Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 8.0),
            child: GestureDetector(
              onTap: () {
                _showDialog();
              },
              child: Text(
                '+ Add Another Category',
                textDirection: TextDirection.ltr,
                style: TextStyle(
                    color: Theme.of(context).accentTextTheme.display3.color,
                    fontSize: 16.0,
                    decoration: TextDecoration.none,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            ////////show category in the another screen////
            child: Column(
              children: <Widget>[
                FutureBuilder(
                  future: helper.getExCategoryList(),
                  builder: (context, snapshot) {
                    categorylist = snapshot.data;
                    if (!snapshot.hasData)
                      return Center(child: CircularProgressIndicator());
//////////for scrolling use list.generate/////////
                    return Column(
                        key: _formKey,
                        children: List.generate(categorylist.length, (int index) {
                          ExCategory cat = categorylist[index];

                          int i = 0;
                          int len = -1;
                          //String ind = "${cat.exIconIndex}"; //icon index with string
                          String ind = cat.exIconIndex;
                          int intIndex = int.parse(ind); //string to integer
                          indx = intIndex;
                          print("intIndex");
                          //print(intIndex);
                          print("indx");
                          print(indx);
                          print("inds");
                          //print(ind);

                          return Slidable(
                            actionPane: SlidableDrawerActionPane(),
                            //delegate: new SlidableDrawerDelegate(),
                            actionExtentRatio: 0.20,
                            child: GestureDetector(
                              /////item call & select from list///
                              onTap: () {

                                if(widget.status!=5){
                                setState(() {
                                  categ = cat.expenseName;
                                  icn = cat.exIconIndex; ////global ver///
                                  icnInt = int.parse(icn);
                                  Navigator.pop(context);
                                });
                                }
                                /////item select from list///
                              },
                              child: Container(
                                  
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      // Divider(
                                      //   color: Color(0XFF707070).withOpacity(0.3),
                                      //   height: 8.0,
                                      // ),
                                      Container(
                                         //  height: 50,
                                      margin: EdgeInsets.only(bottom: 2),
                                      padding: EdgeInsets.only(top:5, bottom:5, left:10, right:10),
                                     decoration: BoxDecoration(
                                       color: Theme.of(context).backgroundColor,
                                         boxShadow: [BoxShadow(color: Colors.grey[200], blurRadius: 5)],
                                       
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 0.8,
                                                  color: Color(0XFF707070)
                                                      .withOpacity(0.2)))),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                                height: 17,
                                                width: 17,
                                                alignment: Alignment.center,
                                                child: Icon(
                                                  iconList[indx]['name'],

                                                  ///icon index call
                                                  color: Theme.of(context)
                                                      .selectedRowColor,
                                                  size: 20,
                                                )),
                                            Container(
                                              alignment: Alignment.centerLeft,
                                              padding: EdgeInsets.fromLTRB(
                                                  20.0, 15.0, 0.0, 8.0),
                                              child: Text(
                                                '${cat.expenseName}',
                                                textDirection: TextDirection.ltr,
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .accentTextTheme
                                                        .display3
                                                        .color,
                                                    fontSize: 16.0,
                                                    decoration: TextDecoration.none,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                            /////////slide edit delete////
                            secondaryActions: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: new IconSlideAction(
                                  // caption: 'Edit',
                                  color: Color(0XFFEFEFEF),
                                  //icon: Icons.edit,
                                  iconWidget: Icon(
                                    Icons.edit,
                                    color: Color(0XFF1A3D7A),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _categoryNameController.text =
                                          cat.expenseName;
                                      String ind = cat.exIconIndex;
                                      int id = int.parse(ind);

                                      ///string to int icon id///
                                      print("id");
                                      print(id);
                                      //string to integer
                                      icon.clear(); ////initially clear data//
                                      icon.add({
                                        'icon': iconList[id]['name']
                                      }); ////call icon index////
                                      print(iconList[index]['name'].toString());
                                      iconInd = index.toString();
                                      print("iconInd");
                                      print(iconInd);
                                      print(icon);
                                      print(icon.length);
                                      exCategory = cat;
                                      updateIndex = index;
                                      //int.parse(ind);

                                      _showDialog();
                                    });
                                  }, // _showSnackBar('More'),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: i == 0
                                      ? BorderRadius.only(
                                          topRight: Radius.circular(10.0),
                                        )
                                      : i == len
                                          ? BorderRadius.only(
                                              bottomRight: Radius.circular(10.0),
                                            )
                                          : BorderRadius.only(
                                              bottomRight: Radius.circular(0.0),
                                              topRight: Radius.circular(0.0)),
                                ),
                                child: new IconSlideAction(
                                  // caption: 'Delete',
                                  color: Colors.transparent,
                                  icon: Icons.delete,
                                  onTap: () {
                                    _showDeleteDialog(cat, index);
                                  },
                                ),
                              ),
                            ],
                          );
                        }));
                  },
                ),

                SizedBox(height:30)
              ],
            ),
            ////////show category in the another screen////
          ),

          //////////  Add Category Option Start  ///////////
          // Divider(
          //   color: Color(0XFF707070).withOpacity(0.3),
          // ),
         
        ],
      ),
    );
  }

  ///////Dialog ////////////////////
  void _showDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                backgroundColor: Color(0XFFF2F4F7),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 35.0),
                content: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                            padding:
                                EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
                            child: Container(
                                alignment: Alignment.center,
                                child: TextFormField(
                                  //key: _formKey,
                                  onChanged: (value) {
                                    setState(() {
                                      ic = value;
                                    });
                                  },
                                  controller: _categoryNameController,
                                  cursorColor: Color(0XFF1A3D7A),
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 12,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w500,
                                  ),
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Color(0XFF1A3D7A),
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0XFF1A3D7A),
                                            width: 1.0),
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                      ),
                                      labelText: 'Enter category name',
                                      labelStyle: TextStyle(
                                        color: Color(0XFF1A3D7A),
                                        fontSize: 12.0,
                                        decoration: TextDecoration.none,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.bold,
                                      ),
                                      hintText: '',
                                      hintStyle: TextStyle(
                                        color:
                                            Color(0XFF1A3D7A).withOpacity(0.6),
                                        fontSize: 14.0,
                                        decoration: TextDecoration.none,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.normal,
                                      ),
                                      border: OutlineInputBorder()),
                                ))),
                        GestureDetector(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => ChooseIcon()));
                            setState(() {
                              if (isIcon == false) {
                                isIcon = true;
                              }
                            });
                          },
                          child: (icon.length == 0)
                              ? Container(
                                  margin: EdgeInsets.only(left: 15, top: 15),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                          height: 17,
                                          width: 17,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                            color: Theme.of(context)
                                                .selectedRowColor,
                                          )),
                                          child: Icon(
                                            Icons.add,
                                            color: Theme.of(context)
                                                .selectedRowColor,
                                            size: 15,
                                          )),
                                      // GestureDetector(
                                      //   onTap: () {
                                      //     Navigator.push(
                                      //         context,
                                      //         MaterialPageRoute(
                                      //             builder: (context) => ChooseIcon()));
                                      //   },
                                      //   child:
                                      Container(
                                        padding: EdgeInsets.only(left: 7.0),
                                        child: Text(
                                          'Select Icon from below',
                                          style: TextStyle(
                                              color:
                                                  Theme.of(context).accentColor,
                                              fontFamily: 'Roboto',
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      // ),
                                    ],
                                  ),
                                )
                              : Icon(icon[0]['icon']),
                        ),
                        isIcon == false
                            ? Container()
                            : Container(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                                child: Wrap(
                                    ////////list generate for icon///////
                                    children: List.generate(
                                  iconList.length,
                                  (int index) {
                                    return GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            icon.clear(); ////initially clear data//
                                            icon.add({
                                              'icon': iconList[index]['name']
                                            }); ////then add icon from list///
                                            //Navigator.pop(context);
                                            print(iconList[index]['name']
                                                .toString());
                                            iconInd = index.toString();
                                            print("iconInd");
                                            print(iconInd);
                                            print(icon);
                                            print(icon.length);
                                            isIcon = false;
                                          });
                                        },
                                        child: Container(
                                          height: 40,
                                          width: 40,
                                          margin: EdgeInsets.all(10.0),
                                          //child: //icon,
                                          child: Icon(
                                            iconList[index][
                                                'name'], ///////icon list call////
                                            color:
                                                Theme.of(context).accentColor,
                                            size: 30,
                                          ),
                                        ));
                                  },
                                )
                                    ////////list generate for icon///////
                                    ),
                              ),
                        Container(
                          padding: EdgeInsets.only(top: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ///////// Cancle Button Start /////
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    icon.clear();
                                    _categoryNameController.text = "";
                                    isIcon = false;
                                  });
                                  Navigator.of(context).pop();
                                },
                                // color: Color(0XFFF2F4F7),
                                // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                child: Container(
                                  padding: EdgeInsets.only(left: 12, right: 12),
                                  child: Text(
                                    'Cancel',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1C3F7B),
                                      fontSize: 14.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              ///////// Cancle Button end /////

                              SizedBox(
                                width: 18.0,
                              ), ///// Spacing /////

                              ///////// Ok Button Start /////
                              GestureDetector(
                                onTap: () {
                                  _okCategory(context);
                                  setState(() {
                                    isIcon = false;
                                  });
                                  Navigator.of(context).pop();
                                },
                                // color: Color(0XFFF2F4F7),
                                // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                child: Container(
                                  padding: EdgeInsets.only(left: 25, right: 25),
                                  child: Text(
                                    'OK',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1C3F7B),
                                      fontSize: 14.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),

                              ///////// Ok Button end /////
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  void _okCategory(BuildContext context) {
    if (_categoryNameController.text.isEmpty) {
      return _showMsg('Category should not empty');
    }

    if (exCategory == null) {

       iconInd==""?iconInd ="0":iconInd;
      ExCategory cat = new ExCategory(
          expenseName: _categoryNameController.text, exIconIndex: iconInd);
      helper
          .insertExCategory(cat)
          .then((id) => {_categoryNameController.clear(), icon.clear()});

          setState(() {
            _categoryNameController.text="";
            iconInd = "";
          });
    } else {
      exCategory.expenseName = _categoryNameController.text;
      exCategory.exIconIndex = iconInd;

      helper.updateExCategory(exCategory).then((id) => {
            setState(() {
              categorylist[updateIndex].expenseName =
                  _categoryNameController.text;
              icon.clear();
            }),
            _categoryNameController.clear(),
            icon.clear(),
            exCategory = null
          });
    }
  }

  ///////Dialog ////////////////////
  void _showDeleteDialog(cat, index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Container(
                        child: new Text(
                          "Do you want to delete this transaction?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0XFF1A3D7A),
                            fontSize: 19.0,
                            decoration: TextDecoration.none,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    //color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ///////// Cancel Button Start /////
                        Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                color: Colors.white),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 12, right: 12),
                                child: Text(
                                  'Cancel',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Colors.white,
                              elevation: 2.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            )),

                        ///////// Cancle Button end /////

                        ///////// Ok Button Start /////
                        Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              color: Color(0XFFFB7676),
                            ),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                helper.deleteExCategory(cat.id);
                                setState(() {
                                  categorylist.removeAt(index);
                                  Navigator.of(context).pop();
                                });
                              },

                              child: Container(
                                padding: EdgeInsets.only(left: 25, right: 25),
                                child: Text(
                                  'Ok',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Color(0XFFFB7676),
                              elevation: 2.0,
                              //splashColor: Colors.blueGrey,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),

                        ///////// Ok Button end /////
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
