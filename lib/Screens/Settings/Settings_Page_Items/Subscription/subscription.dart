import 'package:flutter/material.dart';

class Subscription extends StatefulWidget {
  @override
  _SubscriptionState createState() => _SubscriptionState();
}

class _SubscriptionState extends State<Subscription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Subscription',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            /////////// Image Container /////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 20.0, 8.0, 0.0),
                              height:
                                  (MediaQuery.of(context).size.height / 2) - 120,
                              width: (MediaQuery.of(context).size.width / 1.7),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      // alignment: Alignment.center,
                                      image: AssetImage(
                                          'assets/img/subscribe2.png'),
                                      fit: BoxFit.fill)),
                            ),

                            /////////// Text Container /////////
                            Container(
                              margin: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 12.0),
                              child: Text("Stay tuned !",
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Theme.of(context).accentColor,
                                      fontFamily: 'Roboto',
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.normal)),
                            ),

                            /////////// Text Container /////////
                            Container(
                              child: Text(
                                  "Renew Your Subscriptions with Paybacks And Discount ",
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Theme.of(context).accentColor,
                                      fontFamily: 'Roboto',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                            ),

                            /////////// Button Container /////////
                            Container(
                              margin: EdgeInsets.only(top: 50, bottom: 20.0),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              child: Card(
                                color: Color(0XFFFFFFFF),
                                elevation: 3,
                                child: FlatButton(
                                  color: Color(0XFFFFFFFF),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text('Get Started',
                                          overflow: TextOverflow.clip,
                                          // textAlign: TextAlign.center,
                                          style: TextStyle(
                                              decoration: TextDecoration.none,
                                              color: Color(0XFF234481),
                                              fontFamily: 'Roboto',
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold)),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Icon(
                                        Icons.send,
                                        color: Color(0XFF234481),
                                        size: 17,
                                      ),
                                    ],
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )))));
  }
}
