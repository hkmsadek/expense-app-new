import 'package:flutter/material.dart';

class UpdateNotification extends StatefulWidget {
  @override
  _UpdateNotificationState createState() => _UpdateNotificationState();
}

class _UpdateNotificationState extends State<UpdateNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Notification',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 15, right: 15),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Column(
                          children: <Widget>[
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                            buildCard('Update Alert ',
                                'You have an update, Download Now.'),
                          ],
                        ),
                      ),
                    )))));
  }

  Container buildCard(String title, String subtitle) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(3.0, 2.0, 3.0, 2.0),
      child: Card(
        color: Theme.of(context).cardColor,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //////////////////// Text Container ///////////////////
            Container(
              padding: EdgeInsets.fromLTRB(15.0, 14.0, 8.0, 0.0),
              child: Text(title,
                  // 'Update Alert ',
                  style: TextStyle(
                    color: Theme.of(context).accentTextTheme.body2.color,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                    fontFamily: 'Roboto',
                  )),
            ),

            //////////////////// Text Container ///////////////////
            Container(
              padding: EdgeInsets.fromLTRB(15.0, 10.0, 8.0, 14.0),
              child: Text(subtitle,

                  ///'You have an update, Download Now.',
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Theme.of(context).accentTextTheme.display2.color,
                    fontWeight: FontWeight.normal,
                    fontSize: 16.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
