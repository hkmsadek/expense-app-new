import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/General/Update/updateNotification.dart';
import 'package:flutter/material.dart';

class Update extends StatefulWidget {
  @override
  _UpdateState createState() => _UpdateState();
}

class _UpdateState extends State<Update> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Update',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Card(
                          color: Theme.of(context).cardColor,
                          elevation: 3.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            height: 320,
                            padding: EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 0.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                //////////////////// Text Container ///////////////////
                                Container(
                                  padding:
                                      EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                                  child: Text('New Update Available ',
                                      style: TextStyle(
                                        //color: Theme.of(context).accentTextTheme.caption.color,
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0,
                                        fontFamily: 'Roboto',
                                      )),
                                ),

                                //////////////////// Text Container ///////////////////
                                Container(
                                  padding:
                                      EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10.0),
                                  child: Text('Version 1.1.0',
                                      overflow: TextOverflow.clip,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        //color: Theme.of(context).accentTextTheme.caption.color,
                                        color: Theme.of(context).accentTextTheme.headline.color,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16.0,
                                        fontFamily: 'Roboto',
                                        height: 1.7,
                                      )),
                                ),

                                ////////////////////// Image Container /////////////////////
                                Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.fromLTRB(8.0, 3.0, 8.0, 0.0),
                                  height: 120,
                                  width: 120,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          // alignment: Alignment.center,
                                          image:
                                              AssetImage('assets/img/update.png'),
                                          fit: BoxFit.fill)),
                                ),

                                ///////////////////// Button Container /////////////////////
                                Container(
                                  margin: EdgeInsets.only(top: 18, bottom: 20.0),
                                  child: Card(
                                    color: Theme.of(context).backgroundColor,
                                    elevation: 3,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    child: FlatButton(
                                      color: Theme.of(context).backgroundColor,
                                      child: Text('Update Now',
                                          overflow: TextOverflow.clip,
                                          // textAlign: TextAlign.center,
                                          style: TextStyle(
                                              decoration: TextDecoration.none,
                                              color: Theme.of(context).accentColor,
                                              fontFamily: 'Roboto',
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold)),
                                      onPressed: () {
                                        Navigator.push(context,
                                        MaterialPageRoute(builder: (context) => UpdateNotification()));
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )))));
  }
}
