import 'package:country_currency_pickers/country.dart';
import 'package:country_currency_pickers/currency_picker_dialog.dart';
import 'package:country_currency_pickers/utils/utils.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/General/Update/update.dart';
import 'package:expanse_manegment/Theme_Settings/theme_notifier.dart';
import 'package:expanse_manegment/Theme_Settings/values.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GeneralPage extends StatefulWidget {
  @override
  _GeneralPageState createState() => _GeneralPageState();
}

class _GeneralPageState extends State<GeneralPage> {
  TextEditingController pinController = TextEditingController();

  bool hideRecentAccountValue = false;
  bool notificationValue = false;
  bool isCheck = false;
  var _darkTheme = true;
  String theme = 'Light';

  List<String> _months = [
    'Monthly',
    'Weekly',
    'Yearly',
  ];
  String _currentMonthSelected = 'Monthly';
  List<String> _days = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28',
    '29',
    '30'
  ];
  String _currentDaySelected = '1';

  bool pinValueSwitch = false;

  void onPINValueChanged(bool value) {
    setState(() {
      pinValueSwitch = value;
      if (pinValueSwitch == true) {
        _showSetPINDialog();
      }
    });
  }

/////////////   Currency Setting Start  ///////////////
  bool isOpen = false;
  // String countryName = "US";
  // String countryPhoneCode = "+1";
  String currencyCode = "BDT";
  Country _selectedDialogCountry =
      CountryPickerUtils.getCountryByCurrencyCode('BDT');

  Widget _buildCurrencyDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          isOpen == true
              ? Flexible(
                  child: Text(
                  country.currencyCode,
                  style: TextStyle(
                      color: Theme.of(context).accentTextTheme.subtitle.color,
                      fontFamily: 'Arial',
                      fontSize: 14),
                ))
              : Text(
                  currencyCode,
                  style: TextStyle(
                      color: Theme.of(context).accentTextTheme.subtitle.color,
                      fontFamily: 'Arial',
                      fontSize: 14),)
        ],
      );

  void _openCurrencyPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CurrencyPickerDialog(
                titlePadding: EdgeInsets.all(8.0),
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(hintText: 'Search...', 
                ),
                isSearchable: true,
                title: Text('Select your Currency',
                style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w400,
                      fontSize: 20),
                ),
                onValuePicked: (Country country) =>
                    setState(() => _selectedDialogCountry = country),
                itemBuilder: _buildCurrencyDialogItem)),
      );
  ///////////////   Currency Setting End  //////////////////

  @override
  Widget build(BuildContext context) {

    final themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                  color: Theme.of(context).primaryIconTheme.color,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('General',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
          child: Container(
            decoration:
                BoxDecoration(color: Theme.of(context).bottomAppBarColor),
            child: Container(
              padding:
                  EdgeInsets.only(top: 12, bottom: 0.0, left: 20, right: 20),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0)),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    //////  Start  Calling  Items For List  ////////////

                    ///////////////////////////////// Item Theme Start/////////////////////////////
                    Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 0.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.all(2.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        //alignment: Alignment.center,
                                        padding: EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          'Theme',
                                          style: TextStyle(
                                              color:
                                                  Theme.of(context).accentColor,
                                              fontFamily: 'Roboto',
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 2.0),
                                        child: Text(
                                          '$theme',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .accentTextTheme
                                                  .subtitle
                                                  .color,
                                              fontFamily: 'Arial',
                                              fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              /////////////////// Switch Button ////////////////
                              Container(
                                alignment: Alignment.topCenter,
                                child: Switch(
                                  value: _darkTheme,
                                  activeColor: Colors.white,
                                  activeTrackColor: Color(0XFF1A3D7A),
                                  inactiveThumbColor: Colors.white,
                                  inactiveTrackColor: Colors.grey,
                                  onChanged: (val) {
                                    setState(() {
                                      _darkTheme = val;
                                      if (_darkTheme == true) {
                                        theme = 'Dark';
                                      } else
                                        theme = 'Light';
                                    });
                                    onThemeChanged(val, themeNotifier);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          color: Color(0XFF707070).withOpacity(0.3),
                          height: 10.0,
                        ),
                      ],
                    ),
                    ////////////////////////////// Item Theme End ///////////////////////////////

                    ///////////////////////////////// Item Currency Start /////////////////////////////
                    GestureDetector(
                      onTap: () {
                        _openCurrencyPickerDialog();
                        setState(() {
                          isOpen = true;
                        });
                      },
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 0.0),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.all(2.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          //alignment: Alignment.center,
                                          padding: EdgeInsets.only(bottom: 8.0),
                                          child: Text(
                                            //'Profile',
                                            'Currency',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                fontFamily: 'Roboto',
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 2.0),
                                          child: _buildCurrencyDialogItem(
                                              _selectedDialogCountry),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            color: Color(0XFF707070).withOpacity(0.3),
                            height: 10.0,
                          ),
                        ],
                      ),
                    ),
                    ////////////////////////////// Item Currency End //////////////////////////////

                    /////////////////////////////////// Item Grouping Start //////////////////////////////
                    GestureDetector(
                        onTap: () {
                          /////////  Show AlertDialog on Tap  ///////////
                          _showDateDialog();
                        },
                        child: itemList(
                          'Grouping',
                          'Monthly',
                          Color(0XFF707070).withOpacity(0.3),
                        )),
                    ///////////////////////////// Item Grouping End ///////////////////////////////
                    /////////////////////////////// Item Set PIN Start ///////////////////////////////
                    itemSetPIN(
                      'Set PIN',
                      'Set pin to protect your data',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                    ///////////////////////////////// Item Set PIN End ///////////////////////////////////

                    //////////////////// Item Regular Expense And Income Start //////////////////////////
                    itemList(
                      'Regular Expense And Income',
                      '',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                    ////////////////////////////// Item Regular Expense And Income End ///////////////

                    ///////////////////////////// Item First Day Of Week Start //////////////////////////////
                    itemList(
                      'First Day Of Week',
                      'Sunday',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                    ///////////////////////////// Item First Day Of Week End /////////////////////////////

                    ///////////////////////////// Item Backup/Restore Data Start /////////////////////////////
                    itemList(
                      'Backup/Restore Data',
                      '',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                    ///////////////////////////// Item Backup/Restore Data End /////////////////////////////

                    ///////////////////////////// Item Hide Recent Account Start /////////////////////////////
                    Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 5.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  padding:
                                      EdgeInsets.only(bottom: 8.0, left: 2.0),
                                  child: Text(
                                    'Hide Recent Account ',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontFamily: 'Roboto',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Theme(
                                data: ThemeData(
                                  unselectedWidgetColor: Color(0XFFFFFFFF),
                                ),
                                child: Checkbox(
                                  checkColor: Color(0XFF294F95),
                                  activeColor: Color(0XFFFFFFFF),
                                  tristate: false,
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  value: hideRecentAccountValue,
                                  onChanged: (bool value) {
                                    setState(() {
                                      hideRecentAccountValue = value;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          color: Color(0XFF707070).withOpacity(0.3),
                          height: 10.0,
                        ),
                      ],
                    ),
                    /////////////////////////// Item Hide Recent Account End ////////////////////////////

                    //////////////////////////////// Item Show Recent Update Start ///////////////////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Update()));
                      },
                      child: itemList(
                        'Show Recent Update',
                        '',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////////////// Item Show Recent Update End /////////////////////////////

                    ////////////////////////// Item Show Notification Start /////////////////////////
                    Container(
                      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 12.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 8.0, left: 2.0),
                              child: Text(
                                'Show Notifications ',
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Theme(
                            data: ThemeData(
                              unselectedWidgetColor: Color(0XFFFFFFFF),
                            ),
                            child: Checkbox(
                              checkColor: Color(0XFF294F95),
                              activeColor:
                                  isCheck ? Color(0XFFFFFFFF) : Colors.red,
                              tristate: false,
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              value: notificationValue,
                              onChanged: (bool value) {
                                setState(() {
                                  isCheck = true;
                                  notificationValue = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    //////////////////////// Item Show Notification End ///////////////////////////
                  ],
                ),
              ),
            ),
          ),
        ));
  }

/////////////////////////////  Method itemList  ///////////////////////////////////////
  Column itemList(String title, String subTitle, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 0.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(2.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        //alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          //'Profile',
                          title,
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontFamily: 'Roboto',
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 2.0),
                        child: Text(
                          //'Profile',
                          subTitle,
                          style: TextStyle(
                              color: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              fontFamily: 'Arial',
                              fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: color,
          height: 10.0,
        ),
      ],
    );
  }


/////////////////////////////  Method itemSetPIN  ///////////////////////////////////////
  Column itemSetPIN(String title, String subTitle, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 0.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(2.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        //alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          title,
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontFamily: 'Roboto',
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 2.0),
                        child: Text(
                          subTitle,
                          style: TextStyle(
                              color: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              fontFamily: 'Arial',
                              fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              /////////////////// Switch Button ////////////////
              Container(
                alignment: Alignment.topCenter,
                // color: Colors.red,
                child: Switch(
                  onChanged: onPINValueChanged,
                  value: pinValueSwitch,
                  activeColor: Colors.white,
                  activeTrackColor: Color(0XFF1A3D7A),
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.grey,
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: color,
          height: 10.0,
        ),
      ],
    );
  }

/////////////////////////////  Show Dialog for PIN Selection Start  ///////////////////////////////////////
    void _showSetPINDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0XFFF2F4F7),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            content: SingleChildScrollView(
              child: Container(
                height: 170,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Icon(
                        Icons.lock_open,
                        size: 34,
                        color: Color(0XFF284E94),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text('Choose a PIN',
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Color(0XFF284E94),
                              fontFamily: 'Roboto',
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text('Enter Your PIN to protect Your Records',
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Color(0XFF284E94),
                              fontFamily: 'Roboto',
                              fontSize: 13.0,
                              fontWeight: FontWeight.bold)),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextField(
                          cursorColor: Color(0XFF284E94),
                          controller: pinController,
                          obscureText: true,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0XFF284E94),
                            fontSize: 22,
                            fontFamily: 'Roboto',
                          ),
                          decoration: new InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0XFF284E94)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0XFF284E94)),
                            ),
                            hintText: ' Type PIN Number',
                            hintStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 13,
                              fontFamily: 'Roboto',
                            ),
                            fillColor: Colors.grey[100],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
/////////////////////////////  Show Dialog for PIN Selection End  ///////////////////////////////////////

/////////////////////////////  Show Dialog for Date Selection Start  ///////////////////////////////////////
  void _showDateDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0XFFF2F4F7),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            content: Container(
              height: 170,
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 15.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //////  Text Container  //////
                  Container(
                    child: Text(
                      "Cycle ",
                      style: TextStyle(
                        color: Color(0XFF294F95),
                        fontSize: 14.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  //////  Text Container  //////

                  ///////   Dropdown Button For MonthSelection Start  ////////
                  Container(
                      padding: EdgeInsets.only(right: 25.0),
                      child: DropdownButton<String>(
                        value: _currentMonthSelected,
                        iconSize: 30,
                        iconEnabledColor: Color(0XFF294F95),
                        iconDisabledColor: Color(0XFF294F95),
                        isExpanded: true,
                        //isDense: true,
                        underline: Container(
                          height: 2,
                          color: Color(0XFFF2F4F7),
                        ),

                        onChanged: (String newValue) {
                          _onDropdownSelectedMonth(newValue);
                        },
                        items: _months
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value,
                                style: TextStyle(
                                    color: Color(0XFF294F95),
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                          );
                        }).toList(),
                      )),
                  ///////   Dropdown Button For MonthSelection End ////////

                  ///////   Text Container  ////////
                  Container(
                    child: Text("Starting Date",
                        style: TextStyle(
                            color: Color(0XFF294F95),
                            fontFamily: 'Roboto',
                            fontSize: 14,
                            fontWeight: FontWeight.normal)),
                  ),
                  ///////   Text Container  ////////

                  ///////   Dropdown Button For DaySelection Start ////////
                  Container(
                    padding: EdgeInsets.only(right: 25.0, left: 3.0),
                    child: DropdownButton<String>(
                      value: _currentDaySelected,
                      iconSize: 30,
                      iconEnabledColor: Color(0XFF294F95),
                      iconDisabledColor: Color(0XFF294F95),
                      elevation: 16,
                      isExpanded: true,
                      underline: Container(
                        height: 2,
                        color: Color(0XFFF2F4F7),
                      ),
                      onChanged: (String newValue) {
                        _onDropdownSelectedDay(newValue);
                      },
                      items:
                          _days.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value,
                              style: TextStyle(
                                  color: Color(0XFF294F95),
                                  fontFamily: 'Roboto',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold)),
                        );
                      }).toList(),
                    ),
                  ),
                  ///////   Dropdown Button For DaySelection End ////////
                ],
              ),
            ),
          );
        });
  }
/////////////////////////////  Show Dialog for Date Selection End  ///////////////////////////////////////


  void _onDropdownSelectedMonth(String newValue) {
    setState(() {
      this._currentMonthSelected = newValue;
      Navigator.pop(context);
      _showDateDialog();
    });
  }

  void _onDropdownSelectedDay(String newValue) {
    setState(() {
      this._currentDaySelected = newValue;
      Navigator.pop(context);
      _showDateDialog();
    });
  }

  void onThemeChanged(bool value, ThemeNotifier themeNotifier) async {
    (value)
        ? themeNotifier.setTheme(darkTheme)
        : themeNotifier.setTheme(lightTheme);
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool('darkMode', value);
  }
}
