import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Privacy extends StatefulWidget {
  @override
  _PrivacyState createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Privacy',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        child: SingleChildScrollView(
                          child: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.fromLTRB(3.0, 10.0, 3.0, 10.0),
                            child: Card(
                              color: Colors.white,
                              elevation: 3.0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 0.0),
                                    child:
                                        Text('Agree to use our privacy policy ',
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                              fontFamily: 'Roboto',
                                            )),
                                  ),

                         ////////////////////// Image Container /////////////////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 20.0, 8.0, 0.0),
                              height:
                                  (MediaQuery.of(context).size.height / 2) - 130,
                              width: (MediaQuery.of(context).size.width / 1.7),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      // alignment: Alignment.center,
                                      image: AssetImage(
                                          'assets/img/Privacy2.png'),
                                      fit: BoxFit.fill)),
                            ),

                                  // Container(
                                  //   alignment: Alignment.center,
                                  //   child: Icon(
                                  //     Icons.security,
                                  //     size: 150,
                                  //     color: Color(0XFF2F56A1),
                                  //   ),
                                  // ),

                            //////////////////// Text Container ///////////////////
                                  Container(
                                      padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.start,                                            children: <Widget>[
                                              Icon(
                                                Icons.bookmark,
                                                color: Color(0XFF2E55A0),
                                                size: 18,
                                              ),
                                              SizedBox(width: 2.0,),
                                              Expanded(
                                                child: Text(
                                                    'AppifyLab Ltd. is committed to protecting',
                                                    overflow: TextOverflow.clip,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      //color: Theme.of(context).accentTextTheme.caption.color,
                                                      color: Color(0XFF1D3766),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontSize: 12.0,
                                                      fontFamily: 'Roboto',
                                                      height: 1.7,
                                                    )),
                                              )
                                            ],
                                          ),
                                          Text(
                                              "the privacy of personal information (i.e. any information relating to an identified or identifiable natural person) who visit the https://appifylab.com website and use the services available thereon (the “Services”). Amendments to this Privacy Policy will be posted to the Site and/or Services and will be effective when posted. Your continued use of the Site and/or Services following the posting of any amendment to this Privacy Policy shall constitute your acceptance of such amendment.",
                                              overflow: TextOverflow.clip,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                //color: Theme.of(context).accentTextTheme.caption.color,
                                                color: Color(0XFF1D3766),
                                                fontWeight: FontWeight.normal,
                                                fontSize: 12.0,
                                                fontFamily: 'Roboto',
                                                height: 1.7,
                                              )),
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ))))));
  }
}
