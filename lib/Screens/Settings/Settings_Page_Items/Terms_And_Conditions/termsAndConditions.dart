import 'package:flutter/material.dart';

class TermsAndConditions extends StatefulWidget {
  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                  //color: Color(0XFFF2F4F7),
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Terms And Conditions',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: SingleChildScrollView(
                        child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 10.0),
                          child: Card(
                            color: Colors.white,
                            elevation: 3.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  padding:
                                      EdgeInsets.fromLTRB(10.0, 22.0, 10.0, 12.0),
                                  child: Text('Terms and Conditions',
                                      style: TextStyle(
                                        //color: Theme.of(context).accentTextTheme.caption.color,
                                        color: Color(0XFF1E3869),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0,
                                        fontFamily: 'Roboto',
                                      )),
                                ),
                                Container(
                                  padding:
                                      EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 25.0),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                                        child: Text(
                                            "Please read these Terms of Service (\"Terms\") carefully before using the “Expense Management” mobile application or website (the \"Service\") operated by AppifyLab – Mobile Solutions. (\"Us\", \"We\", or \"Our\").",
                                            overflow: TextOverflow.clip,
                                            // textAlign: TextAlign.center,
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0,
                                              fontFamily: 'Roboto',
                                              height: 1.5,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                                        child: Text(
                                            "Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.",
                                            overflow: TextOverflow.clip,
                                            // textAlign: TextAlign.center,
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0,
                                              fontFamily: 'Roboto',
                                              height: 1.5,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                                        child: Text(
                                            "BY ACCESSING OR USING THE SERVICE YOU AGREE TO BE BOUND BY THESE TERMS. IF YOU DISAGREE WITH ANY PART OF THE TERMS THEN YOU MAY NOT ACCESS THE SERVICE.",
                                            overflow: TextOverflow.clip,
                                            // textAlign: TextAlign.center,
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12.0,
                                              fontFamily: 'Roboto',
                                              height: 1.5,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                                        child: Text(
                                            "Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.Your access to and use of the Service is conditioned on Your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.",
                                            overflow: TextOverflow.clip,
                                            // textAlign: TextAlign.center,
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0,
                                              fontFamily: 'Roboto',
                                              height: 1.5,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                                        child: Text(
                                            "Your access to and use of the Service is conditioned on Your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.Your access to and use of the Service is conditioned on Your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.",
                                            overflow: TextOverflow.clip,
                                            // textAlign: TextAlign.center,
                                            style: TextStyle(
                                              //color: Theme.of(context).accentTextTheme.caption.color,
                                              color: Color(0XFF1D3766),
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0,
                                              fontFamily: 'Roboto',
                                              height: 1.5,
                                            )),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )))));
  }
}
