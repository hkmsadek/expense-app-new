import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('About',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            /////////// Image Container /////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 60.0, 8.0, 12.0),
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      // alignment: Alignment.center,
                                      image: AssetImage('assets/img/about.png'),
                                      fit: BoxFit.fill)),
                            ),
                            /////////// Text Container /////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 8.0),
                              child: Text("Expense Management",
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .headline
                                          .color,
                                      fontFamily: 'Roboto',
                                      fontSize: 24.0,
                                      fontWeight: FontWeight.bold)),
                            ),
                            /////////// Text Container /////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                              child: Text("Version 1.0.0",
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .headline
                                          .color,
                                      fontFamily: 'Roboto',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                            ),
                            /////////// Text Container /////////
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                              child: Text("Income-Expense Management System",
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .headline
                                          .color,
                                      fontFamily: 'Roboto',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                      ),
                    )))));
  }
}
