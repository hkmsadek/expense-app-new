import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/addpro.dart';
import 'package:expanse_manegment/customPlugin/routeTransition/routeAnimation.dart';
import 'package:expanse_manegment/database/ProfilePic.dart';
import 'package:expanse_manegment/database/RegistrationData.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:image_picker/image_picker.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  RegistrationHelper helper = new RegistrationHelper();
  PictureHelper picHelper = new PictureHelper();
  String userName = "",
      fname = "",
      lname = "",
      email = "",
      mobile = "",
      password = "",
      picture = "",
      uId = "",
      image = "";
  int id = 0;
  Uint8List bytes;
  var profileImage;
  File fileImage;

  @override
  void initState() {
    _getUserInfo();
    check();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('user');

    if (user != null) {
      List us = user.split("~");
      setState(() {
        userName = us[3];
      });
    }
  }

  void check() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData1 = await helper.getProfile(userName);

      print("_userAuthData1.length");
      print(_userAuthData1.length);

      for (int j = 0; j < _userAuthData1.length; j++) {
        Registration reg = _userAuthData1[j];

        fname = reg.firstName;
        lname = reg.lastName;
        email = reg.email;
        mobile = reg.mobile;
        password = reg.password;
        id = reg.id;
      }
    });

    print("fname + " " + lname + " " + id");
    print(fname + " " + lname + " " + "$id");

    pictureGet();
  }

  void pictureGet() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData2 = await picHelper.getPicture("$userName");

      print("_userAuthData2.length");
      print(_userAuthData2.length);

      for (int j = 0; j < _userAuthData2.length; j++) {
        Picture pic = _userAuthData2[j];

        picture = pic.pic;
      }

      setState(() {
        bytes = Base64Decoder().convert(picture);
      });
    });
  }

  pickImagefromGallery() async {
    profileImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (profileImage != null) {
      setState(() {
        fileImage = profileImage;
        if (fileImage != null) {
          List<int> imageBytes = fileImage.readAsBytesSync();
          image = base64.encode(imageBytes);
          //image = image;

          print(image);
        }

        Picture pic = new Picture(
          pic: image,
          userName: userName,
        );
        //insert query
        picHelper.insertPicture(pic).then((id) => {});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Theme.of(context).bottomAppBarColor,
          titleSpacing: 2,
          automaticallyImplyLeading: false,
          leading: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: GestureDetector(
              onTap: Navigator.of(context).pop,
              child: Icon(
                Icons.arrow_back,
                size: 28.0,
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: new Icon(
                Icons.edit,
                size: 30,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    SlideLeftRoute(
                        page: AddPro(
                            fname, lname, userName, email, mobile, password)));
              }, // => _scaffoldKey.currentState.openDrawer()
            ),
          ],
          title: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text('Profile',
                style: TextStyle(
                  color: Color(0XFFF2F4F7),
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  fontFamily: 'Roboto',
                )),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Color(0XFFF2F4F7),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    /////////  Profile Picture Container  ////////
                    Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.topCenter,
                      color: Theme.of(context).bottomAppBarColor,
                      child: Container(
                        height: 180,
                        width: 180,
                        child: ClipOval(
                          child: fileImage != null
                              ? Image.file(
                                  fileImage,
                                  fit: BoxFit.cover,
                                )
                              : picture == ""
                                  ? Image.asset(
                                      'assets/img/profile.png',
                                      height: 180,
                                      width: 180,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.memory(
                                      bytes,
                                      fit: BoxFit.cover,
                                    ),
                        ),
                      ),
                      // child: Container(
                      //   height: 190,
                      //   width: 190,
                      //   decoration: BoxDecoration(
                      //       color: Colors.white38,
                      //       borderRadius:
                      //           BorderRadius.all(Radius.circular(190.0)),
                      //       image: DecorationImage(
                      //           image: AssetImage('assets/img/man.png'),
                      //           fit: BoxFit.fill)),
                      // ),
                    ),

                    //////////  Add Picture Container  ////////////
                    Positioned(
                        left: (MediaQuery.of(context).size.width / 1.78),
                        top: 140,
                        child: GestureDetector(
                          onTap: () {
                            pickImagefromGallery();
                          },
                          child: Container(
                            //padding: EdgeInsets.all(5.0),
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Color(0XFF294F95),
                                border: Border.all(
                                    color: Color(0XFFFFFFFF), width: 3.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0))),
                            child: Icon(
                              Icons.add_photo_alternate,
                              color: Color(0XFFFFFFFF),
                              size: 26,
                            ),
                          ),
                        ))
                  ],
                ),

                //////////  Elements Container  ////////////
                Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Theme.of(context).bottomAppBarColor),
                    child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(
                            top: 12, bottom: 0.0, left: 20, right: 20),
                        decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25.0),
                              topRight: Radius.circular(25.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.all(3.0),
                          padding: EdgeInsets.only(top: 5.0),
                          child: Column(
                            children: <Widget>[
                              FutureBuilder(
                                future: helper.getProfile(userName),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData)
                                    return Center(
                                        child: CircularProgressIndicator());
                                  String fn = "", ln = "";
                                   String phone = "";
                                     String email = "";
                                  for (int i = 0;
                                      i < snapshot.data.length;
                                      i++) {
                                    Registration reg = snapshot.data[i];
                                      

                                   

                                    fn = reg.firstName;
                                    ln = reg.lastName;
                                    phone = reg.mobile;
                                     email = reg.email;
                                  }

                                  print(fn);
                                  print(ln);

                                  return Column(
                                    children: <Widget>[
                                      _buildColumn(
                                          Icon(
                                            Icons.person,
                                            color: Color(0XFFFFFFFF),
                                            size: 20,
                                          ),
                                          'Name',
                                          '$fn $ln'),
                                          _buildColumn(
                                      Icon(
                                        Icons.phone,
                                        color: Color(0XFFFFFFFF),
                                        size: 20,
                                      ),
                                      'Phone',
                                      '$phone'),
                                      _buildColumn(
                                      Icon(
                                        Icons.email,
                                        color: Color(0XFFFFFFFF),
                                        size: 20,
                                      ),
                                      'Email',
                                      '$email')
                                    ],
                                  );
                                },
                              ),
                              // FutureBuilder(
                              //   future: helper.getProfile(userName),
                              //   builder: (context, snapshot) {
                              //     if (!snapshot.hasData)
                              //       return Center(
                              //           child: CircularProgressIndicator());
                              //     String phone = "";
                              //     for (int i = 0;
                              //         i < snapshot.data.length;
                              //         i++) {
                              //       Registration reg = snapshot.data[i];

                              //       phone = reg.mobile;
                              //     }

                              //     return _buildColumn(
                              //         Icon(
                              //           Icons.phone,
                              //           color: Color(0XFFFFFFFF),
                              //           size: 20,
                              //         ),
                              //         'Phone',
                              //         '$phone');
                              //   },
                              // ),
                              // FutureBuilder(
                              //   future: helper.getProfile(userName),
                              //   builder: (context, snapshot) {
                              //     if (!snapshot.hasData)
                              //       return Center(
                              //           child: CircularProgressIndicator());
                              //     String email = "";
                              //     for (int i = 0;
                              //         i < snapshot.data.length;
                              //         i++) {
                              //       Registration reg = snapshot.data[i];

                              //       email = reg.email;
                              //     }

                              //     return _buildColumn(
                              //         Icon(
                              //           Icons.email,
                              //           color: Color(0XFFFFFFFF),
                              //           size: 20,
                              //         ),
                              //         'Email',
                              //         '$email');
                              //   },
                              // ),
                            ],
                          ),
                        ))),
              ],
            ),
          ),
        ));
  }

  Container _buildColumn(Icon icon, String title, String subtitle) {
    return Container(
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                radius: 15.0,
                backgroundColor: Theme.of(context).iconTheme.color,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: icon,
                ),
              ),
               Container(
              margin: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 8),
              child: Text(subtitle,
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Theme.of(context).accentTextTheme.subhead.color,
                      fontFamily: 'Roboto',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w700))),
              // Container(
              //   margin: EdgeInsets.only(left: 8.0),
              //   child: Text(title,
              //       overflow: TextOverflow.clip,
              //       textAlign: TextAlign.center,
              //       style: TextStyle(
              //           decoration: TextDecoration.none,
              //           color: Theme.of(context).accentTextTheme.subhead.color,
              //           fontFamily: 'Roboto',
              //           fontSize: 12.0,
              //           fontWeight: FontWeight.normal)),
              // )
            ],
          ),
          // Container(
          //     margin: EdgeInsets.only(top: 10.0, left: 40.0, bottom: 8),
          //     child: Text(subtitle,
          //         overflow: TextOverflow.clip,
          //         textAlign: TextAlign.center,
          //         style: TextStyle(
          //             decoration: TextDecoration.none,
          //             color: Theme.of(context).accentTextTheme.subhead.color,
          //             fontFamily: 'Roboto',
          //             fontSize: 16.0,
          //             fontWeight: FontWeight.w700))),
          // Divider(
          //   color: Color(0XFF707070).withOpacity(0.3),
          // ),
        ],
      ),
    );
  }
}
