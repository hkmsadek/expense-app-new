import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/PasswordChange.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/changePass.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/profile.dart';
import 'package:expanse_manegment/customPlugin/routeTransition/routeAnimation.dart';
import 'package:expanse_manegment/database/RegistrationData.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProEdit extends StatefulWidget {
  final fName;
  final lName;
  final userName;
  final email;
  final mobile;
  ProEdit(this.fName, this.lName, this.userName, this.email, this.mobile);

  @override
  _ProEditState createState() => _ProEditState();
}

class _ProEditState extends State<ProEdit> {
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();
  final RegistrationHelper helper = new RegistrationHelper();
  Registration registration;
  String fn, ln, user, email, phn;
  String chkUser = "", id = "", pass = "";

  @override
  void initState() {
    _getUserInfo();
    setState(() {
      firstnameController.text = widget.fName;
      lastnameController.text = widget.lName;
      usernameController.text = widget.userName;
      emailController.text = widget.email;
      phoneController.text = widget.mobile;

      fn = widget.fName;
      ln = widget.lName;
      user = widget.userName;
      email = widget.email;
      phn = widget.mobile;

      chkUser = widget.userName;
    });
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('user');

    if (user != null) {
      List us = user.split("~");
      setState(() {
        id = us[0];
        pass = us[6];
      });
    }
  }

  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
        child: Container(
            padding: EdgeInsets.only(top: 8, bottom: 0, left: 20, right: 20),
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
            ),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  fn = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.text,
                              controller: firstnameController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "First Name",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  ln = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.text,
                              controller: lastnameController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Last Name",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.person,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  user = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.text,
                              controller: usernameController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "User Name",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.call,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  email = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.number,
                              controller: phoneController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Mobile",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.email,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  phn = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.text,
                              controller: emailController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Email",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: <Widget>[
                    //     CircleAvatar(
                    //       radius: 15.0,
                    //       backgroundColor: Theme.of(context).iconTheme.color,
                    //       child: Padding(
                    //         padding: const EdgeInsets.all(5.0),
                    //         child: Icon(
                    //           Icons.lock,
                    //           color: Color(0XFFFFFFFF),
                    //           size: 20,
                    //         ),
                    //       ),
                    //     ),
                    //     Expanded(
                    //       child: Container(
                    //         margin: EdgeInsets.only(left: 10, top: 8),
                    //         child: TextFormField(
                    //           onChanged: (value) {
                    //             setState(() {
                    //               //ic = value;
                    //             });
                    //           },
                    //           style: TextStyle(
                    //             color: Theme.of(context)
                    //                 .accentTextTheme
                    //                 .subtitle
                    //                 .color,
                    //             fontSize: 14,
                    //             fontFamily: "Poppins",
                    //           ),
                    //           cursorColor: Theme.of(context)
                    //               .accentTextTheme
                    //               .subtitle
                    //               .color,
                    //           controller: passwordController,
                    //           keyboardType: TextInputType.text,
                    //           obscureText: passwordvisible,
                    //           autofocus: false,
                    //           decoration: InputDecoration(
                    //               enabledBorder: UnderlineInputBorder(
                    //                 borderSide: BorderSide(
                    //                     color:
                    //                         Color(0XFF707070).withOpacity(0.2)),
                    //               ),
                    //               focusedBorder: UnderlineInputBorder(
                    //                 borderSide: BorderSide(
                    //                     color:
                    //                         Color(0XFF707070).withOpacity(0.2)),
                    //               ),
                    //               hintText: "Password",
                    //               hintStyle: TextStyle(
                    //                 color: Theme.of(context)
                    //                     .accentTextTheme
                    //                     .subtitle
                    //                     .color,
                    //                 fontSize: 15,
                    //                 fontFamily: "Poppins",
                    //                 fontWeight: FontWeight.normal,
                    //               ),
                    //               suffixIcon: GestureDetector(
                    //                 onTap: () {
                    //                   _togglePasswordVisibility();
                    //                 },
                    //                 child: Icon(
                    //                   passwordvisible
                    //                       ? Icons.visibility_off
                    //                       : Icons.visibility,
                    //                   color: passwordvisible
                    //                       ? Theme.of(context)
                    //                           .accentTextTheme
                    //                           .subtitle
                    //                           .color
                    //                       : Theme.of(context).bottomAppBarColor,
                    //                 ),
                    //               ),
                    //               isDense: true),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: <Widget>[
                    //     CircleAvatar(
                    //       radius: 15.0,
                    //       backgroundColor: Theme.of(context).iconTheme.color,
                    //       child: Padding(
                    //         padding: const EdgeInsets.all(5.0),
                    //         child: Icon(
                    //           Icons.lock,
                    //           color: Color(0XFFFFFFFF),
                    //           size: 20,
                    //         ),
                    //       ),
                    //     ),
                    //     Expanded(
                    //       child: Container(
                    //         margin: EdgeInsets.only(left: 10, top: 8),
                    //         child: TextFormField(
                    //           onChanged: (value) {
                    //             setState(() {
                    //               //ic = value;
                    //             });
                    //           },
                    //           style: TextStyle(
                    //             color: Theme.of(context)
                    //                 .accentTextTheme
                    //                 .subtitle
                    //                 .color,
                    //             fontSize: 14,
                    //             fontFamily: "Poppins",
                    //           ),
                    //           cursorColor: Theme.of(context)
                    //               .accentTextTheme
                    //               .subtitle
                    //               .color,
                    //           controller: confirmpasswordController,
                    //           keyboardType: TextInputType.text,
                    //           obscureText: conpasswordvisible,
                    //           autofocus: false,
                    //           decoration: InputDecoration(
                    //               enabledBorder: UnderlineInputBorder(
                    //                 borderSide: BorderSide(
                    //                     color:
                    //                         Color(0XFF707070).withOpacity(0.2)),
                    //               ),
                    //               focusedBorder: UnderlineInputBorder(
                    //                 borderSide: BorderSide(
                    //                     color:
                    //                         Color(0XFF707070).withOpacity(0.2)),
                    //               ),
                    //               hintText: "Confirm Password",
                    //               hintStyle: TextStyle(
                    //                   color: Theme.of(context)
                    //                       .accentTextTheme
                    //                       .subtitle
                    //                       .color,
                    //                   fontSize: 15,
                    //                   fontFamily: "Poppins",
                    //                   fontWeight: FontWeight.normal),
                    //               suffixIcon: GestureDetector(
                    //                 onTap: () {
                    //                   _toggleconPasswordVisibility();
                    //                 },
                    //                 child: Icon(
                    //                   conpasswordvisible
                    //                       ? Icons.visibility_off
                    //                       : Icons.visibility,
                    //                   color: conpasswordvisible
                    //                       ? Theme.of(context)
                    //                           .accentTextTheme
                    //                           .subtitle
                    //                           .color
                    //                       : Theme.of(context).bottomAppBarColor,
                    //                 ),
                    //               ),
                    //               isDense: true),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),

                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 10, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                //onPressed: _login,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Cancel',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 10, right: 20, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                onPressed: _prof,
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Save',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )));
  }

  Future<void> _prof() async {
    if (firstnameController.text.isEmpty) {
      return _showMsg("First Name is empty");
    }
    if (lastnameController.text.isEmpty) {
      return _showMsg("Last Name is empty");
    }
    if (usernameController.text.isEmpty) {
      return _showMsg("Username is empty");
    }
    if (emailController.text.isEmpty) {
      return _showMsg("Email is empty");
    } else if (!emailController.text.contains("@") ||
        !emailController.text.contains(".")) {
      return _showMsg("Enter a valid email");
    }
    if (phoneController.text.isEmpty) {
      return _showMsg("Mobile number is empty");
    }

    Future.delayed(Duration.zero, () async {
      final _userAuthData1 =
          await helper.getLoginList(chkUser, phoneController.text);

      print("_userAuthData1.length");
      print(_userAuthData1.length);

      registration = _userAuthData1[0];

      registration.firstName = fn;
      registration.lastName = ln;
      registration.userName = user;
      registration.mobile = phn;
      registration.email = email;

      helper.updateRegistration(registration).then((id) => {
            firstnameController.clear(),
            lastnameController.clear(),
            usernameController.clear(),
            phoneController.clear(),
            emailController.clear(),
            passwordController.clear(),
            confirmpasswordController.clear(),
          });

      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString(
          'user',
          "$id" +
              "~" +
              fn +
              "~" +
              ln +
              "~" +
              user +
              "~" +
              email +
              "~" +
              phn +
              "~" +
              pass);

    //  Navigator.pop(context);

      Navigator.push(context, SlideLeftRoute(page: Profile()));
    });
  }
}
