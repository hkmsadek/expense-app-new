import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/profile.dart';
import 'package:expanse_manegment/customPlugin/routeTransition/routeAnimation.dart';
import 'package:flutter/material.dart';

class PasswordChange extends StatefulWidget {
  @override
  _PasswordChangeState createState() => _PasswordChangeState();
}

class _PasswordChangeState extends State<PasswordChange> {


TextEditingController oldpasswordController = TextEditingController();
  TextEditingController newpasswordController = TextEditingController();
  TextEditingController retypepasswordController = TextEditingController();


  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  bool passwordvisible = true;
  bool conpasswordvisible = true;

  void _togglePasswordVisibility() {
    setState(() {
      passwordvisible = !passwordvisible;
    });
  }

  void _toggleconPasswordVisibility() {
    setState(() {
      conpasswordvisible = !conpasswordvisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
        child: Container(
            padding: EdgeInsets.only(top: 8, bottom: 0, left: 20, right: 20),
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
            ),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.lock,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 8, left: 10),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  //ic = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              keyboardType: TextInputType.text,
                              controller: oldpasswordController,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Color(0XFF707070).withOpacity(0.2)),
                                ),
                                hintText: "Enter old password",
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.lock,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 8),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  //ic = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: newpasswordController,
                              keyboardType: TextInputType.text,
                              obscureText: passwordvisible,
                              autofocus: false,
                              decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  hintText: "Enter New Password",
                                  hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .accentTextTheme
                                        .subtitle
                                        .color,
                                    fontSize: 15,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.normal,
                                  ),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      _togglePasswordVisibility();
                                    },
                                    child: Icon(
                                      passwordvisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: passwordvisible
                                          ? Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color
                                          : Theme.of(context).bottomAppBarColor,
                                    ),
                                  ),
                                  isDense: true),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15.0,
                          backgroundColor: Theme.of(context).iconTheme.color,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.lock,
                              color: Color(0XFFFFFFFF),
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 10, top: 8),
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  //ic = value;
                                });
                              },
                              style: TextStyle(
                                color: Theme.of(context)
                                    .accentTextTheme
                                    .subtitle
                                    .color,
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              cursorColor: Theme.of(context)
                                  .accentTextTheme
                                  .subtitle
                                  .color,
                              controller: retypepasswordController,
                              keyboardType: TextInputType.text,
                              obscureText: conpasswordvisible,
                              autofocus: false,
                              decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color(0XFF707070).withOpacity(0.2)),
                                  ),
                                  hintText: "Re-type Password",
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .accentTextTheme
                                          .subtitle
                                          .color,
                                      fontSize: 15,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.normal),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      _toggleconPasswordVisibility();
                                    },
                                    child: Icon(
                                      conpasswordvisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: conpasswordvisible
                                          ? Theme.of(context)
                                              .accentTextTheme
                                              .subtitle
                                              .color
                                          : Theme.of(context).bottomAppBarColor,
                                    ),
                                  ),
                                  isDense: true),
                            ),
                          ),
                        ),
                      ],
                    ),
                   
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 10, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                //onPressed: _login,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Cancel',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 10, right: 20, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.white),
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                onPressed: _prof,
                                child: Container(
                                  //width: 150,
                                  //color: Colors.grey,
                                  child: Text(
                                    'Save',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Color(0XFF1A3D7A),
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Colors.white,
                                elevation: 2.0,
                                //splashColor: Colors.blueGrey,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )));
  }

  void _prof() {
   
    if (oldpasswordController.text.isEmpty) {
      return _showMsg("Password is empty");
    }  else if (newpasswordController.text.isEmpty) {
      return _showMsg("Phone is empty");
    }
    if (retypepasswordController.text.isEmpty) {
      return _showMsg("Confirm your password");
    } else if (retypepasswordController.text != newpasswordController.text) {
      return _showMsg("Password not match");
    }

    Navigator.push(context, SlideLeftRoute(page: Profile()));
  }
}