import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/changePass.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/proedit.dart';
import 'package:expanse_manegment/customPlugin/routeTransition/routeAnimation.dart';
import 'package:flutter/material.dart';

class AddPro extends StatefulWidget {
  final fName;
  final lName;
  final userName;
  final email;
  final mobile;
  final password;
  AddPro(this.fName, this.lName, this.userName, this.email, this.mobile,
      this.password);

  @override
  _AddProState createState() => _AddProState();
}

class _AddProState extends State<AddPro> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: AppBar(
          elevation: 0.0,
          backgroundColor: Theme.of(context).bottomAppBarColor,
          titleSpacing: 2,
          automaticallyImplyLeading: false,
          leading: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: GestureDetector(
              onTap: Navigator.of(context).pop,
              child: Icon(
                Icons.arrow_back,
                size: 28.0,
              ),
            ),
          ),
          title: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text("Edit Profile",
                // widget.val =="1"?"Category":"Sub Category",
                style: TextStyle(
                  color: Color(0XFFF2F4F7),
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                  fontFamily: 'Roboto',
                )),
          ),
          actions: <Widget>[
            IconButton(
              icon: new Icon(
                Icons.lock,
                size: 30,
              ),
              onPressed: () {
                Navigator.push(context, SlideLeftRoute(page: AddPass()));
              }, // => _scaffoldKey.currentState.openDrawer()
            ),
          ],
        ),
      ),
      body: ProEdit(widget.fName, widget.lName, widget.userName, widget.email, widget.mobile),
    );
  }
}
