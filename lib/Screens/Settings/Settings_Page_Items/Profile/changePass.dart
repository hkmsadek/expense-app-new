import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/PasswordChange.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Profile/proedit.dart';
import 'package:flutter/material.dart';

class AddPass extends StatefulWidget {
  @override
  _AddPassState createState() => _AddPassState();
}

class _AddPassState extends State<AddPass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.close,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text("Change Password",
                  // widget.val =="1"?"Category":"Sub Category",
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: PasswordChange(),
    );
  }
}