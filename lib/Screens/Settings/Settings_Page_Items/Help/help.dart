import 'package:flutter/material.dart';

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Settings',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
            child: Container(
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Container(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 0.0, left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.0),
                          topRight: Radius.circular(25.0)),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            /////////// Text Container /////////
                            Container(
                              padding: EdgeInsets.fromLTRB(8.0, 20.0, 8.0, 10.0),
                                child: Text('Help',
                                    overflow: TextOverflow.clip,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        decoration: TextDecoration.none,
                                        color: Theme.of(context)
                                            .accentTextTheme
                                            .headline
                                            .color,
                                        fontFamily: 'Roboto',
                                        fontSize: 24.0,
                                        fontWeight: FontWeight.bold))),
                              /////////// Icon Container /////////
                            Container(
                              padding: EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10.0),
                              child: Icon(Icons.camera_front, color: Theme.of(context).inputDecorationTheme.fillColor, size: 150),
                            ),
                            /////////// Text Container /////////
                            Container(
                              padding: EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10.0),
                              child: Text('Contact Us ',
                              overflow: TextOverflow.clip,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        decoration: TextDecoration.none,
                                        color: Theme.of(context).accentTextTheme.body2.color,
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal)
                              )),
                            /////////// Button Container /////////
                            Container(
                              margin: EdgeInsets.only(top: 45, bottom: 20.0),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              child: Card(
                                color: Color(0XFFFFFFFF),
                                elevation: 3,
                                child: FlatButton(
                                  color: Color(0XFFFFFFFF),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text('Connect Now',
                                          overflow: TextOverflow.clip,
                                          // textAlign: TextAlign.center,
                                          style: TextStyle(
                                              decoration: TextDecoration.none,
                                              color: Color(0XFF234481),
                                              fontFamily: 'Roboto',
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold)),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Icon(
                                        Icons.send,
                                        color: Color(0XFF234481),
                                        size: 17,
                                      ),
                                    ],
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )))));
  }
}
