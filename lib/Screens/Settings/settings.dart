import 'package:expanse_manegment/Screens/LoginPage/loginPage.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Help/help.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Privacy/privacy.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Subscription/subscription.dart';
import 'package:expanse_manegment/Screens/Settings/Settings_Page_Items/Terms_And_Conditions/termsAndConditions.dart';
import 'package:flutter/material.dart';
import 'Settings_Page_Items/About/about.dart';
import 'Settings_Page_Items/General/general.dart';
import 'Settings_Page_Items/Profile/profile.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text('Settings',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
          //alignment: Alignment.centerLeft,
          child: Container(
            decoration:
                BoxDecoration(color: Theme.of(context).bottomAppBarColor),
            child: Container(
              padding: EdgeInsets.only(top: 12, bottom: 0, left: 20, right: 20),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0)),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    ///////// item List Start ///////////

                    /////////////   Profile Start ////////////
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Profile()));
                        },
                        child: itemList(
                          Icon(
                            Icons.person,
                            color: Color(0XFFFFFFFF),
                          ),
                          'Profile',
                          Color(0XFF707070).withOpacity(0.3),
                        )),
                    /////////////   Profile End ////////////

                    ////////////////   General Start ////////////
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GeneralPage()));
                        },
                        child: itemList(
                          Icon(
                            Icons.settings,
                            color: Color(0XFFFFFFFF),
                          ),
                          'General',
                          Color(0XFF707070).withOpacity(0.3),
                        )),
                    /////////////   General End ///////////////

                    /////////////   Switch Account Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                      child: itemList(
                        Icon(Icons.settings_backup_restore,
                            color: Color(0XFFFFFFFF)),
                        'Switch Account',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Switch Account End ////////////

                    /////////////   Terms And Conditions Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TermsAndConditions()));
                      },
                      child: itemList(
                        Icon(
                          Icons.note,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Terms And Conditions',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Terms And Conditions End ////////////

                    /////////////   Privacy Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Privacy()));
                      },
                      child: itemList(
                        Icon(
                          Icons.lock,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Privacy',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Privacy End ////////////

                    /////////////   Feedback Start ////////////
                    GestureDetector(
                      onTap: () {
                        _showDialog();
                      },
                      child: itemList(
                        Icon(
                          Icons.feedback,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Feedback',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Feedback End ////////////

                    /////////////   Subscription Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Subscription()));
                      },
                      child: itemList(
                        Icon(
                          Icons.subscriptions,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Subscription',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Subscription End ////////////

                    /////////////   Help Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Help()));
                      },
                      child: itemList(
                        Icon(
                          Icons.help,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Help',
                        Color(0XFF707070).withOpacity(0.3),
                      ),
                    ),
                    /////////////   Help End ////////////

                    /////////////   About Start ////////////
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => About()));
                      },
                      child: itemList(
                          Icon(
                            Icons.people,
                            color: Color(0XFFFFFFFF),
                          ),
                          'About',
                          Theme.of(context).backgroundColor),
                    ),
                    /////////////   About End ////////////
                  ],
                  //children: _mainList(),
                ),
              ),
            ),
          ),
        ));
  }

  Column itemList(Icon icons, String title, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(5.0, 8.0, 5.0, 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(right: 10.0),
                        decoration: BoxDecoration(
                          color: Color(0XFF596F97),
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        ),
                        child: icons,
                      ),
                      Container(
                        child: Text(
                          title,
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontFamily: 'Roboto',
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Icon(
                Icons.arrow_right,
                color: Color(0XFF596F97),
                size: 28,
              )
            ],
          ),
        ),
        Divider(color: color)
      ],
    );
  }

  void _showDialog() {
    showDialog(
      context: context,
      // builder: (BuildContext context) {
      builder: (_) => Center(
          child: Container(
        width: MediaQuery.of(context).size.width,
        //height: MediaQuery.of(context).size.height * 0.45,
        height: 280,
        decoration: BoxDecoration(
          color: Color(0XFFF2F4F7),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(20, 30, 20, 20),
              padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
              alignment: Alignment.center,
              child: Text(
                  'Your Review Will Help Us To Give You A Better Experience, Make It A Good One. ',
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Color(0XFF1A3D7A),
                      fontFamily: 'Roboto',
                      fontSize: 20.0,
                      fontWeight: FontWeight.normal)),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Card(
                color: Color(0XFFFFFFFF),
                elevation: 3,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(2.0, 2.0, 6.0, 2.0),
                  child: FlatButton(
                    color: Color(0XFFFFFFFF),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.star_half,
                          color: Color(0XFF234481),
                          size: 24,
                          textDirection: TextDirection.ltr,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text('Rate  Now',
                            overflow: TextOverflow.clip,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                color: Color(0XFF234481),
                                fontFamily: 'Roboto',
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
