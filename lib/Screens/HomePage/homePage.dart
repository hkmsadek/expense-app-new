import 'package:expanse_manegment/Screens/HomePage/Add_Transaction/addTransaction.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/AllContainerWidgets/ExpenseDataContainer.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/AllContainerWidgets/IncomeDataContainer.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/allContainer.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/drawerOnly.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/seeAllPage.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/switchWidget.dart';
import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:expanse_manegment/database/IncomeData.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:expanse_manegment/main.dart';

enum WidgetMarker { all, Income, Expense }

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var top = 0.0;
  double animhight = 80.0;

  int inblnc = 0;
  final IncomeDataHelper inhelper = new IncomeDataHelper();
  final ExpenseDataHelper exhelper = new ExpenseDataHelper();
  WidgetMarker selectedWidgetMarker = WidgetMarker.all;

  AnimationController _controller;
  Animation _animation;

  @override
  void initState() {
    //to clear previous total values
    setState(() {
      incomeTotal = 0;
      expenseTotal = 0;
      allTotal = 0;
    });
    anim();
    incometotal(); // calculate income total
    expensetotal(); // calculate expense total
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  //////method for expense calculation/////
  void incometotal() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await inhelper.getIncomeSheetList();
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String inbalnc = _userAuthData[i].amount;
          double inbalanc = double.parse(inbalnc);
          print("inbalanc.toString()");
          print(inbalanc.toString());
          incomeTotal += inbalanc; //calculation
          print("incomeTotal");
          print(incomeTotal);
        });
      }
    });
  }

//////method for expense calculation/////
  void expensetotal() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await exhelper.getExpenseSheetList();
      print("_userAuthData.length");
      print(_userAuthData.length);

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          String exbalnc = _userAuthData[i].amount;
          double exbalanc = double.parse(exbalnc);
          print("exbalanc.toString()");
          print(exbalanc.toString());
          expenseTotal += exbalanc; //calculation
          print("expenseTotal");
          print(expenseTotal);
        });
      }

      setState(() {
        allTotal = incomeTotal - expenseTotal;
      });
    });
  }

  anim() async {
    await Future.delayed(new Duration(milliseconds: 100));
    setState(() {
      animhight = 0.0;
    });
  }

  Container buttonContainer(
      String picture, String title, Color txtColor, FontWeight txtFont) {
    return Container(
        padding: EdgeInsets.fromLTRB(15, 3, 15, 3),
        margin: EdgeInsets.only(left: 5),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
        ),
        //width: MediaQuery.of(context).size.width,
        height: 29,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                child: Icon(
              Icons.keyboard_arrow_right,
              color: Colors.red,
            )),
            Container(
              //width: 150,
              //color: Colors.grey,
              child: Text(
                title,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  color: txtColor, //Color(0XFF1A3D7A),
                  fontSize: 16.0,
                  decoration: TextDecoration.none,
                  fontFamily: 'Roboto',
                  fontWeight: txtFont, // FontWeight.bold,
                ),
              ),
            ),
          ],
        ));
  }

  Future<bool> _onWillPop() async {
    // return (SystemChannels.platform.invokeMethod('SystemNavigator.pop')) ??
    //     false;

      return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text(
              "Exit",
              //  textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                
                
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            content: new Text(
              "Are you sure want to exit the app?",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  //  color: Color(0xFFFF9100),
                
                
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text(
                  "No",
                  style: TextStyle(
                      color: Colors.red[800],
                    
                    
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
              ),
              new FlatButton(
                  onPressed: () => SystemChannels.platform
                      .invokeMethod('SystemNavigator.pop'),
                  child: new Text(
                    "Yes",
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                       
                       
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  )),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        key: _scaffoldKey,
        drawer: DrawerOnly(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AddTransaction(inblnc)));
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
            size: 27,
          ),
          backgroundColor: Theme.of(context).accentColor,//Theme.of(context).accentIconTheme.color,
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                  iconTheme: IconThemeData(color: Color(0xFFFFFFFF)),
                  backgroundColor: Theme.of(context).bottomAppBarColor,
                  expandedHeight: 210.0,
                  //automaticallyImplyLeading: false,
                  centerTitle: true,
                  //floating: false,
                  pinned: true,
                  elevation: 0,
                  leading: new IconButton(
                      icon: new Icon(
                        Icons.sort,
                        size: 30,
                      ),
                      onPressed: () => _scaffoldKey.currentState.openDrawer()),
                  actions: <Widget>[
                    IconButton(
                      icon: new Icon(
                        Icons.notifications,
                        size: 30,
                      ),
                      onPressed:
                          () {}, // => _scaffoldKey.currentState.openDrawer()
                    ),
                  ],
                  flexibleSpace: LayoutBuilder(builder:
                      (BuildContext context, BoxConstraints constraints) {
                    // print('constraints=' + constraints.toString());
                    top = constraints.biggest.height;
                    return FlexibleSpaceBar(
                        //centerTitle: true,
                        title: Row(
                          // mainAxisAlignment:
                          //     MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              //alignment: Alignment.bottomCenter,
                              //   padding: EdgeInsets.only(top: 10),
                              child: Text(
                                top <= 80 ? 'Recent Transactions' : "",
                                //  '${widget.orderedItem.seller.name}',
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 19.0,
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),

                        ////////////////////Collapsed Bar/////////////////
                        background: Stack(
                          children: <Widget>[
                            Container(
                              color: Theme.of(context).bottomAppBarColor,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        height: 141,
                                        width: 187,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                          image: AssetImage(
                                              'assets/img/appbar1.png'),
                                        )),
                                      ),
                                    ],
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.end,
                                  //   children: <Widget>[
                                  //     Container(
                                  //       height: 69,
                                  //       width: 120,
                                  //       decoration: BoxDecoration(
                                  //        image: DecorationImage(image: AssetImage('assets/img/appbar2.png'))),
                                  //     ),
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                            Container(
                              //constraints: new BoxConstraints.expand(height: 256.0, ),
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(
                                  left: top <= 90 ? 40 : top <= 80 ? 60.0 : 30,
                                  bottom: 8.0),

                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(bottom: 3),
                                    child: Text(
                                      //top <= 80 ? 'Home' : "Home",
                                      top <= 90 ? "" : "Balance",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17.0,
                                        decoration: TextDecoration.none,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      //top <= 80 ? 'Home' : "Home",
                                      top <= 90
                                          ? ""
                                          : inblnc == 1
                                              ? incomeTotal.toStringAsFixed(2)
                                              : inblnc == 2
                                                  ? expenseTotal.toStringAsFixed(2)
                                                  : allTotal.toStringAsFixed(2),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 40.0,
                                        decoration: TextDecoration.none,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )

                        ////////////////////Collapsed Bar  End/////////////////

                        );
                  })),

              /////////////////////// Body Start //////////////////////

              SliverToBoxAdapter(
                child: Column(
                  children: <Widget>[
                    Container(
                      //height: 10.0,
                      alignment: Alignment.centerLeft,
                      child: Container(
                          // height: 100,// MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              color: Theme.of(context).bottomAppBarColor),
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 20, bottom: 0, left: 20, right: 20),
                            decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25.0),
                                  topRight: Radius.circular(25.0)),
                            ),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding:
                                  EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Recent Transactions",
                                    //top <= 90 ?  "" : "\$ 2,560",
                                    style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: 20.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      inblnc == 1
                                          ? Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SeeAllPage(1)))
                                          : inblnc == 2
                                              ? Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SeeAllPage(2)))
                                              : Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SeeAllPage(3)));
                                    },
                                    child: Container(
                                      child: Text(
                                        "See All",
                                        //top <= 90 ?  "" : "\$ 2,560",
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .accentTextTheme
                                              .title
                                              .color,
                                          fontSize: 12.0,
                                          decoration: TextDecoration.none,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )),
                    ),
                    //HomeBodySwitch(),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: 20, bottom: 20, left: 20, right: 20),
                            decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                            ),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                ///////////////// All Button  Start///////////////

                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        selectedWidgetMarker = WidgetMarker.all;
                                        inblnc = 0;
                                      });
                                    },
                                    child: Container(
                                        padding:
                                            EdgeInsets.fromLTRB(15, 3, 15, 3),
                                        decoration: BoxDecoration(
                                          color: Theme.of(context).primaryColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0)),
                                        ),
                                        child: Container(
                                          child: Text(
                                            "All",
                                            textDirection: TextDirection.ltr,
                                            style: TextStyle(
                                              color: (selectedWidgetMarker ==
                                                      WidgetMarker.all)
                                                  ? Theme.of(context)
                                                      .accentColor
                                                  : Colors.grey,
                                              fontSize: 16.0,
                                              decoration: TextDecoration.none,
                                              fontFamily: 'Roboto',
                                              fontWeight:
                                                  (selectedWidgetMarker ==
                                                          WidgetMarker.all)
                                                      ? FontWeight.bold
                                                      : FontWeight.normal,
                                            ),
                                          ),
                                        ))),

                                ///////////////// All Button  End///////////////
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        selectedWidgetMarker =
                                            WidgetMarker.Income;
                                        inblnc = 1;
                                        print("inblnc");
                                        print(inblnc);
                                      });
                                    },
                                    child: buttonContainer(
                                        "jj",
                                        "Income",
                                        (selectedWidgetMarker ==
                                                WidgetMarker.Income)
                                            ? Theme.of(context).accentColor
                                            : Colors.grey,
                                        (selectedWidgetMarker ==
                                                WidgetMarker.Income)
                                            ? FontWeight.bold
                                            : FontWeight.normal)),

                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        selectedWidgetMarker =
                                            WidgetMarker.Expense;
                                        inblnc = 2;
                                        print("inblnc");
                                        print(inblnc);
                                      });
                                    },
                                    child: buttonContainer(
                                        "jj",
                                        "Expense",
                                        (selectedWidgetMarker ==
                                                WidgetMarker.Expense)
                                            ? Theme.of(context).accentColor
                                            : Colors.grey,
                                        (selectedWidgetMarker ==
                                                WidgetMarker.Expense)
                                            ? FontWeight.bold
                                            : FontWeight.normal)),
                              ],
                            ),
                          ),

                          /////////////////// Body Start////////////
                          FutureBuilder(
                            future: _playAnimation(),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              return Container(
                                child: getCustomContainer(),
                              );
                            },
                          ),

                          /////////////////// Body end////////////
                        ],
                      ),
                    )
                  ],
                ),
              )
//             new SliverStickyHeader(
//   header: new Container(
//     //height: 10.0,
//     alignment: Alignment.centerLeft,
//     child: Container(
//        // height: 100,// MediaQuery.of(context).size.height,
//         decoration: BoxDecoration(
//           color: Color(0XFF1A3D7A)
//         ),
//         child: Container(
//           padding: EdgeInsets.only(top: 20, bottom: 0, left: 20, right: 20),
//           decoration: BoxDecoration(
//             color: Color(0XFFFFFFFF),
//             borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(25.0),
//               topRight: Radius.circular(25.0)
//             ),
//           ),
//           child: Container(
//             width: MediaQuery.of(context).size.width,
//             child: Text(
//                                //top <= 80 ? 'Home' : "Home",
//                                 top <= 90 ?  "" : "\$ 2,560",
//                                 style: TextStyle(
//                                   color: Colors.black,
//                                   fontSize: 15.0,
//                                   decoration: TextDecoration.none,
//                                   fontFamily: 'Roboto',
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               ),
//           ),
//         )
//       ),
//   ),
//   sliver: new SliverList(
//     delegate: new SliverChildBuilderDelegate(
//       (context, i) => HomeBodySwitch(),
//       childCount: 1,
//     ),
//   ),
// ),
              /////////////////////// Body End //////////////////////
            ],
          ),
        ),
      ),
    );
  }

  ////////Animation Play////////
  _playAnimation() {
    _controller.reset();
    _controller.forward();
  }

//////// All Body /////////
  Widget getAllWidget() {
    return FadeTransition(opacity: _animation, child: AllContainer());
  }

  Widget getIncomeWidget() {
    return FadeTransition(opacity: _animation, child: IncomeDataContainer());
  }

  Widget getExpenseWidget() {
    return FadeTransition(opacity: _animation, child: ExpenseDataContainer());
  }

//////// All Body /////////

//////// Selected Body Method /////////

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.all:
        return getAllWidget();
      case WidgetMarker.Income:
        return getIncomeWidget();
      case WidgetMarker.Expense:
        return getExpenseWidget();
    }
    return getAllWidget();
  }
//////// Selected Body Method /////////
}
