import 'package:expanse_manegment/Screens/HomePage/Add_Transaction/Choose_Icon/chooseExpenseIcon.dart';
import 'package:expanse_manegment/Screens/HomePage/Add_Transaction/Choose_Icon/chooseIncomeIcon.dart';
import 'package:flutter/material.dart';

enum WidgetMarker {
  income,
  expense,
}

class ChooseIcon extends StatefulWidget {
  @override
  _ChooseIconState createState() => _ChooseIconState();
}

class _ChooseIconState extends State<ChooseIcon> {
  WidgetMarker selectedWidgetMarker = WidgetMarker.income;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(80.0),
        //   child: AppBar(
        //     //elevation: 4.0,
        //     backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        //     titleSpacing: 2,
        //     automaticallyImplyLeading: false,
        //     leading: Padding(
        //       padding: const EdgeInsets.only(top: 15.0),
        //       child: GestureDetector(
        //         onTap: Navigator.of(context).pop,
        //         child: Icon(
        //           Icons.arrow_back,
        //           size: 28.0,
        //           color: Color(0XFF1A3D7A),
        //         ),
        //       ),
        //     ),
        //     title: Padding(
        //       padding: const EdgeInsets.only(top: 15.0),
        //       child: Text('Icon',
        //           style: TextStyle(
        //             color: Color(0XFF1A3D7A),
        //             fontWeight: FontWeight.bold,
        //             fontSize: 22.0,
        //             fontFamily: 'Roboto',
        //           )),
        //     ),
        //   ),
        // ),
        body: Container(

            //alignment: Alignment.centerLeft,
            // child: Container(
            //   decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
            //   child: Container(
            //     padding: EdgeInsets.only(top: 8, bottom: 0, left: 20, right: 20),
            //     decoration: BoxDecoration(
            //       color: Theme.of(context).backgroundColor,
            //       borderRadius: BorderRadius.only(
            //           topLeft: Radius.circular(25.0),
            //           topRight: Radius.circular(25.0)),
            //     ),
            child: SingleChildScrollView(
                child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              ////////////////////  Custom AppBar Start  ////////////////////
              Container(
                  height: 100,
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15.0, 50.0, 5.0, 15.0),
                        child: GestureDetector(
                          onTap: Navigator.of(context).pop,
                          child: Icon(
                            Icons.arrow_back,
                            size: 28.0,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15.0, 50.0, 5.0, 15.0),
                        child: Text('Icon',
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                              fontFamily: 'Roboto',
                            )),
                      ),
                    ],
                  )),
            ////////////////////  Custom AppBar End  ////////////////////
            ///////////////////////  Body Start  ///////////////////////
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5.0),
                    alignment: Alignment.center,
                    height: 50,
                    child: Container(
                      height: 30.5,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border.all(color: Color(0XFF1A3D7A), width: 2),
                          borderRadius: BorderRadius.circular(5.0)),
                      alignment: Alignment.center,
                      child: Row(
                        children: <Widget>[
                          Container(
                            height: 29,
                            color: (selectedWidgetMarker == WidgetMarker.income)
                                ? Color(0XFF1A3D7A)
                                : Theme.of(context).primaryColor,
                            child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  selectedWidgetMarker = WidgetMarker.income;
                                });
                              },
                              child: Text(
                                "Income",
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  color: (selectedWidgetMarker ==
                                          WidgetMarker.income)
                                      ? Color(0XFFFFFFFF)
                                      : Color(0XFF1A3D7A),
                                  fontSize: 16.0,
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Roboto',
                                  fontWeight: (selectedWidgetMarker ==
                                          WidgetMarker.income)
                                      ? FontWeight.bold
                                      : FontWeight.normal,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 29,
                            color:
                                (selectedWidgetMarker == WidgetMarker.expense)
                                    ? Color(0XFF1A3D7A)
                                    : Theme.of(context).primaryColor,
                            child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  selectedWidgetMarker = WidgetMarker.expense;
                                });
                              },
                              child: Text(
                                "Expense",
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  color: (selectedWidgetMarker ==
                                          WidgetMarker.expense)
                                      ? Color(0XFFFFFFFF)
                                      : Color(0XFF1A3D7A),
                                  fontSize: 16.0,
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Roboto',
                                  fontWeight: (selectedWidgetMarker ==
                                          WidgetMarker.expense)
                                      ? FontWeight.bold
                                      : FontWeight.normal,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                child: getCustomContainer(),
              )
            ],
          ),
        ))));
    //  ))));
  }

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.income:
        return getIncomeWidget();
      case WidgetMarker.expense:
        return getExpenseWidget();
    }
    return getIncomeWidget();
  }

  Widget getIncomeWidget() {
    return Container(child: ChooseIncomeIcon());
  }

  Widget getExpenseWidget() {
    return Container(child: ChooseExpenseIcon());
  }
}
