import 'package:flutter/material.dart';

class ChooseExpenseIcon extends StatefulWidget {
  @override
  _ChooseExpenseIconState createState() => _ChooseExpenseIconState();
}

class _ChooseExpenseIconState extends State<ChooseExpenseIcon> {
  @override
  Widget build(BuildContext context) {
   return Container(
      padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
      child: Wrap(
        children: <Widget>[
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.camera_alt, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.cake, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.flight, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.computer, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.card_travel, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.phone_android, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.home, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.camera_alt, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.cake, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.flight, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.computer, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.card_travel, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.phone_android, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.home, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.home, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.camera_alt, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.cake, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.flight, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.computer, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.card_travel, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.phone_android, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.fastfood, color: Theme.of(context).accentColor, size: 35)),
          iconContainer(Icon(Icons.card_travel, color: Theme.of(context).accentColor, size: 35)),
        ],
      ),
    );
  }

  Container iconContainer(Icon icon) {
    return Container(
          height: 40,
          width: 40,
          margin: EdgeInsets.all(10.0),
          child: icon,
          // child: Icon(
          //   Icons.fastfood,
          //   color: Color(0XFF1A3D7A),
          //   size: 30,
          // ),
        );
  }
}
