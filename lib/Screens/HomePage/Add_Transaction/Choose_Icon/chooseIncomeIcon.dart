//import 'package:expanse_manegment/Screens/HomePage/Add_Transaction/Add_Income_Transaction/addIncomeTransaction.dart';
import 'package:flutter/material.dart';

import '../../../../main.dart';

class ChooseIncomeIcon extends StatefulWidget {
  @override
  _ChooseIncomeIconState createState() => _ChooseIncomeIconState();
}

class _ChooseIncomeIconState extends State<ChooseIncomeIcon> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
      child: Wrap(
          ////////list generate for icon///////
          children: List.generate(
        iconList.length,
        (int index) {
          return GestureDetector(
              onTap: () {
                setState(() {
                  icon.clear(); ////initially clear data//
                  icon.add({
                    'icon': iconList[index]['name']
                  }); ////then add icon from list///
                  Navigator.pop(context);
                  print(iconList[index]['name'].toString());
                  iconInd = index.toString();
                  print("iconInd");
                  print(iconInd);
                  print(icon);
                  print(icon.length);
                });
              },
              child: Container(
                height: 40,
                width: 40,
                margin: EdgeInsets.all(10.0),
                //child: //icon,
                child: Icon(
                  iconList[index]['name'], ///////icon list call////
                  color: Theme.of(context).accentColor,
                  size: 30,
                ),
              ));
        },
      )
          ////////list generate for icon///////
          ),
    );
  }
}
