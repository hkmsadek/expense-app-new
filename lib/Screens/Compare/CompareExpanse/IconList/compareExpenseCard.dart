import 'package:flutter/material.dart';


class CompareExpenseCard extends StatefulWidget {
  final index;
  CompareExpenseCard(this.index);
  @override
  _CompareExpenseCardState createState() =>
      _CompareExpenseCardState();
}

class _CompareExpenseCardState extends State<CompareExpenseCard> {
  var _selectedFood = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedFood = true;
          print('Food is selected');
        });
      },
      child: Card(
        color: _selectedFood ? Color(0XFF1A3D7A) : Theme.of(context).cardColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),

        /// margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.fastfood,
              size: 32,
              color: _selectedFood
                  ? Colors.white
                  : Theme.of(context).accentColor,
            ),
            SizedBox(height: 8),
            Text(
              "Food",
              style: TextStyle(
                  color: _selectedFood
                      ? Colors.white
                      : Theme.of(context).accentColor,
                  fontSize: 12,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
