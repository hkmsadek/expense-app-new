import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'IconList/compareExpenseCard.dart';

class CompareExpense extends StatefulWidget {
  @override
  _CompareExpenseState createState() => _CompareExpenseState();
}

class _CompareExpenseState extends State<CompareExpense> {
  List<String> _dateCompare = [
    'First Date Group',
    'Next Date Group',
  ];
  String _currentDateSelected;

  bool tapped = false;

  String dateFr = '';
  String dateTo = '';
  DateTime selectedDateFrom = DateTime.now();
  DateTime selectedDateTo = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text(
                "Category",
                style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontFamily: 'Roboto',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    // color: Colors.red,
                    // height: MediaQuery.of(context).size.height / 2.3,
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                      ),
                      itemBuilder: (BuildContext context, int index) =>
                          Container(
                        child: CompareExpenseCard(index),
                      ),
                      itemCount: 12,
                    ),
                  ),
                ),
              ],
            ),
            _buildDropDownDateWidget(),
            _buildGraphWidget(),
          ],
        ),
      ),
    );
  }

  _buildDropDownDateWidget() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
           // height: 150,
            child: Container(
              padding: EdgeInsets.fromLTRB(10, 8, 5, 8),
              width: MediaQuery.of(context).size.width / 2,
             // height: 150,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(1))),
              // child: GestureDetector(
              //   onTap: () {
              //     setState(() {
              //       tapped = true;
              //     });
              //   },
              //   onDoubleTap: () {
              //     setState(() {
              //       tapped = false;
              //     });
              //   },
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: <Widget>[
              //       Text('Compare by Date',
              //           style: TextStyle(
              //               color: Theme.of(context).accentTextTheme.display1.color,
              //               fontFamily: 'Poppins',
              //               fontSize: 14,
              //               fontWeight: FontWeight.bold)),
              //       Icon(
              //         tapped? Icons.arrow_drop_down : Icons.arrow_drop_up,
              //         size: 28,
              //         color: Theme.of(context).accentTextTheme.display1.color,
              //       )
              //     ],
              //   ),
              // ),
              child: //////////   Dropdown Button For Payment Method Start  ////////
                  DropdownButton<String>(
                value: _currentDateSelected,
                iconSize: 24,
                iconEnabledColor: Theme.of(context).accentColor,
                iconDisabledColor: Theme.of(context).accentColor,
                isExpanded: true,
                isDense: true,
                underline: Container(
                  height: 0,
                ),
                onChanged: (String newValue) {
                  _onDropdownSelectedDate(newValue);
                },
                hint: Container(
                  //color: Colors.blue,
                  child: Text('Compare by Date',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          fontWeight: FontWeight.bold)),
                ),
                items: _dateCompare.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Container(
                     // color: Colors.red,
                      child: Text(value,
                          style: TextStyle(
                              color: Theme.of(context).accentTextTheme.overline.color,
                              fontFamily: 'Roboto',
                              fontSize: 12,
                              fontWeight: FontWeight.w500)),
                    ),
                  );
                }).toList(),
              ),
              // ///////   Dropdown Button For Payment Method End ////////
            ),
          ),
          tapped? Container(
            height: 100,
            color: Colors.white,
          )
          : Container()
        ],
      ),
    );
  }

  void _onDropdownSelectedDate(String newValue) {
    setState(() {
      this._currentDateSelected = newValue;
      if(newValue == 'First Date Group')
      {
        print(newValue);
          _selectDateFirst(context);
      }
      else {
        print(newValue);
        _selectDateNext(context);
      }
    });
  }

    ////////////////////////   Date Picker Start  //////////////////
  Future<Null> _selectDateFirst(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDateFrom,
        firstDate: DateTime(1964, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDateFrom) {
      setState(() {
        selectedDateFrom = picked;
        dateFr = "${DateFormat("yyyy-MM-dd").format(selectedDateFrom)}";
        // Navigator.pop(context);
        // selection(2);
      });
    }
  }
  ////////////////////////   Date Picker End  //////////////////

  ////////////////////////   Date Picker Start  //////////////////
  Future<Null> _selectDateNext(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDateTo,
        firstDate: DateTime(1964, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDateTo) {
      setState(() {
        selectedDateTo = picked;
        dateTo = "${DateFormat("yyyy-MM-dd").format(selectedDateTo)}";
        // Navigator.pop(context);
        // selection(2);
      });
    }
  }
  ////////////////////////   Date Picker End  //////////////////

  _buildGraphWidget() {
    return Container();
  }
}
