import 'package:expanse_manegment/Screens/IntroPage/introPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Theme_Settings/theme_notifier.dart';
import 'Theme_Settings/values.dart';
import 'package:expanse_manegment/Screens/HomePage/homePage.dart';

//////global veriable declare ///
String categ = "", iconInd = "";

String icn = "";
int icnInt = 0; // all, income, expense button selection detect in homepage

///make int index
double incomeTotal = 0,
    expenseTotal = 0,
    allTotal = 0; // expense & income total variable
List icon = []; ////list declare for icon select/////

List iconList = [
  {
    'name': Icons.subject,
  },
  { 
    'name': Icons.fastfood,
  },
  {'name': Icons.cake},
  {'name': Icons.flight},
  {'name': Icons.computer},
  {'name': Icons.card_travel},
  {'name': Icons.fastfood},
  {'name': Icons.cake},
  {'name': Icons.flight},
  {'name': Icons.computer},
  {'name': Icons.fastfood},
  {
    'name': Icons.fastfood,
  },
  {'name': Icons.cake},
  {'name': Icons.flight},
  {'name': Icons.computer},
  {'name': Icons.card_travel},
  {'name': Icons.fastfood},
  {'name': Icons.cake},
  {'name': Icons.flight},
  {'name': Icons.computer},
  {'name': Icons.fastfood},
]; /////////////show category list///////

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences.getInstance().then((prefs) {
    var darkModeOn = prefs.getBool('darkMode') ?? true;
    runApp(
      ChangeNotifierProvider<ThemeNotifier>(
        builder: (_) => ThemeNotifier(darkModeOn ? darkTheme : lightTheme),
        // builder: (_) => ThemeNotifier(lightTheme),
        child: MyApp(),
      ),
    );
  });
}

class MyApp extends StatefulWidget {
  @override
  MyHome createState() => MyHome();
  // });
}

class MyHome extends State<MyApp> {
  bool _isLoggedIn = false;

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('user');

    if (user != null) {
      setState(() {
        _isLoggedIn = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return MaterialApp(
        title: 'Expense Manegment',
        debugShowCheckedModeBanner: false,
        theme: themeNotifier.getTheme(),
        home: _isLoggedIn ? HomePage() : IntroPage());
  }
  // });
}
