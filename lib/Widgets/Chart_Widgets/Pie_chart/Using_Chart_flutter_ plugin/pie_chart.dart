import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class PieChart extends StatefulWidget {
  @override
  _PieChartState createState() => _PieChartState();
}

class _PieChartState extends State<PieChart> {
  List<charts.Series<Task, String>> _seriesChartData;

  _generateData() {
    var chartData = [
      new Task('task', 35.0, Colors.amber),
      new Task('eat', 8.0, Colors.red),
      new Task('commute', 10.0, Colors.green),
      new Task('sleep', 15.0, Colors.blue),
    ];

    _seriesChartData.add(charts.Series(
      data: chartData,
      domainFn: (Task task, _) => task.task,
      measureFn: (Task task, _) => task.taskvalue,
      colorFn: (Task task, _) => charts.ColorUtil.fromDartColor(task.colorval),
      id: 'Daily Task',
      labelAccessorFn: (Task row, _) => '${row.taskvalue}',
    ));
  }

  @override
  void initState() {
    super.initState();
    _seriesChartData = List<charts.Series<Task, String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          children: <Widget>[
            Text('Time spent on daily task'),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: charts.PieChart(
                _seriesChartData,
                animate: true,
                animationDuration: Duration(seconds: 5),
                behaviors: [
                  charts.DatumLegend(
                      outsideJustification:
                          charts.OutsideJustification.endDrawArea,
                      horizontalFirst: false,
                      desiredMaxRows: 2,
                      cellPadding: EdgeInsets.only(right: 4.0, bottom: 4.0),
                      entryTextStyle: charts.TextStyleSpec(
                          color: charts.MaterialPalette.purple.shadeDefault,
                          fontFamily: 'Georgia',
                          fontSize: 11))
                ],
                defaultRenderer: new charts.ArcRendererConfig(
                    arcWidth: 100,
                    arcRendererDecorators: [
                      new charts.ArcLabelDecorator(
                        labelPosition: charts.ArcLabelPosition.inside,
                      )
                    ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Task {
  String task;
  double taskvalue;
  Color colorval;

  Task(this.task, this.taskvalue, this.colorval);
}
