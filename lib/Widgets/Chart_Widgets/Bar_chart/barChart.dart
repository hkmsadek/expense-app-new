import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class BarChart extends StatefulWidget {
  @override
  _BarChartState createState() => _BarChartState();
}

class _BarChartState extends State<BarChart> {
  List<charts.Series<Pollution, String>> _seriesBarData;

  _generateData() {
    var data1 = [
      new Pollution('USA', 1980, 30),
      new Pollution('Asia', 1980, 40),
      new Pollution('Europe', 1985, 10),
    ];
    var data2 = [
      new Pollution('USA', 1985, 130),
      new Pollution('Asia', 1980, 240),
      new Pollution('Europe', 1985, 100),
    ];
    var data3 = [
      new Pollution('USA', 1980, 300),
      new Pollution('Asia', 1980, 140),
      new Pollution('Europe', 1985, 200),
    ];

    _seriesBarData.add(charts.Series(
      data: data1,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Color(0xFF990099)),
      id: '2017',
    ));

    _seriesBarData.add(charts.Series(
      data: data2,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Colors.red),
      id: '2017',
    ));

    _seriesBarData.add(charts.Series(
      data: data3,
      domainFn: (Pollution pollution, _) => pollution.place,
      measureFn: (Pollution pollution, _) => pollution.quantity,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      fillColorFn: (Pollution pollution, _) =>
          charts.ColorUtil.fromDartColor(Colors.amber),
      id: '2017',
    ));
  }

  @override
  void initState() {
    super.initState();
    _seriesBarData = List<charts.Series<Pollution, String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          children: <Widget>[
            Text('Time spent on daily task'),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: charts.BarChart(
                _seriesBarData,
                animate: true,
                animationDuration: Duration(seconds: 5),
                barGroupingType: charts.BarGroupingType.grouped,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Pollution {
  String place;
  int year;
  int quantity;

  Pollution(this.place, this.year, this.quantity);
}
