import 'package:expanse_manegment/EditPage/TransactionEdit.dart';
import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:expanse_manegment/database/IncomeData.dart';
import 'package:expanse_manegment/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class SeeAllPage extends StatefulWidget {
  final number;
  SeeAllPage(this.number);

  @override
  _SeeAllPageState createState() => _SeeAllPageState();
}

class _SeeAllPageState extends State<SeeAllPage> {
  //final _formKey = GlobalKey<FormState>();
  var status = 2;
  var inote = 1;

  final IncomeDataHelper inhelper = new IncomeDataHelper();
  final ExpenseDataHelper exhelper = new ExpenseDataHelper();
  final IncomeDataHelper allhelper = new IncomeDataHelper();

  final _formKey = GlobalKey<FormState>();
  //List<IncomeSheet> incategorylist = []; /////////////show category list///////
  //List<IncomeSheet> nonRepetitive = [];
  List allData = [];
  List incData = [];
  List expData = [];
  List totData = [];
  IncomeSheet incomeSheet;
  ExpenseSheet expenseSheet;
  int updateIndex;
  List icon = [];
  String ic = "";
  String nte = "";
  String inbalnc;
  int inbalanc;
  int indx = 0;
  int icoindx = 0;
  //int inblnc = 0;
  bool inte = false;

  @override
  void initState() {
    incomedata();
    expenseData();
    super.initState();
  }

  void incomedata() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await inhelper.getIncomeSheetList();

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          IncomeSheet cat = _userAuthData[i];
          incData.add(cat);
          //allData.add(cat);
        });
      }

      print("incData.length");
      print(incData.length);
    });
  }

  void expenseData() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await exhelper.getExpenseSheetList();

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          ExpenseSheet cat = _userAuthData[i];
          expData.add(cat);
          //allData.add(cat);
        });
      }
      print("expData.length");
      print(expData.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                  color: Color(0XFFF2F4F7),
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text(
                  widget.number == 1
                      ? 'All Income'
                      : widget.number == 2 ? 'All Expense' : 'All Transactions',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
        // padding: EdgeInsets.only(bottom:20),
          color: Theme.of(context).backgroundColor,
          child: SingleChildScrollView(

            physics: BouncingScrollPhysics(),
                      child: Column(
                        children: <Widget>[
                          Column(
              children: <Widget>[
                widget.number == 2
                    ? Container()
                    : Container(
                            margin: EdgeInsets.only(top: 20),
                            padding: EdgeInsets.only(bottom: 15, left: 25),
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Income",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18.0,
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                widget.number == 2
                    ? Container()
                    : Column(
                            children: List.generate(incData.length, (index) {
                            IncomeSheet incat;
                            String amtString, inDate, nt, categaory;
                            incat = incData[index];
                            amtString = incat.amount;
                             double amt = double.parse(amtString);
                            inDate = incat.date;
                            categaory = incat.selectCat;
                            nt = incat.note;
                            print("nt lol");
                            print(incat.selectIcon);

                            int i = 0;
                            int len = -1;

                            return Slidable( 
                              actionPane: SlidableDrawerActionPane(),

                              actionExtentRatio: 0.20,
                              child: GestureDetector(
                                /////item call & select from list///
                                // onTap: () {
                                //   setState(() {
                                //     icn = incat.selectIcon;

                                //     Navigator.pop(context);
                                //   });
                                //   /////item select from list///
                                // },
                                child: Container(
                                    margin: EdgeInsets.only(
                                      bottom: 2,
                                      left: 20,
                                    ),
                                    padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).cardColor,
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(10))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            // color: Colors.yellow,
                                            child: Row(
                                              children: <Widget>[
                                                //////////////  Icon Container  /////////////
                                                Container(
                                                  padding: EdgeInsets.all(6),
                                                  margin:
                                                      EdgeInsets.only(right: 10.0),
                                                  decoration: BoxDecoration(
                                                    color: Theme.of(context)
                                                        .backgroundColor,
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(20.0)),
                                                  ),
                                                  child: Icon(
                                                  
                                                   // iconList[icnInt]['name'],
                                                    iconList[int.parse(incat.selectIcon)]['name'],
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .iconTheme
                                                        .color,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      // Navigator.push(
                                                      //     context,
                                                      //     MaterialPageRoute(
                                                      //         builder: (context) => Collections()));
                                                    },
                                                    child: Container(
                                                      //color: Colors.blue,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          Text(
                                                            '$categaory',
                                                            overflow:
                                                                TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                color:
                                                                    Theme.of(context)
                                                                        .accentColor,
                                                                fontSize: 17.0,
                                                                fontFamily: 'Roboto',
                                                                fontWeight:
                                                                    FontWeight.bold),
                                                          ),
                                                          Text(
                                                            '$nt',
                                                            overflow:
                                                                TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                color:
                                                                    Theme.of(context)
                                                                        .accentColor,
                                                                fontSize: 13.0,
                                                                fontFamily: 'Roboto',
                                                                fontWeight: FontWeight
                                                                    .normal),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),

                                                //   Text(
                                                //   '$nt',
                                                //   style: TextStyle(
                                                //       color: Color(0xFF9F2A2A),
                                                //       fontSize: 15.0,
                                                //       fontFamily: 'Roboto',
                                                //       fontWeight: FontWeight.normal),
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          //color: Colors.green,
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                amt.toStringAsFixed(2),
                                                style: TextStyle(
                                                    color: Color(0xFF9F2A2A),
                                                    fontSize: 15.0,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.normal),
                                              ),
                                              SizedBox(
                                                height: 8.0,
                                              ),
                                              Text(
                                                '$inDate',
                                                style: TextStyle(
                                                    color:
                                                        Theme.of(context).accentColor,
                                                    fontSize: 8.0,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                              /////////slide edit delete////
                              secondaryActions: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 2,
                                    bottom: 2,
                                  ),
                                  child: new IconSlideAction(
                                    // caption: 'Edit',
                                    color: Color(0XFFEFEFEF),
                                    //icon: Icons.edit,
                                    iconWidget: Icon(
                                      Icons.edit,
                                      color: Color(0XFF1A3D7A),
                                    ),
                                    onTap: () {
                                      ////object create////

                                      //Navigator.push(context, MaterialPageRoute(builder:(context) => AddTransaction()));
                                      setState(() {
                                        var incomeObject;
                                        incomeObject = {
                                          "id": incat.id,
                                          "date": inDate,
                                          "incategory": incat.selectCat,
                                          "icon": incat.selectIcon,
                                          "amount": amt,
                                          "note": nt,
                                        };

                                        incomeSheet = incat;

                                        updateIndex = index; // current index
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => TransactionEdit(
                                                    status,
                                                    incomeObject,
                                                    incomeSheet,
                                                    updateIndex)));
                                      });
                                    }, // _showSnackBar('More'),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 2,
                                    bottom: 2,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: i == 0
                                        ? BorderRadius.only(
                                            topRight: Radius.circular(5.0),
                                            bottomRight: Radius.circular(5.0),
                                          )
                                        : i == len
                                            ? BorderRadius.only(
                                                bottomRight: Radius.circular(10.0),
                                              )
                                            : BorderRadius.only(
                                                bottomRight: Radius.circular(0.0),
                                                topRight: Radius.circular(0.0)),
                                  ),
                                  child: new IconSlideAction(
                                    // caption: 'Delete',
                                    color: Colors.transparent,
                                    icon: Icons.delete,
                                    onTap: () {
                                      _showDeleteDialog(incat, index);
                                    },
                                  ),
                                ),
                              ],
                            );
                          })),
                widget.number == 1
                    ? Container()
                    : Container(
                            margin: EdgeInsets.only(top: 20),
                            padding: EdgeInsets.only(bottom: 15, left: 25),
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Expense",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18.0,
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                widget.number == 1
                    ? Container()
                    : Column(
                            children: List.generate(expData.length, (index) {
                            ExpenseSheet excat;
                            String amtString, inDate, nt, categaory;
                            excat = expData[index];
                           
                            amtString = excat.amount;
                              double amt = double.parse(amtString);
                            inDate = excat.date;
                            categaory = excat.selectCat;
                            nt = excat.note;
                            print("nt lol");
                            print(nt);

                            int i = 0;
                            int len = -1;

                            return Slidable(
                              actionPane: SlidableDrawerActionPane(),

                              actionExtentRatio: 0.20,
                              child: GestureDetector(
                                /////item call & select from list///
                                // onTap: () {
                                //   setState(() {
                                //     icn = excat.selectIcon;

                                //     Navigator.pop(context);
                                //   });
                                //   /////item select from list///
                                // },
                                child: Container(
                                    margin: EdgeInsets.only(
                                      bottom: 2,
                                      left: 20,
                                    ),
                                    padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).cardColor,
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(10))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            // color: Colors.yellow,
                                            child: Row(
                                              children: <Widget>[
                                                //////////////  Icon Container  /////////////
                                                Container(
                                                  padding: EdgeInsets.all(6),
                                                  margin:
                                                      EdgeInsets.only(right: 10.0),
                                                  decoration: BoxDecoration(
                                                    color: Theme.of(context)
                                                        .backgroundColor,
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(20.0)),
                                                  ),
                                                  child: Icon(
                                                  //  iconList[icnInt]['name'],
                                                    iconList[int.parse(excat.selectIcon)]['name'],
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .iconTheme
                                                        .color,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      // Navigator.push(
                                                      //     context,
                                                      //     MaterialPageRoute(
                                                      //         builder: (context) => Collections()));
                                                    },
                                                    child: Container(
                                                      //color: Colors.blue,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          Text(
                                                            '$categaory',
                                                            overflow:
                                                                TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                color:
                                                                    Theme.of(context)
                                                                        .accentColor,
                                                                fontSize: 17.0,
                                                                fontFamily: 'Roboto',
                                                                fontWeight:
                                                                    FontWeight.bold),
                                                          ),
                                                          Text(
                                                            '$nt',
                                                            overflow:
                                                                TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                color:
                                                                    Theme.of(context)
                                                                        .accentColor,
                                                                fontSize: 13.0,
                                                                fontFamily: 'Roboto',
                                                                fontWeight: FontWeight
                                                                    .normal),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),

                                                //   Text(
                                                //   '$nt',
                                                //   style: TextStyle(
                                                //       color: Color(0xFF9F2A2A),
                                                //       fontSize: 15.0,
                                                //       fontFamily: 'Roboto',
                                                //       fontWeight: FontWeight.normal),
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          //color: Colors.green,
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                amt.toStringAsFixed(2),
                                                style: TextStyle(
                                                    color: Color(0xFF9F2A2A),
                                                    fontSize: 15.0,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.normal),
                                              ),
                                              SizedBox(
                                                height: 8.0,
                                              ),
                                              Text(
                                                '$inDate',
                                                style: TextStyle(
                                                    color:
                                                        Theme.of(context).accentColor,
                                                    fontSize: 8.0,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                              /////////slide edit delete////
                              secondaryActions: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 2,
                                    bottom: 2,
                                  ),
                                  child: new IconSlideAction(
                                    // caption: 'Edit',
                                    color: Color(0XFFEFEFEF),
                                    //icon: Icons.edit,
                                    iconWidget: Icon(
                                      Icons.edit,
                                      color: Color(0XFF1A3D7A),
                                    ),
                                    onTap: () {
                                      ////object create////

                                      //Navigator.push(context, MaterialPageRoute(builder:(context) => AddTransaction()));
                                      setState(() {
                                        var incomeObject;
                                        incomeObject = {
                                          "id": excat.id,
                                          "date": inDate,
                                          "incategory": excat.selectCat,
                                          "icon": excat.selectIcon,
                                          "amount": amt,
                                          "note": nt,
                                        };

                                        expenseSheet = excat;

                                        updateIndex = index; // current index
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => TransactionEdit(
                                                    status,
                                                    incomeObject,
                                                    expenseSheet,
                                                    updateIndex)));
                                      });
                                    }, // _showSnackBar('More'),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 2,
                                    bottom: 2,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: i == 0
                                        ? BorderRadius.only(
                                            topRight: Radius.circular(5.0),
                                            bottomRight: Radius.circular(5.0),
                                          )
                                        : i == len
                                            ? BorderRadius.only(
                                                bottomRight: Radius.circular(10.0),
                                              )
                                            : BorderRadius.only(
                                                bottomRight: Radius.circular(0.0),
                                                topRight: Radius.circular(0.0)),
                                  ),
                                  child: new IconSlideAction(
                                    // caption: 'Delete',
                                    color: Colors.transparent,
                                    icon: Icons.delete,
                                    onTap: () {
                                      _showDeleteDialog(excat, index);
                                    },
                                  ),
                                ),
                              ],
                            );
                          })),
                // SingleChildScrollView(
                //   physics: BouncingScrollPhysics(),
                //   ////////show category in the another screen////
                //   child: FutureBuilder(
                //     future: widget.number == 1
                //         ? inhelper.getIncomeSheetList()
                //         : widget.number == 2
                //             ? exhelper.getExpenseSheetList()
                //             : inhelper.getIncomeSheetList(),
                //     builder: (context, snapshot) {
                //       allData = snapshot.data;

                //       // for (var i = 0; i < incategorylist.length; i++) {
                //       //   bool repeated = false;
                //       //   for (var j = 0; j < nonRepetitive.length; j++) {
                //       //     if (incategorylist[i] == nonRepetitive[j]) {
                //       //       repeated = true;
                //       //     }
                //       //   }
                //       //   if (!repeated) {
                //       //     nonRepetitive.add(incategorylist[i]);
                //       //   }
                //       // }
                //       // print(nonRepetitive);

                //       if (!snapshot.hasData)
                //         return Center(child: CircularProgressIndicator());
                //       //////////for scrolling use list.generate/////////
                //       return Container(
                //         margin: EdgeInsets.only(top: 30, right: 20),
                //         child: Column(
                //           key: _formKey,
                //           children: List.generate(allData.length, (int index) {
                //             IncomeSheet incat;
                //             ExpenseSheet excat;
                //             String amt, inDate, nt, categaory;
                //             if (widget.number == 1) {
                //               incat = allData[index];
                //               amt = incat.amount;
                //               inDate = incat.date;
                //               categaory = incat.selectCat;
                //               nt = incat.note;
                //               print("nt lol");
                //               print(nt);
                //             } else if (widget.number == 2) {
                //               excat = allData[index];
                //               amt = excat.amount;
                //               inDate = excat.date;
                //               categaory = excat.selectCat;
                //               nt = excat.note;
                //               print("nt lol");
                //               print(nt);
                //             } else {
                //               incat = allData[index];
                //               amt = incat.amount;
                //               inDate = incat.date;
                //               categaory = incat.selectCat;
                //               nt = incat.note;
                //               print("nt lol");
                //               print(nt);
                //             }
                //             //String selectCat = incat.selectCat;

                //             // /////amount calculation///
                //             int i = 0;
                //             int len = -1;

                //             // String ind = "${incat.selectIcon}"; //icon index with string
                //             // int intIndex = int.parse(ind); //string to integer

                //             // indx = intIndex;
                //             // print("ind");
                //             // print(ind);
                //             // print("indx");
                //             // print(indx);

                //             return Slidable(
                //               actionPane: SlidableDrawerActionPane(),

                //               actionExtentRatio: 0.20,
                //               child: GestureDetector(
                //                 /////item call & select from list///
                //                 onTap: () {
                //                   setState(() {
                //                     if (widget.number == 1) {
                //                       icn = incat.selectIcon; ////global ver///
                //                     } else if (widget.number == 2) {
                //                       icn = excat.selectIcon; ////global ver///
                //                     } else {
                //                       icn = incat.selectIcon; ////global ver///
                //                     }

                //                     Navigator.pop(context);
                //                   });
                //                   /////item select from list///
                //                 },
                //                 child: Container(
                //                     margin: EdgeInsets.only(
                //                       bottom: 2,
                //                       left: 20,
                //                     ),
                //                     padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                //                     decoration: BoxDecoration(
                //                         color: Theme.of(context).cardColor,
                //                         borderRadius: BorderRadius.all(
                //                             Radius.circular(10))),
                //                     child: Row(
                //                       mainAxisAlignment:
                //                           MainAxisAlignment.spaceBetween,
                //                       children: <Widget>[
                //                         Expanded(
                //                           child: Container(
                //                             // color: Colors.yellow,
                //                             child: Row(
                //                               children: <Widget>[
                //                                 //////////////  Icon Container  /////////////
                //                                 Container(
                //                                   padding: EdgeInsets.all(6),
                //                                   margin: EdgeInsets.only(
                //                                       right: 10.0),
                //                                   decoration: BoxDecoration(
                //                                     color: Theme.of(context)
                //                                         .backgroundColor,
                //                                     borderRadius:
                //                                         BorderRadius.all(
                //                                             Radius.circular(
                //                                                 20.0)),
                //                                   ),
                //                                   child: Icon(
                //                                     iconList[icnInt]['name'],
                //                                     size: 22,
                //                                     color: Theme.of(context)
                //                                         .iconTheme
                //                                         .color,
                //                                   ),
                //                                 ),
                //                                 Expanded(
                //                                   child: GestureDetector(
                //                                     onTap: () {
                //                                       // Navigator.push(
                //                                       //     context,
                //                                       //     MaterialPageRoute(
                //                                       //         builder: (context) => Collections()));
                //                                     },
                //                                     child: Container(
                //                                       //color: Colors.blue,
                //                                       child: Column(
                //                                         crossAxisAlignment:
                //                                             CrossAxisAlignment
                //                                                 .start,
                //                                         children: <Widget>[
                //                                           Text(
                //                                             '$categaory',
                //                                             overflow: TextOverflow
                //                                                 .ellipsis,
                //                                             style: TextStyle(
                //                                                 color: Theme.of(
                //                                                         context)
                //                                                     .accentColor,
                //                                                 fontSize: 17.0,
                //                                                 fontFamily:
                //                                                     'Roboto',
                //                                                 fontWeight:
                //                                                     FontWeight
                //                                                         .bold),
                //                                           ),
                //                                           Text(
                //                                             '$nt',
                //                                             overflow: TextOverflow
                //                                                 .ellipsis,
                //                                             style: TextStyle(
                //                                                 color: Theme.of(
                //                                                         context)
                //                                                     .accentColor,
                //                                                 fontSize: 13.0,
                //                                                 fontFamily:
                //                                                     'Roboto',
                //                                                 fontWeight:
                //                                                     FontWeight
                //                                                         .normal),
                //                                           ),
                //                                         ],
                //                                       ),
                //                                     ),
                //                                   ),
                //                                 ),

                //                                 //   Text(
                //                                 //   '$nt',
                //                                 //   style: TextStyle(
                //                                 //       color: Color(0xFF9F2A2A),
                //                                 //       fontSize: 15.0,
                //                                 //       fontFamily: 'Roboto',
                //                                 //       fontWeight: FontWeight.normal),
                //                                 // ),
                //                               ],
                //                             ),
                //                           ),
                //                         ),
                //                         Container(
                //                           //color: Colors.green,
                //                           child: Column(
                //                             children: <Widget>[
                //                               Text(
                //                                 '\$$amt',
                //                                 style: TextStyle(
                //                                     color: Color(0xFF9F2A2A),
                //                                     fontSize: 15.0,
                //                                     fontFamily: 'Roboto',
                //                                     fontWeight:
                //                                         FontWeight.normal),
                //                               ),
                //                               SizedBox(
                //                                 height: 8.0,
                //                               ),
                //                               Text(
                //                                 '$inDate',
                //                                 style: TextStyle(
                //                                     color: Theme.of(context)
                //                                         .accentColor,
                //                                     fontSize: 8.0,
                //                                     fontFamily: 'Roboto',
                //                                     fontWeight: FontWeight.w400),
                //                               ),
                //                             ],
                //                           ),
                //                         )
                //                       ],
                //                     )),
                //               ),
                //               /////////slide edit delete////
                //               secondaryActions: <Widget>[
                //                 Container(
                //                   margin: EdgeInsets.only(
                //                     top: 2,
                //                     bottom: 2,
                //                   ),
                //                   child: new IconSlideAction(
                //                     // caption: 'Edit',
                //                     color: Color(0XFFEFEFEF),
                //                     //icon: Icons.edit,
                //                     iconWidget: Icon(
                //                       Icons.edit,
                //                       color: Color(0XFF1A3D7A),
                //                     ),
                //                     onTap: () {
                //                       ////object create////

                //                       //Navigator.push(context, MaterialPageRoute(builder:(context) => AddTransaction()));
                //                       setState(() {
                //                         var incomeObject;
                //                         if (widget.number == 1) {
                //                           incomeObject = {
                //                             "id": incat.id,
                //                             "date": inDate,
                //                             "incategory": incat.selectCat,
                //                             "icon": incat.selectIcon,
                //                             "amount": amt,
                //                             "note": nt,
                //                           };
                //                         } else if (widget.number == 1) {
                //                           incomeObject = {
                //                             "id": excat.id,
                //                             "date": inDate,
                //                             "incategory": excat.selectCat,
                //                             "icon": excat.selectIcon,
                //                             "amount": amt,
                //                             "note": nt,
                //                           };
                //                         } else {
                //                           incomeObject = {
                //                             "id": incat.id,
                //                             "date": inDate,
                //                             "incategory": incat.selectCat,
                //                             "icon": incat.selectIcon,
                //                             "amount": amt,
                //                             "note": nt,
                //                           };
                //                         }

                //                         if (widget.number == 1) {
                //                           incomeSheet = incat;
                //                         } else if (widget.number == 2) {
                //                           expenseSheet = excat;
                //                         } else {
                //                           incomeSheet = incat;
                //                         }

                //                         updateIndex = index; // current index
                //                         Navigator.push(
                //                             context,
                //                             MaterialPageRoute(
                //                                 builder: (context) =>
                //                                     TransactionEdit(
                //                                         status,
                //                                         incomeObject,
                //                                         widget.number == 1
                //                                             ? incomeSheet
                //                                             : widget.number == 2
                //                                                 ? expenseSheet
                //                                                 : incomeSheet,
                //                                         updateIndex)));
                //                       });
                //                     }, // _showSnackBar('More'),
                //                   ),
                //                 ),
                //                 Container(
                //                   margin: EdgeInsets.only(
                //                     top: 2,
                //                     bottom: 2,
                //                   ),
                //                   decoration: BoxDecoration(
                //                     color: Colors.red,
                //                     borderRadius: i == 0
                //                         ? BorderRadius.only(
                //                             topRight: Radius.circular(5.0),
                //                             bottomRight: Radius.circular(5.0),
                //                           )
                //                         : i == len
                //                             ? BorderRadius.only(
                //                                 bottomRight:
                //                                     Radius.circular(10.0),
                //                               )
                //                             : BorderRadius.only(
                //                                 bottomRight: Radius.circular(0.0),
                //                                 topRight: Radius.circular(0.0)),
                //                   ),
                //                   child: new IconSlideAction(
                //                     // caption: 'Delete',
                //                     color: Colors.transparent,
                //                     icon: Icons.delete,
                //                     onTap: () {
                //                       _showDeleteDialog(incat, index);
                //                     },
                //                   ),
                //                 ),
                //               ],
                //             );
                //           }),
                //         ),
                //       );
                //     },
                //   ),
                //   ////////show category in the another screen////
                // ),
              ],
            ),

            SizedBox(height:30)
                        ],
                      ),
          ),
        ));
  }

  ///////Dialog ////////////////////
  ///
  void _showDeleteDialog(incat, index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Container(
                        child: new Text(
                          "Do you want to delete this transaction?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0XFF1A3D7A),
                            fontSize: 19.0,
                            decoration: TextDecoration.none,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    //color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ///////// Cancel Button Start /////
                        Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                color: Colors.white),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 12, right: 12),
                                child: Text(
                                  'Cancel',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Colors.white,
                              elevation: 2.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            )),

                        ///////// Cancle Button end /////

                        ///////// Ok Button Start /////
                        Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              color: Color(0XFFFB7676),
                            ),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                widget.number == 1
                                    ? inhelper.deleteIncomeSheet(incat.id)
                                    : widget.number == 2
                                        ? exhelper.deleteExpenseSheet(incat.id)
                                        : inhelper.deleteIncomeSheet(incat.id);
                                setState(() {
                                  allData.removeAt(index);
                                  Navigator.of(context).pop();
                                });
                              },

                              child: Container(
                                padding: EdgeInsets.only(left: 25, right: 25),
                                child: Text(
                                  'Ok',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Color(0XFFFB7676),
                              elevation: 2.0,
                              //splashColor: Colors.blueGrey,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),

                        ///////// Ok Button end /////
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
