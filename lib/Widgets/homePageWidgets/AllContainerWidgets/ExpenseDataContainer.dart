import 'package:expanse_manegment/EditPage/TransactionEdit.dart';
import 'package:expanse_manegment/Widgets/homePageWidgets/AllContainerWidgets/ExpenseNotePage.dart';
import 'package:expanse_manegment/database/ExpenseData.dart';
import 'package:expanse_manegment/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart' as Path;

class ExpenseDataContainer extends StatefulWidget {
  @override
  _ExpenseDataContainerState createState() => _ExpenseDataContainerState();
}

class _ExpenseDataContainerState extends State<ExpenseDataContainer> {
  var status = 2;

  final ExpenseDataHelper inhelper = new ExpenseDataHelper();
  TextEditingController _amountController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List<ExpenseSheet> excategorylist; /////////////show category list///////
  ExpenseSheet expenseSheet;
  int updateIndex;
  List icon = [];
  List nonRepetitive = [];
  List catDate = [];
  List catDateCheck = [];
  List allData = [];
  List total = [];
  List dataValue = [];
  List newCat = [];
  List checkINC = [];
  String ic = "";
  String nte = "";
  String inbalnc;
  int inbalanc;
  int indx = 0;
  int icoindx = 0;
  Database _database;

  @override
  void initState() {
    checkDB();
    expenseData();
    super.initState();
  }

  Future<void> checkDB() async {
    if (_database == null) {
      _database = await openDatabase(
          Path.join(await getDatabasesPath(), "Expense.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE expenseSheet(id INTEGER PRIMARY KEY autoincrement, selectCat TEXT, selectIcon TEXT, amount TEXT, date TEXT, note TEXT)",
        );
      });
    }
  }

  void expenseData() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData = await inhelper.getExpenseSheetList();

      for (int i = 0; i < _userAuthData.length; i++) {
        /////amount calculation///
        setState(() {
          ExpenseSheet cat = _userAuthData[i];
          //print(cat.selectCat + " - " + cat.date);

          if (!catDate.contains(cat.date)) {
            catDate.add(cat.date);
            allData.add(cat);
            checkdata(cat.selectCat, cat.date);

            print("cat.selectCat");
            print(cat.selectCat);
            print("cat.selectIcon");
            print(cat.selectIcon);
          }

          // if (!nonRepetitive.contains(cat.selectCat)) {
          //   //print("$i");
          //   nonRepetitive.add(cat.selectCat);
          //   //allData.add(cat);
          //   //checkTotal(cat.selectCat); // function for category wise calculation
          //   checkdata(cat.selectCat, cat.date);
          // }
        });
      }

      print("allData.length");
      print(allData.length);

      for (int i = 0; i < allData.length; i++) {
        ExpenseSheet ish = allData[i];
        print("ish.date");
        print(ish.date);
        Future.delayed(Duration.zero, () async {
          final _userAuthData1 =
              await inhelper.getAllExpenseSheetList(ish.date);

          print("_userAuthData1.length");
          print(_userAuthData1.length);

          newCat = [];
          checkINC = [];

          for (int j = 0; j < _userAuthData1.length; j++) {
            ExpenseSheet inc = _userAuthData1[j];

            String category = inc.selectCat;

            if (!newCat.contains(category)) {
              newCat.add(category);
              checkINC.add(inc);
            }

            print(category);
            print("_userAuthData1[j].selectCat");
            print(_userAuthData1[j].selectCat);
          }

          setState(() {
            catDateCheck.add({"date": ish.date, "list": checkINC});
          });
        });
      }

      print("data.length");
      print(dataValue.length);
    });
  }

  Future<void> checkdata(catergory, date) async {
    final _userAuthData = await inhelper.getDatewiseExpenseSheetList(
        catergory, date); // search with category name
    double totAmt = 0;

    for (int i = 0; i < _userAuthData.length; i++) {
      /////amount calculation///

      setState(() {
        ExpenseSheet cat = _userAuthData[i];

        String amt = cat.amount;
        double amtInt = double.parse(amt);

        totAmt = totAmt + amtInt; // calculate category wise amount
      });
    }
    total.add({
      "amt": "$totAmt"
    }); // amount added to another array. here category wise amount is added.
  }

  @override
  Widget build(BuildContext context) {
    return allData == null || total == null
        ? CircularProgressIndicator()
        : Container(
            padding: EdgeInsets.only(bottom: 50),
            color: Theme.of(context).backgroundColor,
            child: Column(
              children: <Widget>[
                SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    margin: EdgeInsets.only(top: 0, right: 20),
                    child: Column(
                      key: _formKey,
                      children: List.generate(catDateCheck.length, (int index) {
                        String inDate = catDateCheck[index]["date"];
                        return Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                padding: EdgeInsets.only(bottom: 0, left: 25),
                                alignment: Alignment.topLeft,
                                child: Text(
                                  inDate,
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: 18.0,
                                      decoration: TextDecoration.none,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              catDateCheck == null
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Column(
                                        children: List.generate(
                                            catDateCheck[index]["list"].length,
                                            (ind) {
                                          ExpenseSheet incateg =
                                              catDateCheck[index]["list"][ind];
                                          String selectCat = incateg.selectCat;

                                          // /////amount calculation///
                                          int i = 0;
                                          int len = -1;
                                          int icons = -1;

                                          String amt = incateg.amount;
                                          String nt = incateg.note;
                                          String inDates = incateg.date;

                                          String icon = catDateCheck[index]
                                                  ['list'][ind]
                                              .selectIcon;

                                          print("icon");
                                          print(icon);
                                          if (icon != "") {
                                            icons = int.parse(icon);
                                          } else {
                                            icons = 0;
                                          }

                                          return Slidable(
                                            actionPane:
                                                SlidableDrawerActionPane(),

                                            actionExtentRatio: 0.20,
                                            child: GestureDetector(
                                              /////item call & select from list///
                                              onTap: () {
                                                setState(() {
                                                  icn = incateg
                                                      .selectIcon; ////global ver///

                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ExpenseCollections(
                                                                  incateg
                                                                      .selectCat,
                                                                  incateg
                                                                      .date)));
                                                });
                                                /////item select from list///
                                              },
                                              child: Container(
                                                  margin: EdgeInsets.only(
                                                    bottom: 2,
                                                    left: 20,
                                                  ),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10, 15, 10, 15),
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .cardColor,
                                                      /////border:  Border(bottom: BorderSide(color: Colors.grey[200]))///
                                                      // borderRadius: i == 0
                                                      //     ? BorderRadius.only(
                                                      //         topLeft: Radius.circular(10.0),
                                                      //         topRight: Radius.circular(10.0))
                                                      //     : i == len
                                                      //         ? BorderRadius.only(
                                                      //             bottomRight: Radius.circular(10.0),
                                                      //             bottomLeft: Radius.circular(10.0))
                                                      //         : BorderRadius.only(
                                                      //             topLeft: Radius.circular(0.0),
                                                      //             topRight: Radius.circular(0.0)),

                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                          // color: Colors.yellow,
                                                          child: Row(
                                                            children: <Widget>[
                                                              //////////////  Icon Container  /////////////
                                                              Container(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(6),
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            10.0),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .backgroundColor,
                                                                  borderRadius:
                                                                      BorderRadius.all(
                                                                          Radius.circular(
                                                                              20.0)),
                                                                ),
                                                                child: Icon(
                                                                  iconList[
                                                                          icons]
                                                                      ['name'],
                                                                  size: 22,
                                                                  color: Theme.of(
                                                                          context)
                                                                      .iconTheme
                                                                      .color,
                                                                ),
                                                              ),
                                                              Expanded(
                                                                child:
                                                                    Container(
                                                                    //color: Colors.blue,
                                                                    child: Text(
                                                                    "$selectCat",
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    style: TextStyle(
                                                                        color: Theme.of(context)
                                                                            .accentColor,
                                                                        fontSize:
                                                                            17.0,
                                                                        fontFamily:
                                                                            'Roboto',
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                    ),
                                                                  ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        //color: Colors.green,
                                                        child: Column(
                                                          children: <Widget>[
                                                            FutureBuilder(
                                                              future: inhelper
                                                                  .getDatewiseExpenseSheetList(
                                                                      incateg
                                                                          .selectCat,
                                                                      incateg
                                                                          .date),
                                                              builder: (context,
                                                                  snapshot) {
                                                                if (!snapshot
                                                                    .hasData)
                                                                  return Center(
                                                                      child:
                                                                          CircularProgressIndicator());
                                                                double totAmt = 0;
                                                                for (int i = 0;
                                                                    i <
                                                                        snapshot
                                                                            .data
                                                                            .length;
                                                                    i++) {
                                                                  ExpenseSheet
                                                                      incateg1 =
                                                                      snapshot
                                                                          .data[i];

                                                                  String amnt =
                                                                      incateg1
                                                                          .amount;
                                                                  double amtInt =
                                                                      double.parse(
                                                                          amnt);
                                                                  totAmt +=
                                                                      amtInt;
                                                                }
                                                                print("totAmt");
                                                                print(totAmt);
                                                                return Text(
                                                                  "$totAmt", //amount from array has been shown. if two categories, then two amount total will be added and shown
                                                                  style: TextStyle(
                                                                      color: Color(
                                                                          0xFF9F2A2A),
                                                                      fontSize:
                                                                          15.0,
                                                                      fontFamily:
                                                                          'Roboto',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .normal),
                                                                );
                                                              },
                                                            ),
                                                            SizedBox(
                                                              height: 8.0,
                                                            ),
                                                            Text(
                                                              '$inDates',
                                                              style: TextStyle(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .accentColor,
                                                                  fontSize: 8.0,
                                                                  fontFamily:
                                                                      'Roboto',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  )),
                                            ),
                                            /////////slide edit delete////
                                            secondaryActions: <Widget>[
                                              Container(
                                                margin: EdgeInsets.only(
                                                  top: 2,
                                                  bottom: 2,
                                                ),
                                                child: new IconSlideAction(
                                                  // caption: 'Edit',
                                                  color: Color(0XFFEFEFEF),
                                                  //icon: Icons.edit,
                                                  iconWidget: Icon(
                                                    Icons.edit,
                                                    color: Color(0XFF1A3D7A),
                                                  ),
                                                  onTap: () {
                                                    ////object create////

                                                    //Navigator.push(context, MaterialPageRoute(builder:(context) => AddTransaction()));
                                                    setState(() {
                                                      var incomeObject = {
                                                        "id": incateg.id,
                                                        "date": inDate,
                                                        "incategory":
                                                            incateg.selectCat,
                                                        "icon":
                                                            incateg.selectIcon,
                                                        "amount": amt,
                                                        "note": nt
                                                      };

                                                      expenseSheet =
                                                          incateg; // current row data
                                                      updateIndex =
                                                          index; // current index
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  TransactionEdit(
                                                                      status,
                                                                      incomeObject,
                                                                      expenseSheet,
                                                                      updateIndex)));
                                                    });
                                                  }, // _showSnackBar('More'),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                  top: 2,
                                                  bottom: 2,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius: i == 0
                                                      ? BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  5.0),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  5.0),
                                                        )
                                                      : i == len
                                                          ? BorderRadius.only(
                                                              bottomRight:
                                                                  Radius
                                                                      .circular(
                                                                          10.0),
                                                            )
                                                          : BorderRadius.only(
                                                              bottomRight:
                                                                  Radius
                                                                      .circular(
                                                                          0.0),
                                                              topRight: Radius
                                                                  .circular(
                                                                      0.0)),
                                                ),
                                                child: new IconSlideAction(
                                                  // caption: 'Delete',
                                                  color: Colors.transparent,
                                                  icon: Icons.delete,
                                                  onTap: () {
                                                    _showDeleteDialog(
                                                        incateg, index);
                                                  },
                                                ),
                                              ),
                                            ],
                                          );
                                        }),
                                      ),
                                    )
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
                  ////////show category in the another screen////
                ),
              ],
            ),
          );
  }

  ///////Dialog ////////////////////
  ///
  void _showDeleteDialog(incat, index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Container(
                        child: new Text(
                          "Do you want to delete this transaction?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0XFF1A3D7A),
                            fontSize: 19.0,
                            decoration: TextDecoration.none,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    //color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ///////// Cancel Button Start /////
                        Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                color: Colors.white),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 12, right: 12),
                                child: Text(
                                  'Cancel',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Colors.white,
                              elevation: 2.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            )),

                        ///////// Cancle Button end /////

                        ///////// Ok Button Start /////
                        Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              color: Color(0XFFFB7676),
                            ),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                inhelper.deleteExpenseSheet(incat.id);
                                setState(() {
                                  excategorylist.removeAt(index);
                                  Navigator.of(context).pop();
                                });
                              },

                              child: Container(
                                padding: EdgeInsets.only(left: 25, right: 25),
                                child: Text(
                                  'Ok',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Color(0XFFFB7676),
                              elevation: 2.0,
                              //splashColor: Colors.blueGrey,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),

                        ///////// Ok Button end /////
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
