import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:expanse_manegment/EditPage/TransactionEdit.dart';
import 'package:expanse_manegment/database/IncomeData.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import '../../../main.dart';

class IncomeCollections extends StatefulWidget {
  final inNotes;
  final date;
  IncomeCollections(this.inNotes, this.date);

  @override
  _IncomeCollectionsState createState() => _IncomeCollectionsState();
}

class _IncomeCollectionsState extends State<IncomeCollections> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var status = 1;
  var inote = 1;

  final IncomeDataHelper inhelper = new IncomeDataHelper();

  final _formKey = GlobalKey<FormState>();
  //List<IncomeSheet> incategorylist = []; /////////////show category list///////
  //List<IncomeSheet> nonRepetitive = [];
  List allData = [];
  IncomeSheet incomeSheet;
  int updateIndex;
  List icon = [];
  String ic = "";
  String nte = "";
  String inbalnc;
  int inbalanc;
  int indx = 0;
  int icoindx = 0;
  //int inblnc = 0;
  bool inte = false;

  @override
  void initState() {
   
    //checkdata(widget.inNote, widget.date);
    super.initState();
  }

  // void checkdata(catergory, date) async {
  //   final _userAuthData = await inhelper.getNoteIncomeSheetList(
  //       catergory, date); // search with category name

  //   print("_userAuthData.length");
  //   print(_userAuthData.length);
  // }

// void innotetotal() {
//     Future.delayed(Duration.zero, () async {
//       final _userAuthData = await inhelper.getIncomeSheetList();
//       print("_userAuthData.length");
//       print(_userAuthData.length);

//       for (int i = 0; i < _userAuthData.length; i++) {
//       /////amount calculation///
//         setState(() {
//           String inbalnc = _userAuthData[i].amount;
//           int inbalanc = int.parse(inbalnc);
//           print("inbalanc.toString()");
//           print(inbalanc.toString());
//           incomeTotal += inbalanc; //calculation
//           print("incomeTotal");
//           print(incomeTotal);
//         });
//       }
//     });
//   }
//////method for expense calculation/////
  // void expensetotal() {
  //   Future.delayed(Duration.zero, () async {
  //     final _userAuthData = await exhelper.getExpenseSheetList();
  //     print("_userAuthData.length");
  //     print(_userAuthData.length);

  //     for (int i = 0; i < _userAuthData.length; i++) {
  //        /////amount calculation///
  //       setState(() {
  //         String exbalnc = _userAuthData[i].amount;
  //         int exbalanc = int.parse(exbalnc);
  //         print("exbalanc.toString()");
  //         print(exbalanc.toString());
  //         expenseTotal += exbalanc; //calculation
  //         print("expenseTotal");
  //         print(expenseTotal);
  //       });
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor:Theme.of(context).backgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(90.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            titleSpacing: 2,
            automaticallyImplyLeading: false,
            leading: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Icon(
                  Icons.arrow_back,
                  size: 28.0,
                  color: Color(0XFFF2F4F7),
                ),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text(
                widget.inNotes,
               // 'Income Collections',
                  style: TextStyle(
                    color: Color(0XFFF2F4F7),
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    fontFamily: 'Roboto',
                  )),
            ),
          ),
        ),
        body: Container(
         // padding: EdgeInsets.only(bottom: 50),
         // color: Theme.of(context).backgroundColor,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            ////////show category in the another screen////
            child: Column(
              children: <Widget>[
                FutureBuilder(
                  future: inhelper.getNoteIncomeSheetList(widget.inNotes),
                  builder: (context, snapshot) {
                    allData = snapshot.data;

                    // for (var i = 0; i < incategorylist.length; i++) {
                    //   bool repeated = false;
                    //   for (var j = 0; j < nonRepetitive.length; j++) {
                    //     if (incategorylist[i] == nonRepetitive[j]) {
                    //       repeated = true;
                    //     }
                    //   }
                    //   if (!repeated) {
                    //     nonRepetitive.add(incategorylist[i]);
                    //   }
                    // }
                    // print(nonRepetitive);

                    if (!snapshot.hasData)
                      return Center(child: CircularProgressIndicator());
                    //////////for scrolling use list.generate/////////
                    return Container(
                      margin: EdgeInsets.only(top: 30, right: 20),
                      child: Column(
                        key: _formKey,
                        children: List.generate(allData.length, (int index) {
                          IncomeSheet incat = allData[index];

                          //String selectCat = incat.selectCat;

                          // /////amount calculation///
                          int i = 0;
                          int len = -1;

                          // String ind = "${incat.selectIcon}"; //icon index with string
                          // int intIndex = int.parse(ind); //string to integer

                          // indx = intIndex;
                          // print("ind");
                          // print(ind);
                          // print("indx");
                          // print(indx);

                          String amtString = incat.amount;
                           double amt = double.parse(amtString);
                          String inDate = incat.date;

                          String nt = incat.note;
                          String method = incat.method;
                          String iconName = incat.selectIcon;
                          int iInt = int.parse(iconName);
                          Uint8List bytes;

                          if (incat.file != "") {
                            bytes = Base64Decoder().convert(incat.file);
                          }

                          return inDate != widget.date
                              ? Container()
                              : Slidable(
                                  actionPane: SlidableDrawerActionPane(),

                                  actionExtentRatio: 0.20,
                                  child:
                                   GestureDetector(
                                    /////item call & select from list///
                                    // onTap: () {
                                    //   setState(() {
                                    //     icn =
                                    //         incat.selectIcon; ////global ver///

                                    //     Navigator.pop(context);
                                    //   });
                                    //   /////item select from list///
                                    // },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                          bottom: 2,
                                          left: 20,
                                        ),
                                        padding:
                                            EdgeInsets.fromLTRB(10, 15, 10, 15),
                                        decoration: BoxDecoration(
                                            color: Theme.of(context).cardColor,
                                            /////border:  Border(bottom: BorderSide(color: Colors.grey[200]))///
                                            // borderRadius: i == 0
                                            //     ? BorderRadius.only(
                                            //         topLeft: Radius.circular(10.0),
                                            //         topRight: Radius.circular(10.0))
                                            //     : i == len
                                            //         ? BorderRadius.only(
                                            //             bottomRight: Radius.circular(10.0),
                                            //             bottomLeft: Radius.circular(10.0))
                                            //         : BorderRadius.only(
                                            //             topLeft: Radius.circular(0.0),
                                            //             topRight: Radius.circular(0.0)),

                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                // color: Colors.yellow,
                                                child: Row(
                                                  children: <Widget>[
                                                    //////////////  Icon Container  /////////////
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(6),
                                                      margin: EdgeInsets.only(
                                                          right: 10.0),
                                                      decoration: BoxDecoration(
                                                        color: Theme.of(context)
                                                            .backgroundColor,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    20.0)),
                                                      ),
                                                      child: Icon(
                                                       iconList[iInt]['name'],
                                                        size: 22,
                                                        color: Theme.of(context)
                                                            .iconTheme
                                                            .color,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          // Navigator.push(
                                                          //     context,
                                                          //     MaterialPageRoute(
                                                          //         builder: (context) => Collections()));
                                                        },
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Container(
                                                              //color: Colors.blue,
                                                              child: Text(
                                                                '$nt',
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .accentColor,
                                                                    fontSize:
                                                                        17.0,
                                                                    fontFamily:
                                                                        'Roboto',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                            Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  //color: Colors.blue,
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              3),
                                                                  decoration: BoxDecoration(
                                                                      color: Color(
                                                                              0XFF294F95)
                                                                          .withOpacity(
                                                                              0.1),
                                                                      borderRadius:
                                                                          BorderRadius.all(
                                                                              Radius.circular(4.0))),
                                                                  child: Text(
                                                                    '$method',
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    style: TextStyle(
                                                                        color: Theme.of(context)
                                                                            .accentColor,
                                                                        fontSize:
                                                                            11.0,
                                                                        fontFamily:
                                                                            'Roboto',
                                                                        fontWeight:
                                                                            FontWeight.w300),
                                                                  ),
                                                                ),
                                                                incat.file != ""
                                                                    ?
                                                                    // Container(
                                                                    //     height: 50,
                                                                    //     child: Image
                                                                    //         .memory(
                                                                    //             bytes))
                                                                    GestureDetector(
                                                                        onTap:
                                                                            () {
                                                                          saveToGallery(
                                                                              incat.file,
                                                                              index);
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          //color: Colors.blue,
                                                                          padding:
                                                                              EdgeInsets.all(3),
                                                                          margin: EdgeInsets.only(
                                                                              top: 3,
                                                                              left: 5),
                                                                          decoration: BoxDecoration(
                                                                              color: Color(0XFF294F95).withOpacity(0.1),
                                                                              borderRadius: BorderRadius.all(Radius.circular(4.0))),
                                                                          child:
                                                                              Text(
                                                                            'Download',
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            style: TextStyle(
                                                                                color: Theme.of(context).accentColor,
                                                                                fontSize: 11.0,
                                                                                fontFamily: 'Roboto',
                                                                                fontWeight: FontWeight.w300),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Container()
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),

                                                    //   Text(
                                                    //   '$nt',
                                                    //   style: TextStyle(
                                                    //       color: Color(0xFF9F2A2A),
                                                    //       fontSize: 15.0,
                                                    //       fontFamily: 'Roboto',
                                                    //       fontWeight: FontWeight.normal),
                                                    // ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              //color: Colors.green,
                                              child: Column(
                                                children: <Widget>[
                                                  Text(
                                                    amt.toStringAsFixed(2),
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xFF9F2A2A),
                                                        fontSize: 15.0,
                                                        fontFamily: 'Roboto',
                                                        fontWeight:
                                                            FontWeight.normal),
                                                  ),
                                                  SizedBox(
                                                    height: 8.0,
                                                  ),
                                                  Text(
                                                    '$inDate',
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                        fontSize: 8.0,
                                                        fontFamily: 'Roboto',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        )),
                                  ),
                                  /////////slide edit delete////
                                  secondaryActions: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 2,
                                        bottom: 2,
                                      ),
                                      child: new IconSlideAction(
                                        // caption: 'Edit',
                                        color: Color(0XFFEFEFEF),
                                        //icon: Icons.edit,
                                        iconWidget: Icon(
                                          Icons.edit,
                                          color: Color(0XFF1A3D7A),
                                        ),
                                        onTap: () {
                                          ////object create////

                                          //Navigator.push(context, MaterialPageRoute(builder:(context) => AddTransaction()));
                                          setState(() {
                                            var incomeObject = {
                                              "id": incat.id,
                                              "date": inDate,
                                              "incategory": incat.selectCat,
                                              "icon": incat.selectIcon,
                                              "amount": amt,
                                              "note": nt,
                                            };

                                            incomeSheet =
                                                incat; // current row data
                                            updateIndex =
                                                index; // current index
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TransactionEdit(
                                                            status,
                                                            incomeObject,
                                                            incomeSheet,
                                                            updateIndex)));
                                          });
                                        }, // _showSnackBar('More'),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 2,
                                        bottom: 2,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: i == 0
                                            ? BorderRadius.only(
                                                topRight: Radius.circular(5.0),
                                                bottomRight:
                                                    Radius.circular(5.0),
                                              )
                                            : i == len
                                                ? BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(10.0),
                                                  )
                                                : BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(0.0),
                                                    topRight:
                                                        Radius.circular(0.0)),
                                      ),
                                      child: new IconSlideAction(
                                        // caption: 'Delete',
                                        color: Colors.transparent,
                                        icon: Icons.delete,
                                        onTap: () {
                                          _showDeleteDialog(incat, index);
                                        },
                                      ),
                                    ),
                                  ],
                                );
                        }),
                      ),
                    );
                  },
                ),
                    SizedBox(height:30)
              ],
            ),
            ////////show category in the another screen////
          ),
        ));
  }

  Future<String> saveToGallery(files, ind) async {
    String encodedStr = files;
    Uint8List bytes = base64.decode(encodedStr);
    String dir = (await getApplicationDocumentsDirectory()).path;
    String fullPath = '$dir/income ${ind + 1}.png';
    print("local file full path $fullPath");
    File file = File(fullPath);
    await file.writeAsBytes(bytes);
    print(file.path);

    final result = await ImageGallerySaver.saveImage(bytes);
    print(result);

    _showMsg('Saved to ' + result);

    return file.path;
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  ///////Dialog ////////////////////
  ///
  void _showDeleteDialog(incat, index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Container(
                        child: new Text(
                          "Do you want to delete this transaction?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0XFF1A3D7A),
                            fontSize: 19.0,
                            decoration: TextDecoration.none,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    //color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ///////// Cancel Button Start /////
                        Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                color: Colors.white),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 12, right: 12),
                                child: Text(
                                  'Cancel',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Colors.white,
                              elevation: 2.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            )),

                        ///////// Cancle Button end /////

                        ///////// Ok Button Start /////
                        Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              color: Color(0XFFFB7676),
                            ),
                            height: 45,
                            child: RaisedButton(
                              onPressed: () {
                                inhelper.deleteIncomeSheet(incat.id);
                                setState(() {
                                  allData.removeAt(index);
                                  Navigator.of(context).pop();
                                });
                              },

                              child: Container(
                                padding: EdgeInsets.only(left: 25, right: 25),
                                child: Text(
                                  'Ok',
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Color(0XFF1A3D7A),
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              color: Color(0XFFFB7676),
                              elevation: 2.0,
                              //splashColor: Colors.blueGrey,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),

                        ///////// Ok Button end /////
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
