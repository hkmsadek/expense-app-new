import 'dart:convert';
import 'dart:typed_data';

import 'package:expanse_manegment/Screens/Category/categoryMainPage.dart';
import 'package:expanse_manegment/Screens/Chart/mainChart.dart';
import 'package:expanse_manegment/Screens/Compare/compareSwitch.dart';
import 'package:expanse_manegment/Screens/HomePage/homePage.dart';
import 'package:expanse_manegment/Screens/LoginPage/loginPage.dart';
import 'package:expanse_manegment/Screens/Settings/settings.dart';
import 'package:expanse_manegment/database/ProfilePic.dart';
import 'package:expanse_manegment/database/RegistrationData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerOnly extends StatefulWidget {
  @override
  _DrawerOnlyState createState() => _DrawerOnlyState();
}

class _DrawerOnlyState extends State<DrawerOnly> {
  var drawerStatus = 5;
  RegistrationHelper helper = new RegistrationHelper();
  PictureHelper picHelper = new PictureHelper();
  String userName = "", picture = "";
  Uint8List bytes;

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('user');

    if (user != null) {
      List us = user.split("~");
      setState(() {
        userName = us[3];
      });
    }

    pictureGet();
  }

  void pictureGet() {
    Future.delayed(Duration.zero, () async {
      final _userAuthData2 = await picHelper.getPicture("$userName");

      print("_userAuthData2.length");
      print(_userAuthData2.length);

      if(_userAuthData2.length!=0){
             for (int j = 0; j < _userAuthData2.length; j++) {
        Picture pic = _userAuthData2[j];

        picture = pic.pic;
      }

      setState(() {
        bytes = Base64Decoder().convert(picture);
      });
      }
   

      print(bytes);
    });
  }

  @override
  Widget build(BuildContext context) {
    ///////DAte//////////
    DateTime selectedDate = DateTime.now();
    final f = new DateFormat('yyyy-MM-dd');

    Future<Null> _selectDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
          context: context,
          initialDate: selectedDate,
          firstDate: DateTime(1964, 8),
          lastDate: DateTime(2101));
      if (picked != null && picked != selectedDate)
        setState(() {
          selectedDate = picked;
        });
    }

    return new Drawer(
        child: new ListView(
      children: <Widget>[
        new DrawerHeader(
          child: Container(
              // decoration: new BoxDecoration(
              //     border: Border.all(
              //     width: 3,
              //       color:Color(0xFF01d56a).withOpacity(0.4),

              //     ),
              //     shape: BoxShape.circle
              //     ),
              child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              ClipOval(
                child: bytes == null || bytes == ""
                    ? Image.asset(
                        'assets/img/profile.png',
                        height: 90,
                        width: 90,
                        fit: BoxFit.cover,
                      )
                    : Image.memory(
                        bytes,
                        height: 90,
                        width: 90,
                        fit: BoxFit.cover,
                      ),
              ),
              FutureBuilder(
                future: helper.getProfile(userName),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());
                  String fn = "", ln = "";
                  for (int i = 0; i < snapshot.data.length; i++) {
                    Registration reg = snapshot.data[i];

                    fn = reg.firstName;
                    ln = reg.lastName;
                  }

                  print(fn);
                  print(ln);
                  return Text(
                    "$fn $ln", //amount from array has been shown. if two categories, then two amount total will be added and shown
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 18.0,
                      decoration: TextDecoration.none,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.bold,
                    ),
                  );
                },
              ),
            ],
          )),

          //////   camera icon  ////////////
          // decoration: new BoxDecoration(
          //     color: Colors.orange
          // ),
        ),
        Divider(
          color: Theme.of(context).accentColor,
            height: 2.1,
        ),

        //////////// Home Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.home,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Home",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(context,
                new MaterialPageRoute(builder: (ctxt) => new HomePage()));
          },
        ),
        //////////// Home Button End////////////////

        //////////// Calendar Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.calendar_today,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Calendar",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            _selectDate(context);
          },
        ),
        //////////// Calendar Button End////////////////

        //////////// Chart Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.pie_chart,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Chart",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(context,
                new MaterialPageRoute(builder: (ctxt) => new Charts()));
          },
        ),
        //////////// Chart Button End////////////////

        //////////// Category Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.view_list,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Category",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            Navigator.pop(context);

            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (ctxt) => CategoryPage(drawerStatus)));
          },
        ),
        //////////// Category Button End////////////////

//////////// Compare Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.repeat,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Compare",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(context,
                new MaterialPageRoute(builder: (ctxt) => new CompareSwitch()));
          },
        ),
        //////////// Compare Button End////////////////

        //////////// Export Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.import_export,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Export",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            // Navigator.pop(ctxt);
            // Navigator.push(ctxt,
            //     new MaterialPageRoute(builder: (ctxt) => new FirstPage()));
          },
        ),
        //////////// Export Button End////////////////

        //////////// Settings Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.settings,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Settings",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(context,
                new MaterialPageRoute(builder: (ctxt) => new Settings()));
          },
        ),
        //////////// Settings Button End////////////////

        /////////// Logout Button Start////////////////
        new ListTile(
          contentPadding: EdgeInsets.only(left: 35),
          leading: Icon(
            Icons.exit_to_app,
            color: Theme.of(context).accentColor,
          ),
          title: new Text(
            "Log Out",
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 17.0,
              decoration: TextDecoration.none,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {
              Navigator.pop(context);
            _showLogoutDialog();
          },
        ),
        //////////// Logout Button End////////////////
      ],
    ));
  }

  Future<Null> _showLogoutDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 0, bottom: 10),
                        child: Text(
                          "Do you want to logout?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 18,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                             
                                Navigator.of(context).pop();
                             
                            },
                            child: Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.2),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: logout,
                            child: Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future logout() async {
    Navigator.of(context).pop();
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }
}
