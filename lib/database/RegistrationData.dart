import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class RegistrationHelper {
  Database _database;

  Future openDb() async {
    if (_database == null) {
      _database = await openDatabase(
          join(await getDatabasesPath(), "registration.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE Registration(id INTEGER PRIMARY KEY autoincrement, firstName TEXT, lastName TEXT, userName TEXT, mobile TEXT, email TEXT, password TEXT, confpassword TEXT)",
        );
      });
    }
  }

  Future<int> insertRegistration(Registration registration) async {
    await openDb();
    return await _database.insert('Registration', registration.toMap());
  }

  Future<List<Registration>> getRegistrationList() async {
    await openDb();

    final List<Map<String, dynamic>> maps =
        await _database.query('Registration');
    return List.generate(maps.length, (i) {
      return Registration(
          id: maps[i]['id'],
          firstName: maps[i]['firstName'],
          lastName: maps[i]['lastName'],
          userName: maps[i]['userName'],
          mobile: maps[i]['mobile'],
          email: maps[i]['email'],
          password: maps[i]['password'],
          confpassword: maps[i]['confpassword']);
    });
  }

  Future<List<Registration>> getLoginList(String username, String phone) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM Registration where userName = '$username' and mobile = '$phone'");
    List<Registration> registration = new List();

    for (int i = 0; i < list.length; i++) {
      registration.add(new Registration(
          id: list[i]['id'],
          firstName: list[i]['firstName'],
          lastName: list[i]['lastName'],
          userName: list[i]['userName'],
          mobile: list[i]['mobile'],
          email: list[i]['email'],
          password: list[i]['password'],
          confpassword: list[i]['confpassword']));
    }
    return registration;
  }

  Future<List<Registration>> getProfile(String username) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM Registration where userName = '$username'");
    List<Registration> registration = new List();

    for (int i = 0; i < list.length; i++) {
      registration.add(new Registration(
          id: list[i]['id'],
          firstName: list[i]['firstName'],
          lastName: list[i]['lastName'],
          userName: list[i]['userName'],
          mobile: list[i]['mobile'],
          email: list[i]['email'],
          password: list[i]['password'],
          confpassword: list[i]['confpassword']));
    }
    return registration;
  }

  Future<int> updateRegistration(Registration registration) async {
    await openDb();
    return await _database.update('Registration', registration.toMap(),
        where: "id = ?", whereArgs: [registration.id]);
  }

  Future<Void> deleteRegistration(int id) async {
    await openDb();
    await _database.delete('Registration', where: "id = ?", whereArgs: [id]);
  }
}

class Registration {
  int id;
  String firstName;
  String lastName;
  String userName;
  String mobile;
  String email;
  String password;
  String confpassword;

  Registration({
    this.firstName,
    this.lastName,
    this.userName,
    this.mobile,
    this.email,
    this.password,
    this.confpassword,
    this.id,
  });
  Map<String, dynamic> toMap() {
    return {
      'firstName': firstName,
      'lastName': lastName,
      'userName': userName,
      'mobile': mobile,
      'email': email,
      'password': password,
      'confpassword': confpassword,
    };
  }
}
