import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class CategoryHelper {
  Database _database;

  Future openDb() async {
    if (_database == null) {
      _database = await openDatabase(
          join(await getDatabasesPath(), "cat_name.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE Category(id INTEGER PRIMARY KEY autoincrement, categoryName TEXT, iconName TEXT)",
        );
      });
    }
  }

  Future<int> insertCategory(Category category) async {
    await openDb();
    return await _database.insert('Category', category.toMap());
  }

  Future<List<Category>> getCategoryList() async {
    await openDb();

    final List<Map<String, dynamic>> maps = await _database.query('Category');
    return List.generate(maps.length, (i) {
      return Category(
          id: maps[i]['id'],
          categoryName: maps[i]['categoryName'],
          iconIndex: maps[i]['iconName']);
    });
  }

  Future<int> updateCategory(Category category) async {
    await openDb();
    return await _database.update('Category', category.toMap(),
        where: "id = ?", whereArgs: [category.id]);
  }

  Future<Void> deleteCategory(int id) async {
    await openDb();
    await _database.delete('Category', where: "id = ?", whereArgs: [id]);
  }
}

class Category {
  int id;
  String categoryName;
  String iconIndex;

  Category({
    this.categoryName,
    this.iconIndex,
    this.id,
  });
  Map<String, dynamic> toMap() {
    return {'categoryName': categoryName, 'iconName': iconIndex};
  }
}
