import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class PictureHelper {
  Database _database;

  Future openDb() async {
    if (_database == null) {
      _database = await openDatabase(
          join(await getDatabasesPath(), "Picture.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE Profile(id INTEGER PRIMARY KEY autoincrement, pic TEXT, userName TEXT)",
        );
      });
    }
  }

  Future<int> insertPicture(Picture registration) async {
    await openDb();
    return await _database.insert('Profile', registration.toMap());
  }

  Future<List<Picture>> getPicture(String userName) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM Profile where userName = '$userName'");
    List<Picture> registration = new List();

    for (int i = 0; i < list.length; i++) {
      registration.add(new Picture(
          id: list[i]['id'],
          pic: list[i]['pic'],
          userName: list[i]['userName'],
          ));
    }
    return registration;
  }

  Future<int> updatePicture(Picture registration) async {
    await openDb();
    return await _database.update('Picture', registration.toMap(),
        where: "id = ?", whereArgs: [registration.id]);
  }
}

class Picture {
  int id;
  String pic;
  String userName;

  Picture({
    this.pic,
    this.userName,
    this.id,
  });
  Map<String, dynamic> toMap() {
    return {
      'pic': pic,
      'userName': userName,
    };
  }
}
