import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ExpenseDataHelper {
  Database _database;

  Future openDb() async {
    if (_database == null) {
      _database = await openDatabase(
          join(await getDatabasesPath(), "ExpenseDB.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE expenseSheet(id INTEGER PRIMARY KEY autoincrement, selectCat TEXT, selectIcon TEXT, amount TEXT, date TEXT, date1 TEXT, note TEXT, method TEXT, file TEXT)",
        );
      });
    }
  }

  Future<int> insertExpenseSheet(ExpenseSheet expenseSheet) async {
    await openDb();
    return await _database.insert('expenseSheet', expenseSheet.toMap());
  }

  Future<List<ExpenseSheet>> getExpenseSheetList() async {
    await openDb();

    final List<Map<String, dynamic>> maps =
        await _database.query('expenseSheet');
    return List.generate(maps.length, (i) {
      return ExpenseSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<ExpenseSheet>> getNoteExpenseSheetList(String selectCat) async {
    await openDb();

    final List<Map<String, dynamic>> maps = await _database.query(
      'expenseSheet',
      where: "selectCat = ?",
      whereArgs: [selectCat],
    );
    return List.generate(maps.length, (i) {
      return ExpenseSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<ExpenseSheet>> getDateExpenseSheetList(String date) async {
    await openDb();

    final List<Map<String, dynamic>> maps = await _database.query(
      'expenseSheet',
      where: "date = ?",
      whereArgs: [date],
    );
    return List.generate(maps.length, (i) {
      return ExpenseSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<ExpenseSheet>> getAllExpenseSheetList(String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM expenseSheet where date = '$date'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      if (!expenseSheet.contains(list[i]['selectIcon'])) {
        expenseSheet.add(new ExpenseSheet(
          id: list[i]['id'],
          date: list[i]['date'],
          selectCat: list[i]['selectCat'],
          selectIcon: list[i]['selectIcon'],
          amount: list[i]['amount'],
          note: list[i]['note'],
          method: list[i]['method'],
          file: list[i]['file'],
        ));
      }
    }
    return expenseSheet;
  }

  Future<List<ExpenseSheet>> getMonthlyExpenseSheetList(String month) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM expenseSheet where date like '%$month%'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      if (!expenseSheet.contains(list[i]['selectIcon'])) {
        expenseSheet.add(new ExpenseSheet(
          id: list[i]['id'],
          date: list[i]['date'],
          selectCat: list[i]['selectCat'],
          selectIcon: list[i]['selectIcon'],
          amount: list[i]['amount'],
          note: list[i]['note'],
          method: list[i]['method'],
          file: list[i]['file'],
        ));
      }
    }
    return expenseSheet;
  }

  Future<List<ExpenseSheet>> getDatewiseExpenseSheetList(
      String selectCat, String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM expenseSheet where selectCat = '$selectCat' and date = '$date'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      expenseSheet.add(new ExpenseSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return expenseSheet;
  }

  Future<List<ExpenseSheet>> getDateRangewiseExpenseSheetList(
      String firstDate, String today) async {
    await openDb();
    //var dbClient = await _database;
    print(
        "SELECT * FROM expenseSheet WHERE date1 BETWEEN '$firstDate' AND '$today'");
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM expenseSheet WHERE date1 BETWEEN '$firstDate' AND '$today'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      expenseSheet.add(new ExpenseSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return expenseSheet;
  }

  Future<List<ExpenseSheet>> getDatewiseNoteList(
      String selectCat, String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM expenseSheet where selectCat = '$selectCat' and date = '$date'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      expenseSheet.add(new ExpenseSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return expenseSheet;
  }

  Future<List<ExpenseSheet>> getYearlyNoteList(String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM expenseSheet where date like '%$date%'");
    List<ExpenseSheet> expenseSheet = new List();

    for (int i = 0; i < list.length; i++) {
      expenseSheet.add(new ExpenseSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return expenseSheet;
  }

  Future<int> updateExpenseSheet(ExpenseSheet expenseSheet) async {
    await openDb();
    return await _database.update('expenseSheet', expenseSheet.toMap(),
        where: "id = ?", whereArgs: [expenseSheet.id]);
  }

  Future<Void> deleteExpenseSheet(int id) async {
    await openDb();
    await _database.delete('expenseSheet', where: "id = ?", whereArgs: [id]);
  }
}

class ExpenseSheet {
  int id;
  String date;
  String date1;
  String selectCat;
  String selectIcon;
  String amount;
  String note;
  String method;
  String file;

  ExpenseSheet({
    this.date,
    this.date1,
    this.selectCat,
    this.selectIcon,
    this.amount,
    this.note,
    this.id,
    this.method,
    this.file,
  });

  String get iconIndex => null;
  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'date1': date1,
      'selectCat': selectCat,
      'selectIcon': selectIcon,
      'amount': amount,
      'note': note,
      'method': method,
      'file': file,
    };
  }
}
