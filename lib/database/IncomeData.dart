import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class IncomeDataHelper {
  Database _database;

  Future openDb() async {
    if (_database == null) {
      _database = await openDatabase(
          join(await getDatabasesPath(), "IncomeDB.db"),
          version: 1, onCreate: (Database db, int version) async {
        await db.execute(
          "CREATE TABLE incomeSheet(id INTEGER PRIMARY KEY autoincrement, selectCat TEXT, selectIcon TEXT, amount TEXT, date TEXT, date1 TEXT, note TEXT, method TEXT, file TEXT)",
        );
      });
    }
  }

  Future<int> insertIncomeSheet(IncomeSheet incomeSheet) async {
    await openDb();
    return await _database.insert('incomeSheet', incomeSheet.toMap());
  }

  Future<List<IncomeSheet>> getIncomeSheetList() async {
    await openDb();

    final List<Map<String, dynamic>> maps =
        await _database.query('incomeSheet');
    return List.generate(maps.length, (i) {
      return IncomeSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<IncomeSheet>> getNoteIncomeSheetList(String selectCat) async {
    await openDb();

    final List<Map<String, dynamic>> maps = await _database.query(
      'incomeSheet',
      where: "selectCat = ?",
      whereArgs: [selectCat],
    );
    return List.generate(maps.length, (i) {
      return IncomeSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<IncomeSheet>> getDateIncomeSheetList(String date) async {
    await openDb();

    final List<Map<String, dynamic>> maps = await _database.query(
      'incomeSheet',
      where: "date = ?",
      whereArgs: [date],
    );
    return List.generate(maps.length, (i) {
      return IncomeSheet(
        id: maps[i]['id'],
        date: maps[i]['date'],
        selectCat: maps[i]['selectCat'],
        selectIcon: maps[i]['selectIcon'],
        amount: maps[i]['amount'],
        note: maps[i]['note'],
        method: maps[i]['method'],
        file: maps[i]['file'],
      );
    });
  }

  Future<List<IncomeSheet>> getAllIncomeSheetList(String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM incomeSheet where date = '$date'");
    List<IncomeSheet> incomeSheet = new List();

    for (int i = 0; i < list.length; i++) {
      if (!incomeSheet.contains(list[i]['selectIcon'])) {
        incomeSheet.add(new IncomeSheet(
          id: list[i]['id'],
          date: list[i]['date'],
          selectCat: list[i]['selectCat'],
          selectIcon: list[i]['selectIcon'],
          amount: list[i]['amount'],
          note: list[i]['note'],
          method: list[i]['method'],
          file: list[i]['file'],
        ));
      }
    }
    return incomeSheet;
  }

  Future<List<IncomeSheet>> getMonthlyIncomeSheetList(String month) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM incomeSheet where date like '%$month%'");
    List<IncomeSheet> incomeSheet = new List();

    for (int i = 0; i < list.length; i++) {
      if (!incomeSheet.contains(list[i]['selectIcon'])) {
        incomeSheet.add(new IncomeSheet(
          id: list[i]['id'],
          date: list[i]['date'],
          selectCat: list[i]['selectCat'],
          selectIcon: list[i]['selectIcon'],
          amount: list[i]['amount'],
          note: list[i]['note'],
          method: list[i]['method'],
          file: list[i]['file'],
        ));
      }
    }
    return incomeSheet;
  }

  Future<List<IncomeSheet>> getDatewiseIncomeSheetList(
      String selectCat, String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM incomeSheet where selectCat = '$selectCat' and date = '$date'");
    List<IncomeSheet> incomeSheet = new List();

    for (int i = 0; i < list.length; i++) {
      incomeSheet.add(new IncomeSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return incomeSheet;
  }

  Future<List<IncomeSheet>> getDateRangewiseIncomeSheetList(
      String firstDate, String today) async {
    print("firstDate");
    print(firstDate);
    print("today");
    print(today);
    await openDb();
    //var dbClient = await _database;
    print(
        "SELECT * FROM incomeSheet WHERE date1 BETWEEN '$today' AND '$firstDate'");
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM incomeSheet WHERE date1 BETWEEN '$today' AND '$firstDate'");
    List<IncomeSheet> incomeSheet = new List();
    print("list");
    print(list);

    for (int i = 0; i < list.length; i++) {
      incomeSheet.add(new IncomeSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return incomeSheet;
  }

  Future<List<IncomeSheet>> getDatewiseNoteList(
      String selectCat, String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database.rawQuery(
        "SELECT * FROM incomeSheet where selectCat = '$selectCat' and date = '$date'");
    List<IncomeSheet> incomeSheet = new List();

    for (int i = 0; i < list.length; i++) {
      incomeSheet.add(new IncomeSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return incomeSheet;
  }

  Future<List<IncomeSheet>> getYearlyNoteList(String date) async {
    await openDb();
    //var dbClient = await _database;
    List<Map> list = await _database
        .rawQuery("SELECT * FROM incomeSheet where date like '%$date%'");
    List<IncomeSheet> incomeSheet = new List();

    for (int i = 0; i < list.length; i++) {
      incomeSheet.add(new IncomeSheet(
        id: list[i]['id'],
        date: list[i]['date'],
        selectCat: list[i]['selectCat'],
        selectIcon: list[i]['selectIcon'],
        amount: list[i]['amount'],
        note: list[i]['note'],
        method: list[i]['method'],
        file: list[i]['file'],
      ));
    }
    return incomeSheet;
  }

  Future<int> updateIncomeSheet(IncomeSheet incomeSheet) async {
    await openDb();
    return await _database.update('incomeSheet', incomeSheet.toMap(),
        where: "id = ?", whereArgs: [incomeSheet.id]);
  }

  Future<Void> deleteIncomeSheet(int id) async {
    await openDb();
    await _database.delete('incomeSheet', where: "id = ?", whereArgs: [id]);
  }
}

class IncomeSheet {
  int id;
  String date;
  String date1;
  String selectCat;
  String selectIcon;
  String amount;
  String note;
  String method;
  String file;

  IncomeSheet({
    this.date,
    this.date1,
    this.selectCat,
    this.selectIcon,
    this.amount,
    this.note,
    this.id,
    this.method,
    this.file,
  });

  String get iconIndex => null;
  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'date1': date1,
      'selectCat': selectCat,
      'selectIcon': selectIcon,
      'amount': amount,
      'note': note,
      'method': method,
      'file': file,
    };
  }
}
