import 'dart:async';
import 'dart:ffi';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ExpenceHelper{
Database _database;

Future openDb() async{
  if(_database== null){
    _database = await openDatabase(
      join(await getDatabasesPath(), "exCat.db"),
      version: 1, onCreate: (Database db, int version) async{
        await db.execute(
          "CREATE TABLE ExCategory(id INTEGER PRIMARY KEY autoincrement, expenseName TEXT, exIconName TEXT)",
        );
      }
    );
  }
}

Future<int>insertExCategory(ExCategory exCategory)async{
  await openDb();
  return await _database.insert('ExCategory', exCategory.toMap());
}


Future<List<ExCategory>> getExCategoryList() async{
  await openDb();
  final List<Map<String, dynamic>> maps= await _database.query('ExCategory');
  return List.generate(maps.length, (i){
    return ExCategory(
      id: maps[i]['id'], 
      expenseName: maps[i]['expenseName'],
       exIconIndex: maps[i]['exIconName']);
    
  });
}

Future<int> updateExCategory(ExCategory exCategory) async{
  await openDb();
  return await _database.update('ExCategory', exCategory.toMap(),
  where: "id = ?", whereArgs: [exCategory.id]);
}


Future<Void> deleteExCategory(int id) async{
  await openDb();
  await _database.delete(
    'ExCategory',
 where: "id = ?", whereArgs: [id]
 );
}
}










class ExCategory{
  int id;
  String expenseName;
  String exIconIndex;

  ExCategory({
    this.expenseName, 
    this.exIconIndex, 
    this.id,} );

  
  Map<String, dynamic> toMap(){
    return {'expenseName': expenseName, 'exIconName': exIconIndex,};
  }
}