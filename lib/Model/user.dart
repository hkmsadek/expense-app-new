class User {

  int id;
  String _date;
  String _category;
  String _subcategory;
  String _amount;
  String _icon1 ;


  User(this._date, this._category, this._subcategory, this._amount, this._icon1);

  User.map(dynamic obj) {
    this._date = obj["date"];
    this._category = obj["category"];
    this._subcategory = obj["subcategory"];
    this._amount = obj["amount"];
    this._icon1 = obj["icon1"];

  }

  String get date => _date;

  String get category => _category;

  String get subcategory => _subcategory;

  String get amount => _amount;
  String get icon1 => _icon1;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();

    map["date"] = _date;
    map["category"] = _category;
    map["subcategory"] = _subcategory;
    map["amount"] = _amount;
    map["icon1"] = _icon1;

    return map;
  }

  void setUserId(int id) {
    this.id = id;
  }
}