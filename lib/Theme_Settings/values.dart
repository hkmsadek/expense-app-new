import 'package:flutter/material.dart';

final darkTheme = ThemeData(
  primarySwatch: Colors.grey,
  primaryColor: Color(0xFF000000),
  brightness: Brightness.dark,
  backgroundColor: const Color(0xFF142543),
  scaffoldBackgroundColor: Color(0xFF142543),
  accentColor: Colors.white,
  accentTextTheme: TextTheme(title: TextStyle(color: Color(0XFF2A539F).withOpacity(0.7)),
  subhead: TextStyle(color: Colors.white),
  subtitle: TextStyle(color: Color(0XFFF2F4F7).withOpacity(0.6)),
  body1: TextStyle(color: Color(0XFFFFFFFF)),
  body2: TextStyle(color: Colors.white),
  display1: TextStyle(color: Colors.white),
  overline: TextStyle(color: Color(0XFFF2F4F7).withOpacity(0.8)),
  display2: TextStyle(color: Color(0XFFF2F4F7).withOpacity(0.6)),
  display3: TextStyle(color: Colors.white),
  display4: TextStyle(color: Colors.white),
  caption: TextStyle(color: Colors.white),
  headline: TextStyle(color: Colors.white)),
  accentIconTheme: IconThemeData(color: Color(0xFF142543)),
  iconTheme: IconThemeData(color: Color(0XFF596F97)),
  inputDecorationTheme: InputDecorationTheme(fillColor: Colors.white),
  primaryIconTheme: IconThemeData(color: Colors.white),

  cardColor: Color(0XFF000000).withOpacity(0.5),//Color(0XFF0D0D0D),
  cardTheme: CardTheme(color: Colors.white10, ),

  dividerColor: Colors.black12,
  bottomAppBarColor: Color(0XFF000000),
  buttonColor: Color(0XFF294F95).withOpacity(0.2),

  selectedRowColor: Color(0XFFF2F4F7).withOpacity(0.6),

);

final lightTheme = ThemeData(
  primarySwatch: Colors.grey,
  primaryColor: Colors.white,
  brightness: Brightness.light,
  backgroundColor: const Color(0xFFF2F4F7),
  scaffoldBackgroundColor: Colors.white,
  accentColor: Color(0XFF1A3D7A),
  accentTextTheme: TextTheme(title: TextStyle(color: Color(0XFF2A539F).withOpacity(0.7)),
  subhead: TextStyle(color: Color(0XFF284E95)),
  subtitle: TextStyle(color: Color(0XFF284E94).withOpacity(0.6)),
  body1: TextStyle(color: Color(0XFF2B2B2B)),
  body2: TextStyle(color: Color(0XFF244683)),
  display1: TextStyle(color: Color(0XFF1B3D7A)),
  overline: TextStyle(color: Color(0XFF284E94).withOpacity(0.8)),
  display2: TextStyle(color: Color(0XFF244683).withOpacity(0.7)),
  display3: TextStyle(color: Color(0XFF294F95)),
  display4: TextStyle(color: Color(0XFFFFFFFF)),
  caption: TextStyle(color: Color(0XFF1E3869)),
  headline: TextStyle(color: Color(0XFF214078))),
  iconTheme: IconThemeData(color: Color(0XFF294F95).withOpacity(0.8)),
  inputDecorationTheme: InputDecorationTheme(fillColor: Color(0XFF2C549E)),
  accentIconTheme: IconThemeData(color: Color(0XFF397FC5)),
  primaryIconTheme: IconThemeData(color: Colors.white),
  cardColor: Color(0XFFFFFFFF),

  cardTheme: CardTheme(color: Colors.grey[350]),

  dividerColor: Colors.white54,
  bottomAppBarColor: Color(0XFF1A3D7A),
  buttonColor: Colors.white,

  selectedRowColor: Color(0XFF1C3F7B),

);